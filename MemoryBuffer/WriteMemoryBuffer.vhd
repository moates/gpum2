library ieee;
use ieee.std_logic_1164.all;

LIBRARY altera_mf;
USE altera_mf.all;

use work.GpuTypes.all;

entity WriteMemoryBuffer is
	generic(
		transferLength : natural := 32;
		depth          : natural := 2;
		width          : natural := 32;
		addressWidth   : natural := 19;
		byteEnableWidth: natural := 2
	);
	port(
		rdclk, wrclk  : in  std_logic;
		wrreq, rdreq : in  std_logic;
		wrdata             : in  std_logic_vector(width - 1 downto 0);
		rddata             : out std_logic_vector(width - 1 downto 0);
		wrbyteenable	   : in std_logic_vector(byteEnableWidth - 1 downto 0);
		rdbyteenable	   : out std_logic_vector(byteEnableWidth - 1 downto 0);
		rdempty, wrfull, writerequest : out std_logic;
		rdaddress          : out std_logic_vector(18 downto 0);
		wraddress          : in  std_logic_vector(18 downto 0)
	);
end entity WriteMemoryBuffer;

architecture RTL of WriteMemoryBuffer is
	COMPONENT dcfifo
		GENERIC(
			clocks_are_synchronized : STRING;
			intended_device_family  : STRING;
			lpm_hint                : STRING;
			lpm_numwords            : NATURAL;
			lpm_showahead           : STRING;
			lpm_type                : STRING;
			lpm_width               : NATURAL;
			lpm_widthu              : NATURAL;
			overflow_checking       : STRING;
			rdsync_delaypipe        : NATURAL;
			underflow_checking      : STRING;
			use_eab                 : STRING;
			wrsync_delaypipe        : NATURAL
		);
		PORT(
			data    : IN  STD_LOGIC_VECTOR(lpm_width-1 DOWNTO 0);
			rdclk   : IN  STD_LOGIC;
			rdreq   : IN  STD_LOGIC;
			wrfull  : OUT STD_LOGIC;
			q       : OUT STD_LOGIC_VECTOR(lpm_width-1 DOWNTO 0);
			rdempty : OUT STD_LOGIC;
			wrclk   : IN  STD_LOGIC;
			wrreq   : IN  STD_LOGIC
		);
	END COMPONENT;

	signal wraddressrequest : std_logic := '0';
	signal rdaddressrequest : std_logic := '0';
	signal writeData, readData : std_logic_vector(width+byteEnableWidth-1 downto 0);
	signal writeAddress : std_logic_vector(18 downto 0);
	signal wrrequest : std_logic := '0';
begin
	writeData <= wrdata & wrbyteenable;
	rddata  <= readData(width+byteEnableWidth-1 downto byteEnableWidth);
	rdbyteenable <= readData(byteEnableWidth-1 downto 0);
	writerequest <= not wrrequest;

	writeProcess : process(wrclk)
		variable writeProgress : integer range 0 to transferLength - 1 := 0;
		
	begin
		if rising_edge(wrclk) then
			wraddressrequest <= '0';

			if wrreq = '1' then
				if writeProgress = 0 then
					writeAddress <= wraddress;					
				end if;

				if writeProgress = transferLength - 1 then
					writeProgress := 0;
					wraddressrequest <= '1';
				else
					writeProgress := writeProgress + 1;
				end if;
			end if;
		end if;
	end process writeProcess;
	
	readProcess : process(rdclk) 
		variable readProgress : integer range 0 to transferLength - 1  := 0;
	begin
		if rising_edge(rdclk) then
		rdaddressrequest <= '0';
		
		if rdreq = '1' then
				if readProgress = 0 then
					rdaddressrequest <= '1';
				end if;
		
				if readProgress = transferLength - 1 then
					readProgress := 0;					
				else
					readProgress := readProgress + 1;
				end if;
			end if;
		end if;
	end process readProcess;

	dataFifo : component dcfifo
		generic map(clocks_are_synchronized => "TRUE",
			        intended_device_family  => "Cyclone II",
			        lpm_hint                => "MAXIMIE_SPEED=5",
			        lpm_numwords            => depth * transferLength,
			        lpm_showahead           => "ON",
			        lpm_type                => "DCFIFO",
			        lpm_width               => width+byteEnableWidth,
			        lpm_widthu              => LOG2(depth * transferLength),
			        overflow_checking       => "ON",
			        rdsync_delaypipe        => 3,
			        underflow_checking      => "ON",
			        use_eab                 => "ON",
			        wrsync_delaypipe        => 3)
		port map(data    => writeData,
			     rdclk   => rdclk,
			     rdreq   => rdreq,
			     wrfull  => wrfull,
			     q       => readData,
			     rdempty => rdempty,
			     wrclk   => wrclk,
			     wrreq   => wrreq);			    

		addressFifo : component dcfifo
			generic map(clocks_are_synchronized => "TRUE",
				        intended_device_family  => "Cyclone II",
				        lpm_hint                => "MAXIMIE_SPEED=5",
				        lpm_numwords            => depth+1,
				        lpm_showahead           => "ON",
				        lpm_type                => "DCFIFO",
				        lpm_width               => 19,
				        lpm_widthu              => LOG2(depth)+1,
				        overflow_checking       => "ON",
				        rdsync_delaypipe        => 3,
				        underflow_checking      => "ON",
				        use_eab                 => "OFF",
				        wrsync_delaypipe        => 3)
			port map(data    => writeAddress,
				     rdclk   => rdclk,
				     rdreq   => rdaddressrequest,
				     wrfull  => open,
				     q       => rdaddress,
				     rdempty => wrrequest,
				     wrclk   => wrclk,
				     wrreq   => wraddressrequest);

		end architecture RTL;
