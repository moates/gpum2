library ieee;
use ieee.std_logic_1164.all;
use work.GpuTypes.all;

entity MemoryBuffer is
	generic(
		transferLength : natural := 32;
		writeDepth     : natural := 2;
		readDepth      : natural := 1;
		width          : natural := 32;
		addressWidth   : natural := 19;
		read           : boolean := true;
		write          : boolean := true
	);
	port(
		memoryreaddata, pipelinereaddata                                                          : out std_logic_vector(31 downto 0);
		memorywritedata, pipelinewritedata                                                        : in  std_logic_vector(31 downto 0);
		memoryreadbe : out std_logic_vector(1 downto 0);
		pipelinewritebe : in std_logic_vector(1 downto 0);
		memoryread, memorywrite, pipelineread, pipelinewrite, pipelinerequest                     : in  std_logic;
		memorywriteempty, memoryreadfull, memoryreadrequest, pipelinereadempty, pipelinewritefull, memorywriterequest : out std_logic;
		memoryreadaddress, memorywriteaddress                                                     : out std_logic_vector(18 downto 0);
		pipelinereadaddress, pipelinewriteaddress                                                 : in  std_logic_vector(18 downto 0);
		memoryclk, pipeline_read_clk, pipeline_write_clk                                          : in  std_logic
	);
end entity MemoryBuffer;

architecture RTL of MemoryBuffer is
	component ReadMemoryBuffer
		generic(transferLength : natural := 32;
			    depth          : natural := 1;
			    width          : natural := 32;
			    addressWidth   : natural := 19);
		port(memclk, dataclk                 : in  std_logic;
			 memreq, datareq, datarequest    : in  std_logic;
			 memdata                         : in  std_logic_vector(width - 1 downto 0);
			 datadata                        : out std_logic_vector(width - 1 downto 0);
			 dataempty, memfull, readrequest : out std_logic;
			 memaddress                      : out std_logic_vector(18 downto 0);
			 dataaddress                     : in  std_logic_vector(18 downto 0));
	end component ReadMemoryBuffer;

	component WriteMemoryBuffer
		generic(transferLength : natural := 32;
			    depth          : natural := 2;
			    width          : natural := 32;
			    addressWidth   : natural := 19);
		port(rdclk, wrclk    : in  std_logic;
			 wrreq, rdreq    : in  std_logic;
			 wrdata          : in  std_logic_vector(width - 1 downto 0);
			 rddata          : out std_logic_vector(width - 1 downto 0);
			 wrbyteenable	   : in std_logic_vector(width / BPP - 1 downto 0);
			 rdbyteenable	   : out std_logic_vector(width / BPP - 1 downto 0);
			 rdempty, wrfull, writerequest : out std_logic;
			 rdaddress       : out std_logic_vector(18 downto 0);
			 wraddress       : in  std_logic_vector(18 downto 0));
	end component WriteMemoryBuffer;
begin
	readProcess : if read generate
		readComponent : component ReadMemoryBuffer
			generic map(transferLength => transferLength,
				        depth          => readDepth,
				        width          => width,
				        addressWidth   => addressWidth)
			port map(memclk      => memoryclk,
				     dataclk     => pipeline_read_clk,
				     memreq      => memoryread,
				     datareq     => pipelineread,
				     datarequest => pipelinerequest,
				     memdata     => memorywritedata,
				     datadata    => pipelinereaddata,
				     dataempty   => pipelinereadempty,
				     memfull     => memoryreadfull,
				     memaddress  => memoryreadaddress,
				     dataaddress => pipelinereadaddress,
				     readrequest => memoryreadrequest);
	end generate readProcess;

	writeProcess : if write generate
		writeComponent : component WriteMemoryBuffer
			generic map(transferLength => transferLength,
				        depth          => writeDepth,
				        width          => width,
				        addressWidth   => addressWidth)
			port map(rdclk     => memoryclk,
				     wrclk     => pipeline_write_clk,
				     wrreq     => pipelinewrite,
				     rdreq     => memorywrite,
				     wrdata    => pipelinewritedata,
				     rddata    => memoryreaddata,
				     rdempty   => memorywriteempty,
				     wrfull    => pipelinewritefull,
				     rdaddress => memorywriteaddress,
				     wraddress => pipelinewriteaddress,
				     wrbyteenable => pipelinewritebe,
				     rdbyteenable => memoryreadbe,
				     writerequest => memorywriterequest);
	end generate writeProcess;

end architecture RTL;
