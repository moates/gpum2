library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity SSRAM is
	port(
		memory_clk, fpga_clk, vga_clk                               : in    std_logic;
		ssram_data                                                  : inout std_logic_vector(31 downto 0);
		ssram_address                                               : out   std_logic_vector(18 downto 0);
		ssram_ce1_n, ssram_ce2, ssram_ce3_n                         : out   std_logic;
		ssram_gwe, ssram_bwe                                        : out   std_logic;
		ssram_adsp, ssram_adsc                                      : out   std_logic;
		ssram_bw                                                    : out   std_logic_vector(3 downto 0);
		ssram_adv, ssram_oe                                         : out   std_logic;
		vga_readAddress                                             : in    Address;
		vga_readData                                                : out   std_logic_vector(31 downto 0);
		outputCache_writeData                                       : in    std_logic_vector(31 downto 0);
		zBuffer_readAddress                                         : in    Address;
		zBuffer_readData                                            : out   std_logic_vector(31 downto 0);
		zBuffer_writeData                                           : in    std_logic_vector(31 downto 0);
		vga_read, vga_request, outputCache_write                    : in    std_logic;
		zBuffer_read, zBuffer_request, zBuffer_write                : in    std_logic;
		outputCache_be                                              : in    std_logic_vector(1 downto 0);
		outputCache_writeAddress                                    : in    Address;
		vga_readEmpty                                               : out   std_logic;
		zBuffer_be                                                  : in    std_logic_vector(1 downto 0);
		zBuffer_writeAddress                                        : in    Address;
		zBuffer_readEmpty, zBuffer_writeFull, outputCache_writeFull : out   std_logic;
		activeBuffer : in std_logic
	);
end entity SSRAM;

architecture RTL of SSRAM is
	component MemoryBuffer
		generic(transferLength : natural := 32;
			    writeDepth     : natural := 2;
			    readDepth      : natural := 1;
			    width          : natural := 32;
			    addressWidth   : natural := 19;
			    read           : boolean := true;
			    write          : boolean := true);
		port(memoryreaddata, pipelinereaddata                                                          : out std_logic_vector(31 downto 0);
			 memorywritedata, pipelinewritedata                                                        : in  std_logic_vector(31 downto 0);
			 memoryread, memorywrite, pipelineread, pipelinewrite, pipelinerequest                     : in  std_logic;
			 memoryreadbe                                                                              : out std_logic_vector(1 downto 0);
			 pipelinewritebe                                                                           : in  std_logic_vector(1 downto 0);
			 memorywriteempty, memoryreadfull, memoryreadrequest, pipelinereadempty, pipelinewritefull,memorywriterequest : out std_logic;
			 memoryreadaddress, memorywriteaddress                                                     : out std_logic_vector(18 downto 0);
			 pipelinereadaddress, pipelinewriteaddress                                                 : in  std_logic_vector(18 downto 0);
			 memoryclk, pipeline_read_clk, pipeline_write_clk                                          : in  std_logic);
	end component MemoryBuffer;

	component SSRAMScheduler
		port(clk                                                                                                                                                   : in  std_logic;
			 cache_readAddress_0, cache_readAddress_1                                                                                                              : in  std_logic_vector(18 downto 0);
			 cache_readData_0, cache_readData_1                                                                                                                    : out std_logic_vector(31 downto 0);
			 cache_writeAddress_0, cache_writeAddress_1                                                                                                            : in  std_logic_vector(18 downto 0);
			 cache_byteEnable_0, cache_byteEnable_1                                                                                                                : in  std_logic_vector(1 downto 0);
			 memory_byteEnable                                                                                                                                     : out std_logic_vector(1 downto 0);
			 cache_writeData_0, cache_writeData_1                                                                                                                  : in  std_logic_vector(31 downto 0);
			 cache_readFull_0, cache_readFull_1, cache_writeEmpty_0, memory_Read, cache_writeEmpty_1, cache_readRequest_0, cache_readRequest_1, memory_ReadAck, memory_writeAck, cache_writeRequest_0, cache_writeRequest_1 : in  std_logic;
			 memory_readFull, memory_writeEmpty, memory_readReady, memory_writeReady, cache_read_0, cache_read_1, cache_write_0, cache_write_1        : out std_logic;
			 memory_ReadAddress, memory_WriteAddress                                                                                                               : out std_logic_vector(18 downto 0);
			 memory_readData                                                                                                                                       : in  std_logic_vector(31 downto 0);
			 memory_WriteData                                                                                                                                      : out std_logic_vector(31 downto 0));
	end component SSRAMScheduler;

	component SSRAMInterface
		port(ssram_data                          : inout std_logic_vector(31 downto 0);
			 ssram_address                       : out   std_logic_vector(18 downto 0);
			 read_data                           : out   std_logic_vector(31 downto 0);
			 write_data                          : in    std_logic_vector(31 downto 0);
			 write_data_be                       : in    std_logic_vector(1 downto 0);
			 read_address                        : in    std_logic_vector(18 downto 0);
			 write_address                       : in    std_logic_vector(18 downto 0);
			 readReady, writeReady               : in    std_logic;
			 readAck, writeAck, memory_Read      : out   std_logic;
			 readFull, writeEmpty                : in    std_logic;
			 ssram_ce1_n, ssram_ce2, ssram_ce3_n : out   std_logic;
			 ssram_gwe, ssram_bwe                : out   std_logic;
			 ssram_adsp, ssram_adsc              : out   std_logic;
			 ssram_bw                            : out   std_logic_vector(3 downto 0);
			 ssram_adv, ssram_oe                 : out   std_logic;
			 clk                                 : in    std_logic);
	end component SSRAMInterface;
	signal memory_ReadAddress, memory_WriteAddress                                              : std_logic_vector(18 downto 0);
	signal memory_readFull, memory_writeEmpty                                                   : std_logic;
	signal memory_writeAck                                                                      : std_logic;
	signal memory_ReadAck                                                                       : std_logic;
	signal memory_readReady                                                                     : std_logic;
	signal memory_writeReady, cache_read_0, cache_write_0                                       : std_logic;
	signal memory_readData                                                                      : std_logic_vector(31 downto 0);
	signal memory_WriteData                                                                     : std_logic_vector(31 downto 0);
	signal memory_writeDataBE                                                                   : std_logic_vector(1 downto 0);
	signal cache_writeData_0, cache_writeData_1, cache_readData_0, cache_readData_1             : std_logic_vector(31 downto 0);
	signal cache_writeAddress_0, cache_writeAddress_1, cache_readAddress_0, cache_readAddress_1 : std_logic_vector(18 downto 0);
	signal cache_byteEnable_0, cache_byteEnable_1                            : std_logic_vector(1 downto 0);
	signal cache_readFull_0, cache_writeEmpty_0, cache_readRequest_0                            : std_logic;
	signal cache_readFull_1, cache_writeEmpty_1, cache_readRequest_1                            : std_logic;
	signal cache_read_1, cache_write_1                                                          : std_logic;
	signal memory_Read                                                                          : std_logic;
	signal gpu_buffer_address, vga_buffer_address : std_logic_vector(18 downto 0);
	signal zBuffer_buffer_readAddress, zBuffer_buffer_writeAddress : std_logic_vector(18 downto 0);
	signal cache_writeRequest_1, cache_writeRequest_0 : std_logic;
begin
	ssramSchedulerInstance : component SSRAMScheduler
		port map(clk                  => memory_clk,
			     cache_readAddress_0  => cache_readAddress_0,
			     cache_readAddress_1  => cache_readAddress_1,
			     cache_readData_0     => cache_readData_0,
			     cache_readData_1     => cache_readData_1,
			     cache_writeAddress_0 => cache_writeAddress_0,
			     cache_writeAddress_1 => cache_writeAddress_1,
			     cache_byteEnable_0   => cache_byteEnable_0,
			     cache_byteEnable_1   => cache_byteEnable_1,
			     memory_byteEnable    => memory_writeDataBE,
			     cache_writeData_0    => cache_writeData_0,
			     cache_writeData_1    => cache_writeData_1,
			     cache_readFull_0     => cache_readFull_0,
			     cache_readFull_1     => cache_readFull_1,
			     cache_writeEmpty_0   => cache_writeEmpty_0,
			     cache_writeEmpty_1   => cache_writeEmpty_1,
			     cache_readRequest_0  => cache_readRequest_0,
			     cache_readRequest_1  => cache_readRequest_1,
			     memory_ReadAck       => memory_ReadAck,
			     memory_writeAck      => memory_writeAck,
			     memory_readFull      => memory_readFull,
			     memory_writeEmpty    => memory_writeEmpty,
			     memory_readReady     => memory_readReady,
			     memory_writeReady    => memory_writeReady,
			     memory_Read          => memory_Read,
			     cache_read_0         => cache_read_0,
			     cache_read_1         => cache_read_1,
			     cache_write_0        => cache_write_0,
			     cache_write_1        => cache_write_1,
			     memory_ReadAddress   => memory_ReadAddress,
			     memory_WriteAddress  => memory_WriteAddress,
			     memory_readData      => memory_readData,
			     memory_WriteData     => memory_WriteData,
			     cache_writeRequest_0 => cache_writeRequest_0,
			     cache_writeRequest_1 => cache_writeRequest_1);

	bufferInstance_0 : component MemoryBuffer
		generic map(transferLength => 32,
			        writeDepth     => 4,
			        readDepth 	   => 4,
			        width          => 32,
			        addressWidth   => BusWidth,
			        read           => true,
			        write          => true)
		port map(memoryreaddata       => cache_writeData_0,
			     pipelinereaddata     => vga_readData,
			     memorywritedata      => cache_readData_0,
			     pipelinewritedata    => outputCache_writeData,
			     memoryread           => cache_read_0,
			     memorywrite          => cache_write_0,
			     pipelineread         => vga_read,
			     pipelinewrite        => outputCache_write,
			     pipelinerequest      => vga_request,
			     memorywriteempty     => cache_writeEmpty_0,
			     memoryreadfull       => cache_readFull_0,
			     memoryreadrequest    => cache_readRequest_0,
			     pipelinereadempty    => vga_readEmpty,
			     pipelinewritefull    => outputCache_writeFull,
			     memoryreadaddress    => cache_readAddress_0,
			     memorywriteaddress   => cache_writeAddress_0,
			     pipelinereadaddress  => vga_buffer_address,
			     pipelinewriteaddress => gpu_buffer_address,
			     memoryclk            => memory_clk,
			     pipeline_read_clk    => vga_clk,
			     pipeline_write_clk   => fpga_clk, 
			     memoryreadbe         => cache_byteEnable_0,
			     pipelinewritebe      => outputCache_be,
			     memorywriterequest => cache_writeRequest_0);

	bufferInstance_1 : component MemoryBuffer
		generic map(transferLength => 32,
			        writeDepth     => 4,
			        readDepth => 4,
			        width          => 32,
			        addressWidth   => BusWidth,
			        read           => true,
			        write          => true)
		port map(memoryreaddata       => cache_writeData_1,
			     pipelinereaddata     => zBuffer_readData,
			     memorywritedata      => cache_readData_1,
			     pipelinewritedata    => zBuffer_writeData,
			     memoryread           => cache_read_1,
			     memorywrite          => cache_write_1,
			     pipelineread         => zBuffer_read,
			     pipelinewrite        => zBuffer_write,
			     pipelinerequest      => zBuffer_request,
			     memorywriteempty     => cache_writeEmpty_1,
			     memoryreadfull       => cache_readFull_1,
			     memoryreadrequest    => cache_readRequest_1,
			     pipelinereadempty    => zBuffer_readEmpty,
			     pipelinewritefull    => zBuffer_writeFull,
			     memoryreadaddress    => cache_readAddress_1,
			     memorywriteaddress   => cache_writeAddress_1,
			     pipelinereadaddress  => zBuffer_buffer_readAddress,
			     pipelinewriteaddress => zBuffer_buffer_writeAddress,
			     memoryclk            => memory_clk,
			     pipeline_read_clk    => fpga_clk,
			     pipeline_write_clk   => fpga_clk,
			     memoryreadbe         => cache_byteEnable_1,
			     pipelinewritebe      => zBuffer_be,
			     memorywriterequest => cache_writeRequest_1);

	ssramInterfaceInstance : component SSRAMInterface
		port map(ssram_data    => ssram_data,
			     ssram_address => ssram_address,
			     read_data     => memory_readData,
			     write_data    => memory_writeData,
			     write_data_be => memory_writeDataBE,
			     read_address  => memory_ReadAddress,
			     write_address => memory_WriteAddress,
			     readReady     => memory_readReady,
			     writeReady    => memory_writeReady,
			     readAck       => memory_readAck,
			     writeAck      => memory_writeAck,
			     readFull     => memory_readFull,
			     writeEmpty     => memory_writeEmpty,
			     memory_Read   => memory_Read,
			     ssram_ce1_n   => ssram_ce1_n,
			     ssram_ce2     => ssram_ce2,
			     ssram_ce3_n   => ssram_ce3_n,
			     ssram_gwe     => ssram_gwe,
			     ssram_bwe     => ssram_bwe,
			     ssram_adsp    => ssram_adsp,
			     ssram_adsc    => ssram_adsc,
			     ssram_bw      => ssram_bw,
			     ssram_adv     => ssram_adv,
			     ssram_oe      => ssram_oe,
			     clk           => memory_clk);
			     
	gpu_buffer_address <= "0"&outputCache_writeAddress when activeBuffer = '0'
		else std_logic_vector(resize(unsigned(outputCache_writeAddress),19) + 320 * 480);

	vga_buffer_address <= "0"&vga_readAddress when activeBuffer = '1'
		else std_logic_vector(resize(unsigned(vga_readAddress),19) + 320 * 480);		
		
	zBuffer_buffer_readAddress <= std_logic_vector(resize(unsigned(zBuffer_readAddress),19) + 320 * 480 * 2);
	zBuffer_buffer_writeAddress <= std_logic_vector(resize(unsigned(zBuffer_writeAddress),19) + 320 * 480 * 2);

end architecture RTL;
