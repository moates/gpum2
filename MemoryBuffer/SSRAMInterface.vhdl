library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gputypes.all;

entity SSRAMInterface is
	port(
		ssram_data                          : inout std_logic_vector(31 downto 0);
		ssram_address                       : out   std_logic_vector(18 downto 0); -- Address lines to ssram
		read_data                           : out   std_logic_vector(31 downto 0);
		write_data                          : in    std_logic_vector(31 downto 0);
		write_data_be                       : in    std_logic_vector(1 downto 0);
		read_address                        : in    std_logic_vector(18 downto 0);
		write_address                       : in    std_logic_vector(18 downto 0);
		readReady, writeReady               : in    std_logic;
		readAck, writeAck, memory_Read      : out   std_logic;
		readFull, writeEmpty                : in    std_logic;
		ssram_ce1_n, ssram_ce2, ssram_ce3_n : out   std_logic;
		ssram_gwe, ssram_bwe                : out   std_logic;
		ssram_adsp, ssram_adsc              : out   std_logic;
		ssram_bw                            : out   std_logic_vector(3 downto 0);
		ssram_adv, ssram_oe                 : out   std_logic;
		clk                                 : in    std_logic);
end SSRAMInterface;

architecture SSRAMPipeline_RTL of SSRAMInterface is
	type OperandType is (Idle, Reading, Writing);

	type PipelineStage is record
		operand : OperandType;
		address : std_logic_vector(18 downto 0);
		data    : std_logic_vector(31 downto 0);
		be      : std_logic_vector(3 downto 0);
	end record;

	type PipelineType is array (4 downto 0) of PipelineStage;

	signal pipeline : PipelineType := (others => (operand => Idle, address => (others => '0'), data => (others => '0'), be => (others => '0')));
begin                                   -- SSRAMPipeline_RTL

	pipelineProcess : process(clk)
		variable burstIndex : integer range 0 to 31 := 0;

		variable currentState : OperandType := Idle;
		variable lastOp       : OperandType := Idle; -- Last operation - read/write

		variable wait_step : integer range 0 to 2 := 0;
	begin
		if rising_edge(clk) then        -- process pipelineProcess
			case currentState is
				when Idle =>
					burstIndex := 0;
					readAck    <= '0';
					writeAck   <= '0';

					pipeline(4).operand <= Idle;

					if wait_step = 2 then
						lastOp    := Idle;
						wait_step := 0;
					elsif lastOp /= Idle then
						wait_step := wait_step + 1;
					end if;

					if readReady = '1' and (lastOp = Idle or lastOp = Reading) then
						currentState := Reading;
						
						--readAck             <= '1';
					elsif writeReady = '1' then
						--writeAck <= '1';
						currentState := Writing;
						wait_step := 0;
					end if;
				when Reading =>
					pipeline(4).operand <= Reading;
					pipeline(4).address <= read_address;
					readAck             <= '1';

					if burstIndex = 31 then
						lastOp       := Reading;
						currentState := Idle;
						wait_step := 0;
					else
						burstIndex := burstIndex + 1;
					end if;
				when Writing =>
					writeAck <= '1';

					pipeline(4).operand <= Writing;
					pipeline(4).address <= write_address;
					pipeline(4).data    <= write_data;
					pipeline(4).be(0)   <= write_data_be(0);
					pipeline(4).be(1)   <= write_data_be(0);
					pipeline(4).be(2)   <= write_data_be(1);
					pipeline(4).be(3)   <= write_data_be(1);

					if burstIndex = 31 then
						lastOp       := Writing;
						currentState := Idle;
						wait_step := 0;
					else
						burstIndex := burstIndex + 1;
					end if;
			end case;

			if pipeline(0).operand = Reading then
				read_data   <= pipeline(0).data;
				memory_Read <= '1';
			else
				memory_Read <= '0';
			end if;

			-------------------------------------------------------------------------
			-- Advance pipeline
			-------------------------------------------------------------------------
			for i in 1 to 4 loop
				pipeline(i - 1) <= pipeline(i);
			end loop;                   -- i

			if pipeline(1).operand = Reading then
				pipeline(0).data <= ssram_data;
			end if;
		end if;
	end process pipelineProcess;

	ssram_ce1_n <= '0';
	ssram_ce2   <= '1';
	ssram_ce3_n <= '0';
	ssram_adsp  <= '1';
	ssram_adv   <= '1';
	ssram_gwe   <= '1';

	outputProcess : process(clk)
	begin                               -- process outputProcess
		if clk'event and clk = '1' then -- rising clock edge    
			if pipeline(4).operand = Reading then
				ssram_oe      <= '0';
				ssram_address <= pipeline(4).address;
				ssram_data    <= (others => 'Z');
				ssram_adsc    <= '0';
				ssram_bw      <= (others => '0');
				ssram_bwe     <= '1';
			elsif pipeline(1).operand = Writing then
				ssram_oe      <= '1';
				ssram_data    <= pipeline(1).data;
				ssram_address <= pipeline(1).address;
				ssram_bwe     <= '0';
				ssram_adsc    <= '0';
				ssram_oe      <= '1';
				ssram_bw      <= pipeline(1).be;
			else
				ssram_oe   <= '0';
				ssram_bwe  <= '1';
				ssram_data <= (others => 'Z');
				ssram_adsc <= '1';
				ssram_bw   <= (others => '0');
			end if;
		end if;
	end process outputProcess;

end SSRAMPipeline_RTL;