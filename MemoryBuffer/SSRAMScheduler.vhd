library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity SSRAMScheduler is
	port (
		clk : in std_logic;
		cache_readAddress_0, cache_readAddress_1 : in std_logic_vector(18 downto 0);
		cache_readData_0, cache_readData_1 : out std_logic_vector(31 downto 0);
		cache_writeAddress_0, cache_writeAddress_1 : in std_logic_vector(18 downto 0);
		cache_byteEnable_0, cache_byteEnable_1 : in std_logic_vector(1 downto 0);
		memory_byteEnable : out std_logic_vector(1 downto 0);
		cache_writeData_0, cache_writeData_1 : in std_logic_vector(31 downto 0);
		cache_readFull_0, cache_readFull_1, cache_writeEmpty_0, memory_Read, cache_writeEmpty_1, cache_readRequest_0, cache_readRequest_1, memory_ReadAck, memory_writeAck, cache_writeRequest_0, cache_writeRequest_1 : in std_logic;
		memory_readFull, memory_writeEmpty, memory_readReady, memory_writeReady, cache_read_0, cache_read_1, cache_write_0, cache_write_1 : out std_logic;
		memory_ReadAddress, memory_WriteAddress : out std_logic_vector(18 downto 0);
		memory_readData : in std_logic_vector(31 downto 0);
		memory_WriteData : out std_logic_vector(31 downto 0)
	);
end entity SSRAMScheduler;

architecture RTL of SSRAMScheduler is
	type State is (Idle, Reading_0, Reading_1, Writing_0, Writing_1);
	
	signal currentState, readState : State;
	signal indexCounter : unsigned (4 downto 0); 
	signal vga_tileOffset : std_logic_vector(18 downto 0);
	signal tile_index : unsigned(2 downto 0);
	signal writeAddress : std_logic_vector(18 downto 0);
begin
	
	memory_readReady <= '1' when currentState = Reading_0 or currentState = Reading_1 else '0';
	memory_writeReady <= '1' when currentState = Writing_0 or currentState = Writing_1 else '0';
	
	memory_readFull <= cache_readFull_0 when readState = Reading_0
			else cache_readFull_1 when readState = Reading_1
			else '0';					
			
	memory_writeEmpty <= cache_writeEmpty_0 when currentState = Writing_0
			 else cache_writeEmpty_1 when currentState = Writing_1
			 else '0';
			 
	memory_WriteData <= cache_writeData_0 when currentState = Writing_0
			else cache_writeData_1;
	
	memory_byteEnable <= cache_byteEnable_0 when currentState = Writing_0
			else cache_byteEnable_1; 
	
	tile_index <= indexCounter(4 downto 2);
	vga_tileOffset <= (18 downto 8 => '0') & std_logic_vector(tile_index & "00000");
	
	memory_ReadAddress <= std_logic_vector(unsigned(cache_readAddress_0) + unsigned(vga_tileOffset) + (indexCounter(1 downto 0))) when currentState = Reading_0
			else std_logic_vector(unsigned(cache_readAddress_1) + indexCounter);
			
	memory_WriteAddress <= std_logic_vector(unsigned(writeAddress) + indexCounter);
	
	cache_readData_0 <= memory_readData;
	cache_readData_1 <= memory_readData;
	
	-- Cache fifo read/write signals 
	cache_read_0 <= '1' when readState = Reading_0 and memory_Read = '1' else '0';
	cache_read_1 <= '1' when readState = Reading_1 and memory_Read = '1' else '0';
	
	cache_write_0 <= '1' when currentState = Writing_0 and memory_writeAck = '1' else '0';
	cache_write_1 <= '1' when currentState = Writing_1 and memory_writeAck = '1' else '0';
	
	memoryControllerProcess : process (clk) is
		variable readCounter : integer range 0 to 31;
		variable nextReadState : State := Idle;
	begin
		if rising_edge(clk) then
			case currentState is 
				when Idle =>
					indexCounter <= (others => '0');									
					if cache_readRequest_0 = '1' then
						currentState <= Reading_0;
						nextReadState := Reading_0;
					elsif cache_readRequest_1 = '1' then
						currentState <= Reading_1;
						nextReadState := Reading_1;
					elsif cache_writeRequest_0 = '1' then
						currentState <= Writing_0;
						writeAddress <= cache_writeAddress_0;
					elsif cache_writeRequest_1 = '1' then
						currentState <= Writing_1;
						writeAddress <= cache_writeAddress_1;
					end if;
				when Reading_0 | Reading_1 =>					
					
					if memory_ReadAck = '1' then
						if indexCounter /= 31 then
							indexCounter <= indexCounter + 1;
						else
							currentState <= Idle;
						end if;															
					end if;									
				when Writing_0 | Writing_1 =>
					if memory_WriteAck = '1' then
						if indexCounter = 31 then
							currentState <= Idle;
						else
							indexCounter <= indexCounter + 1;
						end if;					
					end if;
			end case;
			
			if readState = Idle and nextReadState /= Idle then
				readState <= nextReadState;
				nextReadState := Idle;
			elsif memory_Read = '1' then
				if readCounter = 31 then
					readCounter := 0;
					readState <= nextReadState;
					nextReadState := Idle;
				else
					readCounter := readCounter + 1;
				end if; 					
			end if;
		end if;
	end process memoryControllerProcess;
			
end architecture RTL;
