library ieee;
use ieee.std_logic_1164.all;

LIBRARY altera_mf;
USE altera_mf.all;

use work.GpuTypes.all;

entity ReadMemoryBuffer is
	generic(
		transferLength : natural := 32;
		depth          : natural := 1;
		width          : natural := 32;
		addressWidth   : natural := 19
	);
	port(
		memclk, dataclk  : in  std_logic;
		memreq, datareq, datarequest : in  std_logic;
		memdata             : in  std_logic_vector(width - 1 downto 0);
		datadata             : out std_logic_vector(width - 1 downto 0);
		dataempty, memfull, readrequest    : out std_logic;
		memaddress          : out std_logic_vector(18 downto 0);
		dataaddress          : in  std_logic_vector(18 downto 0)
	);
end entity ReadMemoryBuffer;

architecture RTL of ReadMemoryBuffer is
	COMPONENT dcfifo
		GENERIC(
			clocks_are_synchronized : STRING;
			intended_device_family  : STRING;
			lpm_hint                : STRING;
			lpm_numwords            : NATURAL;
			lpm_showahead           : STRING;
			lpm_type                : STRING;
			lpm_width               : NATURAL;
			lpm_widthu              : NATURAL;
			overflow_checking       : STRING;
			rdsync_delaypipe        : NATURAL;
			underflow_checking      : STRING;
			use_eab                 : STRING;
			wrsync_delaypipe        : NATURAL
		);
		PORT(
			data    : IN  STD_LOGIC_VECTOR(lpm_width-1 DOWNTO 0);
			rdclk   : IN  STD_LOGIC;
			rdreq   : IN  STD_LOGIC;
			wrfull  : OUT STD_LOGIC;
			q       : OUT STD_LOGIC_VECTOR(lpm_width-1 DOWNTO 0);
			rdempty : OUT STD_LOGIC;
			wrclk   : IN  STD_LOGIC;
			wrreq   : IN  STD_LOGIC
		);
	END COMPONENT;

	signal wraddressrequest : std_logic := '0';
	signal rdaddressfifoempty : STD_LOGIC;
begin
	readrequest <= not rdaddressfifoempty;

	readProcess : process(dataclk)
		variable writeProgress : integer range 0 to transferLength - 1 := 0;
	begin
		if rising_edge(dataclk) then
			wraddressrequest <= '0';

			if memreq = '1' then
				if writeProgress = transferLength - 9 then
					wraddressrequest <= '1';
					writeProgress := writeProgress + 1;
				elsif writeProgress = transferLength - 1 then
					writeProgress := 0;					
				else
					writeProgress := writeProgress + 1;
				end if;
			end if;
		end if;
	end process readProcess;

	dataFifo : component dcfifo
		generic map(clocks_are_synchronized => "TRUE",
			        intended_device_family  => "Cyclone II",
			        lpm_hint                => "MAXIMIE_SPEED=5",
			        lpm_numwords            => depth * transferLength,
			        lpm_showahead           => "ON",
			        lpm_type                => "DCFIFO",
			        lpm_width               => width,
			        lpm_widthu              => LOG2(depth * transferLength),
			        overflow_checking       => "ON",
			        rdsync_delaypipe        => 3,
			        underflow_checking      => "ON",
			        use_eab                 => "ON",
			        wrsync_delaypipe        => 3)
		port map(data    => memdata,
			     rdclk   => dataclk,
			     rdreq   => datareq,
			     wrfull  => memfull,
			     q       => datadata,
			     rdempty => dataempty,
			     wrclk   => memclk,
			     wrreq   => memreq);

		addressFifo : component dcfifo
			generic map(clocks_are_synchronized => "TRUE",
				        intended_device_family  => "Cyclone II",
				        lpm_hint                => "MAXIMIE_SPEED=5",
				        lpm_numwords            => depth,
				        lpm_showahead           => "ON",
				        lpm_type                => "DCFIFO",
				        lpm_width               => 19,
				        lpm_widthu              => LOG2(depth),
				        overflow_checking       => "ON",
				        rdsync_delaypipe        => 3,
				        underflow_checking      => "ON",
				        use_eab                 => "OFF",
				        wrsync_delaypipe        => 3)
			port map(data    => dataaddress,
				     rdclk   => memclk,
				     rdreq   => wraddressrequest,
				     wrfull  => open,
				     q       => memaddress,
				     rdempty => rdaddressfifoempty,
				     wrclk   => dataclk,
				     wrreq   => datarequest);

		end architecture RTL;
