library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

LIBRARY altera_mf;
USE altera_mf.all;

entity ZTileCache is
	generic (
		Tiles : natural := 1200;
		IdentifierWidth : natural := 5
	);
	port (
		clk : in std_logic;
		readdress, wraddress : in std_logic_vector(IdentifierWidth-1 downto 0);
		clear, write : in std_logic;
		empty, tile_empty : out std_logic
	);
end entity ZTileCache;

architecture RTL of ZTileCache is
	constant TileIndexWidth : natural := IdentifierWidth-log2(4800/32/4);

	COMPONENT altsyncram
	GENERIC (
		address_reg_b		: STRING;
		clock_enable_input_a		: STRING;
		clock_enable_input_b		: STRING;
		clock_enable_output_a		: STRING;
		clock_enable_output_b		: STRING;
		intended_device_family		: STRING;
		lpm_type		: STRING;
		numwords_a		: NATURAL;
		numwords_b		: NATURAL;
		operation_mode		: STRING;
		outdata_aclr_b		: STRING;
		outdata_reg_b		: STRING;
		power_up_uninitialized		: STRING;
		read_during_write_mode_mixed_ports		: STRING;
		widthad_a		: NATURAL;
		widthad_b		: NATURAL;
		width_a		: NATURAL;
		width_b		: NATURAL;
		width_byteena_a		: NATURAL
	);
	PORT (
			address_a	: IN STD_LOGIC_VECTOR (TileIndexWidth-1 DOWNTO 0);
			clock0	: IN STD_LOGIC ;
			data_a	: IN STD_LOGIC_VECTOR (width_a-1 DOWNTO 0);
			q_b	: OUT STD_LOGIC_VECTOR (width_b-1 DOWNTO 0);
			wren_a	: IN STD_LOGIC ;
			address_b	: IN STD_LOGIC_VECTOR (TileIndexWidth-1 DOWNTO 0);
			byteena_a	: IN STD_LOGIC_VECTOR (width_byteena_a-1 DOWNTO 0);
			clocken0	: IN STD_LOGIC 
	);
	END COMPONENT;
	
	constant clearingMaxValue : std_logic_vector := std_logic_vector(to_unsigned(Tiles/32, IdentifierWidth));
	
	
	signal clearing, write_enable : std_logic;
	signal clearing_index : std_logic_vector(IdentifierWidth-1 downto 0);
	signal read_address, write_address : std_logic_vector(TileIndexWidth-1 downto 0);
	signal read_data, write_data, clear_mask : std_logic_vector(31 downto 0);
	signal tile_index : integer range 0 to 31;
	
	
begin
	
	-- Switch inputs/outputs depending on whether we are clearing
	read_address <= readdress(readdress'high downto readdress'high-TileIndexWidth+1);

	write_address <= wraddress(wraddress'high downto wraddress'high-TileIndexWidth+1) when clearing = '0'
		else clearing_index(clearing_index'high downto clearing_index'high-TileIndexWidth+1);

	write_enable <= write when clearing = '0'
		else clearing;

	write_data <= read_data or clear_mask when clearing = '0'
		else (others => '0');			

	tile_index <= to_integer(unsigned(read_address));
	
	decoderProcess : process(tile_index)
	begin
		clear_mask             <= (others => '0');
		clear_mask(tile_index) <= '1';
	end process decoderProcess;

	tile_empty <= read_data(tile_index);
	
	clearProcess : process (clk) is	
	begin
		if rising_edge(clk) then
			if clear = '1' and clearing = '1' then
				if clearing_index = clearingMaxValue then
					clearing_index <= (others => '0');
					clearing <= '0';
					empty <= '1';
				else
					clearing_index <= std_logic_vector(unsigned(clearing_index)+1);					
				end if;
			elsif clear = '1' then
				clearing <= '0';
			end if;			
		end if;
	end process clearProcess;
		
	altsyncram_component : altsyncram
	GENERIC MAP (
		address_reg_b => "CLOCK0",
		clock_enable_input_a => "BYPASS",
		clock_enable_input_b => "BYPASS",
		clock_enable_output_a => "BYPASS",
		clock_enable_output_b => "BYPASS",
		intended_device_family => "Cyclone II",
		lpm_type => "altsyncram",
		numwords_a => 4800/32/4,
		numwords_b => 4800/32/4,
		operation_mode => "DUAL_PORT",
		outdata_aclr_b => "NONE",
		outdata_reg_b => "CLOCK0",
		power_up_uninitialized => "FALSE",
		read_during_write_mode_mixed_ports => "DONT_CARE",
		widthad_a => TileIndexWidth,
		widthad_b => TileIndexWidth,
		width_a => 32,
		width_b => 32,
		width_byteena_a => 4
	)
	PORT MAP (
		address_a => write_address,
		clock0 => clk,
		data_a => write_data,
		wren_a => write_enable,
		address_b => read_address,
		q_b => read_data,
		clocken0 => '1',
		byteena_a => (others => '1')
	);
	
end architecture RTL;
