library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity ZTransferUnit is
	generic(
		IdentifierWidth : natural := 7;
		BankWidth       : natural := 2;
		BankIndex       : integer := 0;
		PixelChannels   : natural := 2
	);
	port(
		clk                                                                                  : in  std_logic;
		beginReadTransfer, beginWriteTransfer                                                : in  std_logic;
		transferComplete                                                                     : out std_logic;
		cacheReadData                                                                        : in  std_logic_vector(31 downto 0);
		cacheWriteData                                                                       : out std_logic_vector(31 downto 0);
		cacheWriteEnable, tileWrite                                                                     : out std_logic;
		memoryReadData                                                                       : in  std_logic_vector(31 downto 0);
		memoryWriteData                                                                      : out std_logic_vector(31 downto 0);
		tileWriteAddress                                                                     : out std_logic_vector(TileWidth * 2 - 2 downto 0);
		tileReadAddress                                                                      : out std_logic_vector(TileWidth * 2 - 2 downto 0);
		tileIdentifier                                                                       : in  std_logic_vector(IdentifierWidth - 1 downto 0);
		readAddress, writeAddress                                                            : out Address;
		tileEmpty                                                                            : in  std_logic;
		tileClearDepth                                                                       : in  std_logic_vector(15 downto 0);
		requestReadLock, requestWriteLock, beginReadRequest, cacheReadEnabled, read, write : out std_logic := '0';
		readLock, writeLock, readEmpty, writeFull                                            : in  std_logic
	);
end entity ZTransferUnit;

architecture RTL of ZTransferUnit is
begin
	transferProcess : process(clk) is
		type states is (Idle, Reading, ReadRequest, Filling, Writing, Complete, WriteRequest);
		variable currentState                                  : states := Idle;
		variable readCounter, writeCounter, writeCounterOutput : integer range 0 to 31;
	begin
		if rising_edge(clk) then
			case currentState is
				when Idle =>
					transferComplete <= '0';

					if beginWriteTransfer = '1' then
						currentState := WriteRequest;
						requestWriteLock <= '1';
						writeAddress <= tileIdentifier & std_logic_vector(to_unsigned(BankIndex, BankWidth))& std_logic_vector(to_unsigned(0, TileWidth)) & "00";
					elsif beginReadTransfer = '1' then
						if tileEmpty = '1' then
							currentState := Filling;
							tileWrite <= '1';
						else
							currentState    := ReadRequest;
							requestReadLock <= '1';
						end if;
					end if;
				when Filling =>
					tileWrite <= '0';
					tileWriteAddress <= std_logic_vector(to_unsigned(readCounter, 2*TileWidth-1 ));

					for i in 0 to PixelChannels - 1 loop
						cacheWriteData((i + 1) * BPD - 1 downto i * BPD) <= tileClearDepth;
					end loop;

					if readCounter = TileSize * TileSize / PixelChannels - 1 then
						transferComplete <= '1';
						currentState     := Complete;
						readCounter      := 0;
					else
						readCounter := readCounter + 1;
					end if;
				when ReadRequest =>
					if readLock = '1' then
						currentState     := Reading;
						beginReadRequest <= '1';
						readAddress      <= tileIdentifier  & std_logic_vector(to_unsigned(BankIndex, BankWidth))& std_logic_vector(to_unsigned(0, TileWidth)) & "00";
					end if;
				when Reading =>
					beginReadRequest <= '0';

					if readEmpty = '0' then
						cacheWriteData   <= memoryReadData;
						cacheWriteEnable <= '1';
						tileWriteAddress <= std_logic_vector(to_unsigned(readCounter, TileWidth*2-Log2(PixelChannels)));
						read <= '1';
						

						if readCounter = TileSize * TileSize / PixelChannels - 1 then
							transferComplete <= '1';
							currentState     := Complete;
							readCounter      := 0;
							requestReadLock  <= '0';
						else
							readCounter := readCounter + 1;							
						end if;
					else
						read <= '0';
						cacheWriteEnable <= '0';
					end if;
				when WriteRequest =>
					if writeLock = '1' then
						currentState := Writing;
					end if;
				when Writing =>
					if writeFull = '0' then
						cacheReadEnabled <= '1';
						tileReadAddress <= std_logic_vector(to_unsigned(writeCounter, TileWidth*2-Log2(PixelChannels)));

						if writeCounter < TileSize * TileSize / PixelChannels - 1 then
							writeCounter := writeCounter + 1;
						end if;

						if writeCounter > 1 then
							write <= '1';
						
							if writeCounterOutput < TileSize * TileSize / PixelChannels - 1 then
								writeCounterOutput := writeCounterOutput + 1;
							else
								transferComplete   <= '1';
								writeCounter       := 0;
								writeCounterOutput := 0;
								requestWriteLock   <= '0';
								currentState       := Complete;
							end if;

							memoryWriteData <= cacheReadData;

						end if;
					else
						write <= '0';
						cacheReadEnabled <= '0';
					end if;
				when Complete =>
					transferComplete <= '0';
					cacheWriteEnable <= '0';
					read <= '0';
					write <= '0';
					currentState     := Idle;
			end case;
		end if;
	end process transferProcess;

end architecture RTL;
