library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;
use work.ZBufferTypes.all;

entity ZUnit is
	generic (
		TileCount  : natural := 4800;
	    ZBankWidth : natural := 2;
	    ClockWidth : natural := 2;
	    SetWidth   : natural := 4	
	);
	port (
		clk, clear, res: in std_logic;
		iReady, oWaiting : in std_logic;
		iWaiting, oReady, empty, buffer_empty : out std_logic;
		readMemoryAddress, writeMemoryAddress : out Address;
		readMemoryData : in std_logic_vector(31 downto 0);
		writeMemoryData : out std_logic_vector(31 downto 0);
		inputData : in ZBufferCheckType;
		outputData : out ZBufferCheckType;
		read, write, beginRead : out std_logic;
		readEmpty, writeFull : in std_logic;
		clearDepth : in std_logic_vector(BPD-1 downto 0);
		comparisonType : in std_logic_vector(1 downto 0)  
	);
end entity ZUnit;

architecture RTL of ZUnit is
	component ZTestUnit
		generic(ZBufferLatency : natural := 2);
		port(clk, clken            : in  std_logic;
			 iReady, oWaiting      : in  std_logic;
			 oReady, zRequest, zWrite : buffer std_logic := '0';
			 empty                 : out std_logic    := '1';
			 pixelData             : in  ZBufferCheckType;
			 outData               : out ZBufferCheckType;
			 inZBufferFetchAddress : out Address;
			 inZBufferData         : in  ZBufferDataArray(PixelChannels - 1 downto 0);
			 outZBufferSaveAddress : out Address;
			 outZBufferData        : out ZBufferDataArray(PixelChannels - 1 downto 0);
			 comparisonType        : in  std_logic_vector(1 downto 0));
	end component ZTestUnit;
	
	component ZBuffer
		generic(TileCount  : natural := 4800;
			    ZBankWidth : natural := 2;
			    ClockWidth : natural := 2;
			    SetWidth   : natural := 4);
		port(clk, res                                      : in  std_logic;
			 inZRequest, outZRequest                       : in  std_logic;
			 stallZPipeline, read, write, beginRead, empty : out std_logic;
			 inZBufferFetchAddress                         : in  Address;
			 inZBufferData                                 : out ZBufferDataArray(PixelChannels - 1 downto 0);
			 outZBufferSaveAddress                         : in  Address;
			 outZBufferData                                : in  ZBufferDataArray(PixelChannels - 1 downto 0);
			 memoryReadData                                : in  std_logic_vector(PixelChannels * BPD - 1 downto 0);
			 writeMemoryData                               : out std_logic_vector(PixelChannels * BPD - 1 downto 0);
			 readEmpty, writeFull, clear                   : in  std_logic;
			 writeMemoryAddress, readMemoryAddress         : out Address;
			 clear_depth                                   : in  std_logic_vector(BPD - 1 downto 0));
	end component ZBuffer;
		
	signal inZBufferFetchAddress : Address;
	signal outZBufferSaveAddress : Address;
	signal inZBufferData : ZBufferDataArray(PixelChannels - 1 downto 0);
	signal outZBufferData : ZBufferDataArray(PixelChannels - 1 downto 0);
	signal stall, clken : std_logic;
	signal inZRequest : std_logic;
	signal test_empty: std_logic;
	signal outZRequest : std_logic;
	signal zTest_ready : std_logic;
begin
	clken <= not stall;
	empty <= test_empty;
	iWaiting <= clken or not or_reduce(comparisonType);
	oReady <= zTest_ready and clken;
	
	zTestUnitInstance: component ZTestUnit
		generic map(ZBufferLatency => 3)
		port map(clk                   => clk,
			     clken                 => clken,
			     iReady                => iReady,
			     oWaiting              => oWaiting,
			     oReady                => zTest_ready,
			     zRequest              => inZRequest,
			     empty                 => test_empty,
			     pixelData             => inputData,
			     outData               => outputData,
			     inZBufferFetchAddress => inZBufferFetchAddress,
			     inZBufferData         => inZBufferData,
			     outZBufferSaveAddress => outZBufferSaveAddress,
			     outZBufferData        => outZBufferData,
			     comparisonType        => comparisonType,
			     zWrite => outZRequest);
			     
	zBufferInstance : component ZBuffer
		generic map(TileCount  => TileCount,
			        ZBankWidth => ZBankWidth,
			        ClockWidth => ClockWidth,
			        SetWidth   => SetWidth)
		port map(clk                   => clk,
			     res                   => res,
			     inZRequest            => inZRequest,
			     outZRequest           => outZRequest,
			     stallZPipeline        => stall,
			     read                  => read,
			     write                 => write,
			     beginRead             => beginRead,
			     empty                 => buffer_empty,
			     inZBufferFetchAddress => inZBufferFetchAddress,
			     inZBufferData         => inZBufferData,
			     outZBufferSaveAddress => outZBufferSaveAddress,
			     outZBufferData        => outZBufferData,
			     memoryReadData        => readMemoryData,
			     writeMemoryData       => writeMemoryData,
			     readEmpty             => readEmpty,
			     writeFull             => writeFull,
			     clear                 => clear,
			     writeMemoryAddress    => writeMemoryAddress,
			     readMemoryAddress     => readMemoryAddress,
			     clear_depth           => clearDepth);			     
	
end architecture RTL;
