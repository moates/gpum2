library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;
use work.ZBufferTypes.all;

entity ZBufferBank is
	generic(
		TileCount       : natural := 4800;
		BankWidth       : natural := 2;
		SetWidth        : natural := 4;
		PixelChannels   : natural := 2;
		IdentifierWidth : natural := 10;
		ClockWidth      : natural := 2;
		BankIndex       : natural := 0
	);
	port(
		clk, res                                                                        : in  std_logic;
		in_enabled, out_enabled                                                           : in  std_logic;
		in_address, out_address                                                           : in  Address;
		readData                                                                        : out ZBufferDataArray(PixelChannels - 1 downto 0);
		writeData                                                                       : in  ZBufferDataArray(PixelChannels - 1 downto 0);
		stallRead, stallWrite, empty                                             : out std_logic := '0';
		memoryReadData                                                                  : in  std_logic_vector(PixelChannels * BPD - 1 downto 0);
		memoryWriteData                                                                 : out std_logic_vector(PixelChannels * BPD - 1 downto 0);

		readAddress, writeAddress                                                       : out Address;
		requestReadLock, requestWriteLock, beginReadRequest, read, write : out std_logic;
		readLock, writeLock, readEmpty, writeFull, clear                                       : in  std_logic;
		tile_clear_depth : in std_logic_vector(BPD-1 downto 0)
	);
end entity ZBufferBank;

architecture RTL of ZBufferBank is
	component ZBufferCacheSet
		generic(IDENTIFIER_WIDTH : natural := 6;
			    SET_WIDTH        : natural := 3;
			    CLOCK_WIDTH      : natural := 2);
		port(clk, res                              : in  std_logic;
			 read, write                           : in  std_logic;
			 stallZBuffer                          : out std_logic;
			 readAddress, writeAddress             : in  std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
			 cacheReadIndex, cacheWriteIndex       : out std_logic_vector(SET_WIDTH - 1 downto 0);
			 beginReadTransfer, beginWriteTransfer : out std_logic;
			 endTransfer                           : in  std_logic;
			 transferIdentifier                    : out std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0));
	end component ZBufferCacheSet;

	component ZBufferBankMemory
		port(byteena_a : IN  STD_LOGIC_VECTOR(3 DOWNTO 0) := (OTHERS => '1');
			 clock     : IN  STD_LOGIC                    := '1';
			 data      : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			 enable    : IN  STD_LOGIC                    := '1';
			 rdaddress : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
			 wraddress : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
			 wren      : IN  STD_LOGIC                    := '0';
			 q         : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
	end component ZBufferBankMemory;

	component ZTransferUnit
		generic(IdentifierWidth : natural := 7;
			    BankWidth       : natural := 2;
			    BankIndex       : integer := 0;
			    PixelChannels   : natural := 2);
		port(clk                                                                                               : in  std_logic;
			 beginReadTransfer, beginWriteTransfer                                                             : in  std_logic;
			 transferComplete                                                                                  : out std_logic;
			 cacheReadData                                                                                     : in  std_logic_vector(31 downto 0);
			 cacheWriteData                                                                                    : out std_logic_vector(31 downto 0);
			 cacheWriteEnable, tileWrite                                                                       : out std_logic;
			 memoryReadData                                                                                    : in  std_logic_vector(31 downto 0);
			 memoryWriteData                                                                                   : out std_logic_vector(31 downto 0);
			 tileWriteAddress                                                                                  : out std_logic_vector(TileWidth * 2 - 2 downto 0);
			 tileReadAddress                                                                                   : out std_logic_vector(TileWidth * 2 - 2 downto 0);
			 tileIdentifier                                                                                    : in  std_logic_vector(IdentifierWidth - 1 downto 0);
			 readAddress, writeAddress                                                                         : out Address;
			 tileEmpty                                                                                         : in  std_logic;
			 tileClearDepth                                                                                    : in  std_logic_vector(15 downto 0);
			 requestReadLock, requestWriteLock, beginReadRequest, cacheReadEnabled, read, write : out std_logic;
			 readLock, writeLock, readEmpty, writeFull                                                         : in  std_logic);
	end component ZTransferUnit;
	
	component ZTileCache
		generic(Tiles         : natural := 1200;
			    IdentifierWidth : natural := 5);
		port(clk                  : in  std_logic;
			 readdress, wraddress : in  std_logic_vector(IdentifierWidth - 1 downto 0);
			 clear, write         : in  std_logic;
			 empty, tile_empty    : out std_logic);
	end component ZTileCache;

	signal stallZBuffer : std_logic := '0';

	signal cacheReadIdentifier, cacheWriteIdentifier          : std_logic_vector(IdentifierWidth - 1 downto 0);
	signal cacheReadIndex, cacheWriteIndex                    : std_logic_vector(SetWidth - 1 downto 0);
	signal beginReadTransfer, beginWriteTransfer, endTransfer : std_logic;
	signal beginReadTransfer_0, beginReadTransfer_1, beginReadTransfer_2: std_logic;
	signal zBufferReadAddress                                 : STD_LOGIC_VECTOR(7 DOWNTO 0);
	signal zBufferWriteAddress                                : STD_LOGIC_VECTOR(7 DOWNTO 0);

	signal write_data_0, write_data_1 : std_logic_vector(31 downto 0);
	signal wren_0, wren_1             : std_logic;
	signal read_data                  : std_logic_vector(31 downto 0);

	signal transfer_cache_wren, cache_wren : std_logic;
	signal transfer_write_data, write_data : std_logic_vector(31 downto 0);
	signal tileWriteAddress                : std_logic_vector(TileWidth * 2 - 2 downto 0);
	signal tileReadAddress                 : std_logic_vector(TileWidth * 2 - 2 downto 0);
	signal transferIdentifier              : std_logic_vector(IdentifierWidth - 1 downto 0);
	signal cache_enable                    : STD_LOGIC;
	signal cacheReadEnabled                : std_logic;
	signal tile_write, tile_empty : std_logic;
	signal stallRestart : std_logic := '0';
	signal stall_address : STD_LOGIC_VECTOR(7 DOWNTO 0);

begin
	stallRead  <= stallZBuffer or stallRestart;
	stallWrite <= stallZBuffer or stallRestart;

	--Control cache mode depending upon these values
	write_data <= transfer_write_data when beginReadTransfer = '1' else write_data_1;
	cache_wren <= transfer_cache_wren when beginReadTransfer = '1' else wren_1;

	zBufferReadAddress   <= stall_address when stallRestart = '1' 
					else cacheReadIndex & tileReadAddress when beginWriteTransfer = '1' 
					else cacheReadIndex & in_address(4 downto 0);
					
	zBufferWriteAddress  <= cacheWriteIndex & tileWriteAddress when beginReadTransfer = '1' else cacheWriteIndex & out_address(4 downto 0);
	cacheReadIdentifier  <= in_address(in_address'high downto in_address'high - IdentifierWidth + 1);
	cacheWriteIdentifier <= out_address(out_address'high downto out_address'high - IdentifierWidth + 1);
	cache_enable         <= cacheReadEnabled when beginWriteTransfer = '1' else '1';
	beginReadTransfer_2 <=  beginReadTransfer_1 and beginReadTransfer;
	
	stallProcess: process(clk) is
		type states is (Running,Stalled,Starting_0);
		variable currentState : states := Running;
		variable read_0, read_1: STD_LOGIC_VECTOR(7 DOWNTO 0);
	begin
		if rising_edge(clk) then
			if beginReadTransfer = '1' then		
				beginReadTransfer_0 <= beginReadTransfer;
				beginReadTransfer_1 <= beginReadTransfer_0;
			else
				beginReadTransfer_0 <= '0';
				beginReadTransfer_1 <= '0';				
			end if;
		
			case currentState is 
				when Running =>
					if stallZBuffer = '0' then
						read_0 := zBufferReadAddress;
						read_1 := read_0;
						stallRestart <= '0';
					else
						currentState := Stalled;
						stallRestart <= '1';
					end if;
				when Stalled =>
					if stallZBuffer = '0' then
						stall_address <= read_0;
						currentState := Starting_0;
					end if;
				when Starting_0 =>
					stall_address <= read_1;
					currentState := Running;				
			end case;
		end if;
	end process stallProcess;

	-- Pipeline process by which we stream data through the pipelined cache
	pipelineProcess : process(clk) is
	begin
		if rising_edge(clk) then
			for i in 0 to PixelChannels - 1 loop
				write_data_0((i + 1) * BPD - 1 downto i * BPD) <= std_logic_vector(writeData(i));
			end loop;
			wren_0 <= out_enabled;

			if stallZBuffer = '0' then
				write_data_1 <= write_data_0;
				wren_1       <= wren_0;
			end if;
		end if;
	end process pipelineProcess;	
	
	readMapping : for i in 0 to PixelChannels - 1 generate
		readData(i) <= unsigned(read_data((i + 1) * BPD - 1 downto i * BPD));
	end generate readMapping;

	cacheSet : component ZBufferCacheSet
		generic map(IDENTIFIER_WIDTH => IdentifierWidth,
			        SET_WIDTH        => SetWidth,
			        CLOCK_WIDTH      => ClockWidth)
		port map(clk                => clk,
			     res                => res,
			     read               => in_enabled,
			     write              => out_enabled,
			     stallZBuffer       => stallZBuffer,
			     readAddress        => cacheReadIdentifier,
			     writeAddress       => cacheWriteIdentifier,
			     cacheReadIndex     => cacheReadIndex,
			     cacheWriteIndex    => cacheWriteIndex,
			     beginReadTransfer  => beginReadTransfer,
			     beginWriteTransfer => beginWriteTransfer,
			     endTransfer        => endTransfer,
			     transferIdentifier => transferIdentifier);

	zBufferMemory : component ZBufferBankMemory
		port map(byteena_a => (others => '1'),
			     clock     => clk,
			     data      => write_data,
			     enable    => cache_enable,
			     rdaddress => zBufferReadAddress,
			     wraddress => zBufferWriteAddress,
			     wren      => cache_wren,
			     q         => read_data);
			     
	zTileCacheInstance : component ZTileCache
		generic map(Tiles           => TileCount / 2**BankWidth,
			        IdentifierWidth => IdentifierWidth)
		port map(clk        => clk,
			     readdress  => transferIdentifier,
			     wraddress  => transferIdentifier,
			     clear      => clear,
			     write      => tile_write,
			     empty      => empty,
			     tile_empty => tile_empty);

	zTransferInstance : component ZTransferUnit
		generic map(IdentifierWidth => IdentifierWidth,
			        BankWidth       => BankWidth,
			        BankIndex       => BankIndex,
			        PixelChannels   => PixelChannels)
		port map(clk                => clk,
			     beginReadTransfer  => beginReadTransfer_2,
			     beginWriteTransfer => beginWriteTransfer,
			     transferComplete   => endTransfer,
			     cacheReadData      => read_data,
			     cacheWriteData     => transfer_write_data,
			     cacheWriteEnable   => transfer_cache_wren,
			     memoryReadData     => memoryReadData,
			     memoryWriteData    => memoryWriteData,
			     tileWriteAddress   => tileWriteAddress,
			     tileReadAddress    => tileReadAddress,
			     tileIdentifier     => transferIdentifier,
			     tileEmpty          => tile_empty,
			     tileClearDepth     => tile_clear_depth,
			     tileWrite 			=> tile_write,
			     requestReadLock    => requestReadLock,
			     requestWriteLock   => requestWriteLock,
			     beginReadRequest   => beginReadRequest,
			     cacheReadEnabled   => cacheReadEnabled,
			     readLock           => readLock,
			     writeLock          => writeLock,
			     readEmpty          => readEmpty,
			     writeFull          => writeFull,
			     readAddress        => readAddress,
			     writeAddress       => writeAddress,
			     read               => read,
			     write              => write);
end architecture RTL;
