library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;
use work.ZBufferTypes.all;

entity ZBuffer is
	generic(
		TileCount  : natural := 4800;
		ZBankWidth : natural := 2;
		ClockWidth : natural := 2;
		SetWidth   : natural := 4
	);
	port(
		clk, res                               : in  std_logic;
		inZRequest, outZRequest                : in  std_logic;
		stallZPipeline, read, write, beginRead, empty : out std_logic;
		inZBufferFetchAddress                  : in  Address;
		inZBufferData                          : out ZBufferDataArray(PixelChannels - 1 downto 0);
		outZBufferSaveAddress                  : in  Address;
		outZBufferData                         : in  ZBufferDataArray(PixelChannels - 1 downto 0);
		memoryReadData                         : in  std_logic_vector(PixelChannels * BPD - 1 downto 0);
		writeMemoryData                        : out std_logic_vector(PixelChannels * BPD - 1 downto 0);
		readEmpty, writeFull, clear     : in  std_logic;
		writeMemoryAddress, readMemoryAddress  : out Address;
		clear_depth : in std_logic_vector(BPD-1 downto 0)
	);
end entity ZBuffer;

architecture RTL of ZBuffer is
	component ZBufferBank
		generic(TileCount       : natural := 4800;
			    BankWidth       : natural := 2;
			    SetWidth        : natural := 4;
			    PixelChannels   : natural := 2;
			    IdentifierWidth : natural := 10;
			    ClockWidth      : natural := 2;
			    BankIndex       : natural := 0);
		port(clk, res                                                         : in  std_logic;
			 in_enabled, out_enabled                                            : in  std_logic;
			 in_address, out_address                                            : in  Address;
			 readData                                                         : out ZBufferDataArray(PixelChannels - 1 downto 0);
			 writeData                                                        : in  ZBufferDataArray(PixelChannels - 1 downto 0);
			 stallRead, stallWrite,  empty                                           : out std_logic;
			 memoryReadData                                                   : in  std_logic_vector(PixelChannels * BPD - 1 downto 0);
			 memoryWriteData                                                  : out std_logic_vector(PixelChannels * BPD - 1 downto 0);
			 readAddress, writeAddress                                        : out Address;
			 requestReadLock, requestWriteLock, beginReadRequest, read, write : out std_logic;
			 readLock, writeLock, readEmpty, writeFull, clear                 : in  std_logic;
			 tile_clear_depth : std_logic_vector(BPD-1 downto 0));
	end component ZBufferBank;

	type MemoryBusMuxType is array (integer range <>) of std_logic_vector(PixelChannels * BPD - 1 downto 0);
	type MemoryAddressMuxType is array (integer range <>) of Address;
	type ZBufferDataMuxType is array (integer range <>) of ZBufferDataArray(PixelChannels - 1 downto 0);
	signal memoryWriteBusMux               : MemoryBusMuxType(2 ** ZBankWidth - 1 downto 0);
	signal writeAddressMux, readAddressMux : MemoryAddressMuxType(2 ** ZBankWidth - 1 downto 0);
	signal zBufferDataMux                  : ZBufferDataMuxType(2 ** ZBankWidth - 1 downto 0);

	signal stallIn, stallOut, requestReadLock, requestWriteLock, readMux, writeMux, beginReadRequestMux, emptyMux : std_logic_vector(2 ** ZBankWidth - 1 downto 0);
	constant IdentifierWidth                                                                            : natural                                    := BusWidth - ZBankWidth - 2 * TileWidth + 1;
	signal readLockIndex, writeLockIndex                                                                : integer range 2 ** ZBankWidth - 1 downto 0 := 0;
	signal inZBufferIndex, outZBufferIndex                                                              : integer range 2 ** ZBankWidth - 1 downto 0 := 0;
begin
	stallZPipeline <= (or_reduce(stallIn)) or (or_reduce(stallOut));

	activeTransferScheduler : process(clk) is
	begin
		if rising_edge(clk) then
			if requestReadLock(readLockIndex) = '0' then
				for i in 0 to 2 ** ZBankWidth - 1 loop
					if requestReadLock(i) = '1' then
						readLockIndex <= i;
					end if;
					exit;
				end loop;
			end if;

			if requestWriteLock(writeLockIndex) = '0' then
				for i in 0 to 2 ** ZBankWidth - 1 loop
					if requestReadLock(i) = '1' then
						writeLockIndex <= i;
					end if;
					exit;
				end loop;
			end if;
		end if;
	end process activeTransferScheduler;

	read      <= readMux(readLockIndex);
	write     <= writeMux(writeLockIndex);
	beginRead <= beginReadRequestMux(readLockIndex);

	writeMemoryData <= memoryWriteBusMux(readLockIndex);

	writeMemoryAddress <= writeAddressMux(readLockIndex);
	readMemoryAddress  <= readAddressMux(readLockIndex);
	inZBufferIndex     <= to_integer(unsigned(inZBufferFetchAddress(ZBankWidth + TileWidth * 2 - 2 downto TileWidth * 2 - 1)));
	outZBufferIndex    <= to_integer(unsigned(outZBufferSaveAddress(ZBankWidth + TileWidth * 2 - 2 downto TileWidth * 2 - 1)));
	inZBufferData      <= zBufferDataMux(inZBufferIndex);
	empty <= and_reduce(emptyMux);

	zBanks : for i in 0 to 2 ** ZBankWidth - 1 generate
		signal inEnabled, outEnabled : std_logic := '0';
		signal readLock, writeLock   : std_logic;
	begin
		inEnabled <= '1' when inZBufferIndex = i and inZRequest = '1'
			else '0';
		outEnabled <= '1' when outZBufferIndex = i and outZRequest = '1'
			else '0';

		readLock  <= '1' when readLockIndex = i else '0';
		writeLock <= '1' when writeLockIndex = i else '0';

		zInstance : component ZBufferBank
			generic map(TileCount       => TileCount,
				        BankWidth       => ZBankWidth,
				        SetWidth        => SetWidth,
				        PixelChannels   => PixelChannels,
				        IdentifierWidth => IdentifierWidth,
				        ClockWidth      => ClockWidth,
				        BankIndex       => i)
			port map(clk              => clk,
				     res              => res,
				     in_enabled        => inEnabled,
				     out_enabled       => outEnabled,
				     in_address        => inZBufferFetchAddress,
				     out_address       => outZBufferSaveAddress,
				     readData         => zBufferDataMux(i),
				     writeData        => outZBufferData,
				     stallRead        => stallIn(i),
				     stallWrite       => stallOut(i),
				     memoryReadData   => memoryReadData,
				     memoryWriteData  => memoryWriteBusMux(i),
				     requestReadLock  => requestReadLock(i),
				     requestWriteLock => requestWriteLock(i),
				     beginReadRequest => beginReadRequestMux(i),
				     readLock         => readLock,
				     writeLock        => writeLock,
				     readEmpty        => readEmpty,
				     writeFull        => writeFull,
				     readAddress      => readAddressMux(i),
				     writeAddress     => writeAddressMux(i),
				     read             => readMux(i),
				     write            => writeMux(i),
				     empty            => emptyMux(i),
				     clear			  => clear,
				     tile_clear_depth => clear_depth);
	end generate zBanks;

end architecture RTL;
