library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;
use work.ZBufferTypes.all;

entity ZTestUnit is
	generic(
		ZBufferLatency : natural := 2
	);
	port(
		clk, clken          : in  std_logic;
		iReady, oWaiting    : in  std_logic;
		oReady, zRequest, zWrite : buffer std_logic := '0';
		empty               : out std_logic    := '1';
		pixelData           : in  ZBufferCheckType;
		outData             : out ZBufferCheckType;
		inZBufferFetchAddress : out Address;
		inZBufferData         : in  ZBufferDataArray(PixelChannels - 1 downto 0);
		outZBufferSaveAddress : out Address;
		outZBufferData		  : out ZBufferDataArray(PixelChannels - 1 downto 0);
		comparisonType      : in  std_logic_vector(1 downto 0)
	);
end entity;

architecture RTL of ZTestUnit is
	function Mul16(a : InterpolationElement; b : DepthElement) return DepthElement is
		variable temporaryElement : unsigned(INS + BPD - 1 downto 0) := (others => '0');
	begin
		temporaryElement := a * b;

		return temporaryElement(temporaryElement'high downto temporaryElement'high - BPD + 1);
	end function Mul16;

begin
	name : process(clk) is
		constant pipelineStages : natural                                      := ZBufferLatency + 3;
		constant Stage_Disabled : std_logic_vector(PixelChannels - 1 downto 0) := (others => '0');

		type pipelineData is array (pipelineStages - 1 downto 0) of ZBufferCheckType;

		variable pipeline       : pipelineData;
		variable pipelineActive : std_logic_vector(pipelineStages - 1 downto 0) := (others => '0');
		variable contents       : integer range 0 to pipelineStages := 0;

		type zDepthsArrayType is array (PixelChannels - 1 downto 0) of DepthElement;
		variable calculatedZDepths : zDepthsArrayType := (others => (others => '0'));	
		variable zBufferData : ZBufferDataArray(PixelChannels - 1 downto 0) := (others => (others => '0'));	
	begin
		if rising_edge(clk) then
			if clken = '1' or comparisonType = "00" then

				-- push along pipeline						
				for i in pipelineStages - 1 downto 1 loop
					pipeline(i)       := pipeline(i - 1);
					pipelineActive(i) := pipelineActive(i - 1);
				end loop;

				-- Perform depth comparison
				for i in 0 to PixelChannels - 1 loop
					case comparisonType is
						when "00" =>     -- Disabled
							null;
						when "01" => 	-- LT
							-- If the pixel is disabled or gte the zbuffer we diable the pixel and take the in data to write to
							-- the output
							if calculatedZDepths(i) >= inZBufferData(i) or pipeline(2+ZBufferLatency).Enabled(i) = '0' then
								pipeline(2+ZBufferLatency).Enabled(i) := '0';
								zBufferData(i) := inZBufferData(i);								
							else
							--Else we use this pixel in the result set
								zBufferData(i) := calculatedZDepths(i); 
							end if;
						
						when others =>
							null;
					end case;
				end loop;

				--Calculate zDepths for next pipeline iteration
				--Accurate to 16 bits
				for i in 0 to PixelChannels - 1 loop
					calculatedZDepths(i) := Mul16(pipeline(2).Coords(i).B0, pipeline(2).Attributes.z0) + Mul16(pipeline(2).Coords(i).B1, pipeline(2).Attributes.z1) + Mul16(pipeline(2).Coords(i).B2, pipeline(2).Attributes.z2);
				end loop;

				--Take inputs and assign outputs

				pipeline(0)       := pixelData;
				pipelineActive(0)   := iReady;
				
				--Only request data if zbuffer is enabled
				zRequest            <= pipelineActive(1) and (or_reduce(comparisonType));
				zWrite 				<= pipelineActive(2+ZBufferLatency) and (or_reduce(comparisonType));
				inZBufferFetchAddress <= pipeline(1).PixelAddress;
				outZBufferSaveAddress <= pipeline(2+ZBufferLatency).PixelAddress;
				outZBufferData <= zBufferData;
				outData <= pipeline(2 + ZBufferLatency);

				if iReady = '1' then					
					contents          := contents + 1;
				end if;

			elsif oReady = '1' and oWaiting = '1' then

				-- If pipeline has stalled only issue pixel write once
				pipelineActive(2 + ZBufferLatency) := '0';
			end if;

			--Set ready flag etc if the current output stage is active
			if pipelineActive(2 + ZBufferLatency) = '1' then
				contents := contents - 1;

				if pipeline(2 + ZBufferLatency).Enabled /= Stage_Disabled then
					oReady <= '1';
				end if;
			else
				oReady <= '0';
			end if;

			if contents = 0 then
				empty <= '1';
			else
				empty <= '0';
			end if;
		end if;
	end process name;

end architecture RTL;
