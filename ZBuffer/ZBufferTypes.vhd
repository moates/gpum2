library ieee;
use ieee.std_logic_1164.all;

use work.GpuTypes.all;

package ZBufferTypes is
	constant ZBufferPipelineWidth : natural := 1;
	constant PixelChannels : natural := 2;
	
	type ZBufferDataArray is array(integer range <>) of DepthElement;
	type CoordinateArray is array(PixelChannels-1 downto 0) of InterpolationCoords;

	type ZBufferCheckType is record
		PixelAddress : Address;
		Enabled		 : std_logic_vector(2**ZBufferPipelineWidth-1 downto 0);
		Coords		 : CoordinateArray;
		Attributes   : TriangleAttributes;
		DataEnabled  : std_logic;
	end record ZBufferCheckType;
	 

end package ZBufferTypes;

package body ZBufferTypes is
end package body ZBufferTypes;
