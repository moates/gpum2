library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity ZBufferCacheSet is
	generic(
		IDENTIFIER_WIDTH : natural := 6;
		SET_WIDTH        : natural := 3;
		CLOCK_WIDTH      : natural := 2
	);

	port(
		clk, res : in std_logic;
		read, write : in std_logic;
		stallZBuffer : out std_logic := '0';
		readAddress, writeAddress : in std_logic_vector(IDENTIFIER_WIDTH-1 downto 0);
		cacheReadIndex, cacheWriteIndex : out std_logic_vector(SET_WIDTH-1 downto 0);
		beginReadTransfer, beginWriteTransfer : out std_logic := '0';	
		endTransfer : in std_logic;
		transferIdentifier : out std_logic_vector(IDENTIFIER_WIDTH-1 downto 0)
	);
end ZBufferCacheSet;

architecture ZBufferCacheSet_RTL of ZBufferCacheSet is
	type Page is record
		used  : std_logic;
		clock : integer range 2 ** CLOCK_WIDTH - 1 downto 0;
		addr  : std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
	end record;

	constant SET_SIZE : natural := 2 ** SET_WIDTH;

	type Set is array (SET_SIZE - 1 downto 0) of Page;
	type state is (Idle, WaitingRead, WaitingWrite, WaitingReadStart, WaitingWriteStart);
	signal CacheSet     : Set       := (others => (used => '0', clock => 0, addr => (others => '0')));
	signal Clock        : integer range 0 to SET_SIZE - 1;

begin                                   -- OutputCacheSet_RTL

	process(clk)
		variable index, readIndex, writeIndex : integer range 0 to SET_SIZE - 1 := 0;
		variable idleTimer					  : integer range 0 to 3 := 0;
		variable found, stall, clear          : boolean := false;
		variable ready, missRead, missWrite     : boolean := true;
		variable used                         : integer range 0 to SET_SIZE;
		variable inputAddress                 : std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
		variable currentState                 : state   := Idle;
	begin                               -- process      
		if clk'event and clk = '1' then -- rising clock edge			
			case currentState is
				when Idle =>
					missRead := false;
					missWrite := false;
					
					-- If we are idle, count down, if we have been idle for 3 cycles we should
					-- start a transfer back to main ram if the cache is sufficently loaded
					if read = '0' and write = '0' then
						if idleTimer = 3 then
							if clear then
								currentState := WaitingWriteStart;								
							end if;
						else
							idleTimer := idleTimer + 1;
						end if;
					else
					
					
					stall := false;
					idleTimer := 0;

					-- If we are performing a z-buffer cache read, then check if it is currently in the cache
					-- if it is not in the cache, then we need to transfer it into the z-cache
					if read = '1' then
						found := false;

						for i in 0 to SET_SIZE - 1 loop
							if CacheSet(i).addr = readAddress and CacheSet(i).used = '1' then
								readIndex := i;
								found     := true;
								cacheReadIndex <= std_logic_vector(to_unsigned(readIndex, SET_WIDTH));
								exit;
							end if;
						end loop;       -- i

						if found = false then
							missRead := true;
							stall := true;
						end if;
					end if;

					-- Likewise if we are performing a z-buffer cache write, then check if its in the cache
					if write = '1' then
						found := false;

						for i in 0 to SET_SIZE - 1 loop
							if CacheSet(i).addr = writeAddress and CacheSet(i).used = '1' then
								writeIndex := i;
								found      := true;
								cacheWriteIndex <= std_logic_vector(to_unsigned(writeIndex, SET_WIDTH));
								exit;
							end if;
						end loop;       -- i

						if found = false then
							missWrite := true;
							stall := true;
						end if;
					end if;

					-- If we are unable to find the requested index in the cache, we are going to need to transfer it
					-- into ram.
					if stall = true then
						stallZBuffer <= '1';						
						
						-- If the cache is full, first we need to empty the cache (this is going to be slow)
						if used = SET_SIZE then
							currentState := WaitingWriteStart;
							inputAddress := writeAddress;
						else
							currentState := WaitingReadStart;
							inputAddress := readAddress;
						end if;
					else
						stallZBuffer <= '0';
					end if;
				end if;
				when WaitingReadStart =>	
					found := false;
								
					for i in 0 to SET_SIZE - 1 loop
						if CacheSet(i).used = '0' then
							CacheSet(i) <= (used => '1', clock => 2**CLOCK_WIDTH-1, addr => inputAddress);
							found       := true;
							index       := i;

							if used /= SET_SIZE then
								used := used + 1;
							end if;

							if used = SET_SIZE - 1 or used = SET_SIZE then
								clear := true;
							end if;
							exit;
						end if;
					end loop;           -- i

					if found = true then
						currentState := WaitingRead;
						beginReadTransfer <= '1';
						cacheWriteIndex <= std_logic_vector(to_unsigned(index, SET_WIDTH));	
						transferIdentifier <= inputAddress;			
						
						if missRead then
							cacheReadIndex <= std_logic_vector(to_unsigned(readIndex, SET_WIDTH));
						end if;
						
						if missWrite then
							cacheWriteIndex <= std_logic_vector(to_unsigned(writeIndex, SET_WIDTH));
						end if;				
					else
						currentState := WaitingWriteStart;
					end if;
				when WaitingWriteStart => 
					if CacheSet(clock).clock = 0 then
						beginWriteTransfer <= '1';
						cacheReadIndex <= std_logic_vector(to_unsigned(clock, SET_WIDTH));
						CacheSet(clock).used <= '0';
						transferIdentifier <= CacheSet(clock).addr;
						currentState := WaitingWrite;
						used := used - 1;
						clear := false;
					end if;	
				when WaitingRead =>
					if endTransfer = '1' then
						beginReadTransfer <= '0';
						currentState := Idle;						
					end if;
				when WaitingWrite =>
					if endTransfer = '1' then
						beginWriteTransfer <= '0';	
						currentState := Idle;
					end if;
			end case;
			
			if CacheSet(clock).clock /= 0 or CacheSet(clock).used = '1' then
				if CacheSet(clock).clock /= 0 then
						CacheSet(clock).clock <= CacheSet(clock).clock - 1;					
				
						if clock = SET_SIZE - 1 then
							clock <= 0;
						else
							clock <= clock + 1;
						end if;
				end if;
			end if;
		end if;
end process;
end ZBufferCacheSet_RTL;
