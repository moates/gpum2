library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SSRAMPipeline is

  
  port (
    ssram_data                             : inout  std_logic_vector(31 downto 0);
    ssram_address                          : out    std_logic_vector(18 downto 0);  -- Address lines to ssram
    vga_out                                : out    std_logic_vector(127 downto 0);
    gpu_data_in                            : in     std_logic_vector(127 downto 0);
    gpu_data_be                            : in     std_logic_vector(7 downto 0);
    vga_address                            : in     std_logic_vector(16 downto 0);
    gpu_address                            : in     std_logic_vector(16 downto 0);
    gpu_rw                                 : in     std_logic;
    ssram_ce1_n, ssram_ce2, ssram_ce3_n    : out    std_logic;
    ssram_gwe, ssram_bwe                   : out    std_logic;
    ssram_adsp, ssram_adsc                 : out    std_logic;
    ssram_bw                               : out    std_logic_vector(3 downto 0);
    ssram_adv, ssram_oe                    : out    std_logic;
    clk, res, vga_waiting, gpu_write_ready : in     std_logic;
    vga_ready 							   : buffer std_logic;
    gpu_write_waiting                      : buffer std_logic := '1');
end SSRAMPipeline;

architecture SSRAMPipeline_RTL of SSRAMPipeline is
  

  type DeviceType is (dt_Vga, dt_Gpu, dt_None);

  type OperandType is (op_Read, op_Write, op_None);

  type PipelineStage is record
    operand : OperandType;
    address : std_logic_vector(18 downto 0);
    data    : std_logic_vector(31 downto 0);
    be      : std_logic_vector(3 downto 0);
    device  : DeviceType;
  end record;

  type PipelineType is array (4 downto 0) of PipelineStage;

  signal pipeline : PipelineType := (others => (operand => op_None, address => (others => '0'), data => (others => '0'), device => dt_None, be => (others => '0')));
  signal vga_ack, gpu_ack : std_logic := '0';
begin  -- SSRAMPipeline_RTL

  pipelineProcess : process (clk, res)
    variable operation, lastOp                  : OperandType := op_None;  -- Last operation - read/write
    variable device                             : DeviceType  := dt_None;
    variable input_step, output_step, wait_step : integer range 0 to 3;
    variable address                            : std_logic_vector(16 downto 0);
    variable input_data, output_data            : std_logic_vector(127 downto 0);
    variable inc_address                        : std_logic_vector(16 downto 0);
    variable input_be                           : std_logic_vector(7 downto 0);
    variable clean                              : std_logic   := '1';
  begin  -- process pipelineProcess
    if res = '0' then                   -- asynchronous reset (active low)
      vga_ready         <= '0';
      --gpu_write_waiting <= '0';
      vga_ack <= '0';
      gpu_ack <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      if vga_ready = '1' and vga_waiting = '1' then
        vga_ready <= '0';
        vga_ack <= '0';
      end if;

      if gpu_write_waiting = '1' and gpu_write_ready = '1' then
        gpu_write_waiting <= '0';
        gpu_ack <= '0';
      end if;

      -------------------------------------------------------------------------
      -- Set current operaiton
      -------------------------------------------------------------------------
      if operation = op_None then
        if vga_waiting = '1' and vga_ack = '0' then
          operation := op_Read;
          device    := dt_Vga;
          address   := vga_address;
          vga_ack <= '1';
        elsif gpu_write_ready = '1' and gpu_ack = '0' and vga_waiting = '0' then
          operation  := op_Write;
          device     := dt_Gpu;
          address    := gpu_address;
          input_data := gpu_data_in;
          input_be   := gpu_data_be;
          gpu_ack <= '1';
        end if;
      end if;

      if (lastOp = op_Read and operation = op_Write) or (lastOp = op_Write and operation = op_Read) then
        if wait_step = 2 then
          lastOp    := op_None;
          wait_step := 0;
        else
          wait_step := wait_step + 1;
        end if;

        pipeline(4).operand <= op_None;
        pipeline(4).address <= address & std_logic_vector(to_unsigned(input_step, 2));
        pipeline(4).device  <= device;
      else
        -------------------------------------------------------------------------
        -- Create initial pipeline state
        -------------------------------------------------------------------------
        pipeline(4).operand <= operation;
        pipeline(4).address <= address & std_logic_vector(to_unsigned(input_step, 2));
        pipeline(4).device  <= device;


        if operation = op_Write then
          pipeline(4).data <= input_data((input_step+1)*32 - 1 downto input_step*32);
          pipeline(4).be   <= (1|0 => input_be(input_step*2), 3|2 => input_be(input_step*2+1));
        end if;

        if operation /= op_None then
          if input_step = 3 then
            if device = dt_Gpu and operation = op_Write then
              gpu_write_waiting <= '1';
            end if;

            input_step := 0;
            lastOp     := operation;
            operation  := op_None;
            device     := dt_None;
          else
            input_step := input_step + 1;
          end if;
        end if;
      end if;

      if pipeline(0).operand = op_Read then
        output_data((output_step+1)*32-1 downto output_step*32) := pipeline(0).data;

        if output_step = 3 then
          output_step := 0;
          if pipeline(0).device = dt_vga then
            vga_out   <= output_data;
            vga_ready <= '1';
          end if;
        else
          output_step := output_step + 1;
        end if;
      end if;

      -------------------------------------------------------------------------
      -- Advance pipeline
      -------------------------------------------------------------------------
      for i in 1 to 4 loop
        pipeline(i-1) <= pipeline(i);
      end loop;  -- i

      if pipeline(1).operand = op_Read then
        pipeline(0).data <= ssram_data;
      end if;
    end if;
  end process pipelineProcess;

  ssram_ce1_n <= '0';
  ssram_ce2   <= '1';
  ssram_ce3_n <= '0';
  ssram_adsp  <= '1';
  ssram_adv   <= '1';
  ssram_gwe   <= '1';

  outputProcess : process (clk, res)
  begin  -- process outputProcess
    if res = '0' then                   -- asynchronous reset (active low)

    elsif clk'event and clk = '1' then  -- rising clock edge    
      if pipeline(4).operand = op_Read then
        ssram_oe      <= '0';
        ssram_address <= pipeline(4).address;
        ssram_data    <= (others => 'Z');
        ssram_adsc    <= '0';
        ssram_bw      <= (others => '0');
        ssram_bwe     <= '1';
      elsif pipeline(1).operand = op_Write then
        ssram_oe <= '1';
        ssram_data    <= pipeline(1).data;
        ssram_address <= pipeline(1).address;
        ssram_bwe     <= '0';
        ssram_adsc    <= '0';
        ssram_oe <= '1';
		  ssram_bw      <= pipeline(1).be;
      else
        ssram_oe <= '0';
        ssram_bwe     <= '1';
        ssram_data <= (others => 'Z');
        ssram_adsc <= '1';
        ssram_bw <= (others => '0');
      end if;
    end if;
  end process outputProcess;

end SSRAMPipeline_RTL;
