library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity BInterpolator is
  port (
    clk, res : in STD_LOGIC;
    coords : in BarycentricCoords;
    oCoords : out InterpolationCoords;
    clkEnable, enabledIn : in STD_LOGIC;
    enabledOut : out STD_LOGIC
    );
end BInterpolator;

architecture BInterpolator of BInterpolator is

-------------------------------------------------------------------------------
-- Component declarations
-------------------------------------------------------------------------------
  
  component clshift
    port (
      data     : IN  STD_LOGIC_VECTOR (VS-1 DOWNTO 0);
      distance : IN  STD_LOGIC_VECTOR (4 DOWNTO 0);
      result   : OUT STD_LOGIC_VECTOR (VS-1 DOWNTO 0));
  end component;

  component intDiv
    port (
      clock, clken    : IN  STD_LOGIC;
      denom    : IN  STD_LOGIC_VECTOR (7 DOWNTO 0);
      numer    : IN  STD_LOGIC_VECTOR (15 DOWNTO 0);
      quotient : OUT STD_LOGIC_VECTOR (15 DOWNTO 0);
      remain   : OUT STD_LOGIC_VECTOR (7 DOWNTO 0));
  end component;

  signal A0_0, A1_0, A0_1, A1_1, A2_0, A2_1, SUM_0, SUM_1 : STD_LOGIC_VECTOR(VS-1 downto 0);
  signal Shift, Shift_8 : STD_LOGIC_VECTOR(4 downto 0);

  type PipelineType is array (8 downto 0) of STD_LOGIC;
  signal pipeline : PipelineType := (others => '0');
  signal clockenable : STD_LOGIC;
  signal B0_0, B1_0, B2_0 : STD_LOGIC_VECTOR(15 downto 0);  
begin  -- BInterpolator
  enabledOut <= pipeline(0);

  oCoords.B0 <= unsigned(B0_0(7 downto 0));
  oCoords.B1 <= unsigned(B1_0(7 downto 0));
  oCoords.B2 <= unsigned(B2_0(7 downto 0));

  clshift_1: clshift
    port map (
      data     => A0_0,
      distance => Shift,
      result   => A0_1);

  clshift_2: clshift
    port map (
      data     => A1_0,
      distance => Shift,
      result   => A1_1);
      
  clshift_3: clshift
    port map (
      data     => A2_0,
      distance => Shift,
      result   => A2_1);

  clshift_4: clshift
    port map (
      data     => SUM_0,
      distance => Shift,
      result   => SUM_1);

  intDiv_1: intDiv
    port map (
      clock    => clk,
      clken => clkenable,
      denom    => SUM_1(31 downto 24),
      numer    => A0_1(31 downto 16),
      quotient => B0_0,
      remain   => open);

  intDiv_2: intDiv
    port map (
      clock    => clk,
      clken => clkenable,
      denom    => SUM_1(31 downto 24),
      numer    => A1_1(31 downto 16),
      quotient => B1_0,
      remain   => open);

  intDiv_3: intDiv
    port map (
      clock    => clk,
      clken => clkenable,
      denom    => SUM_1(31 downto 24),
      numer    => A2_1(31 downto 16),
      quotient => B2_0,
      remain   => open);

  interpolationPipelineProcess: process (clk, res,clkenable)
    variable sum : unsigned(VS-1 downto 0);
  begin  -- process clshiftProcess      
    if clk'event and clk = '1' then  -- rising clock edge
      if clkenable = '1' then
	      sum := unsigned(coords.A0) + unsigned(coords.A1) + unsigned(coords.A2);
	      SUM_0 <= STD_LOGIC_VECTOR(sum);
	      A0_0 <= std_logic_vector(coords.A0);
	      A1_0 <= std_logic_vector(coords.A1);
	      A2_0 <= std_logic_vector(coords.A2);
	      
	      for i in 31 downto 0 loop
	        if sum(i)='1' then
	          Shift <= std_logic_vector(to_unsigned(31 - i,5));
	--          Shift_8 <= std_logic_vector(to_unsigned(31 - i - 8,5));
	          exit;
	        end if;
	      end loop;  -- i
	
	      for i in 0 to pipeline'high - 1 loop
	        pipeline(i) <= pipeline(i+1);
	      end loop;  -- i
	
	      pipeline(pipeline'high) <= enabledIn;
      end if;
    end if;
  end process interpolationPipelineProcess;

end BInterpolator;
