library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity CollectTriangle is
	port(
		clk                 : in  std_logic;
		rst                 : in  std_logic;
		inV                 : in  Vector4Type;
		inAttributes		: in  VertexPreTransformAttributes;
		Triangle_Unit_Force_Empty : in std_logic;
		outV0, outV1, outV2 : out Vector4Type;
		outAttributes       : out TriangleAttributes;
		iReady, oWaiting    : in  std_logic;
		iWaiting, oReady    : buffer std_logic := '0';
		empty : out std_logic := '1'
	);
end entity CollectTriangle;

architecture RTL of CollectTriangle is
begin
	collectVertexes : process(clk) is
		variable collect : integer range 2 downto 0 := 0;
		variable ready   : boolean                  := true;
		variable temp	 : unsigned(BPC downto 0);
	begin
		if rising_edge(clk) then
			if oWaiting = '1' and oReady = '1' then
				oReady <= '0';
				ready := true;
				empty <= '1';
			end if; 
		
			if Triangle_Unit_Force_Empty = '1' then
				collect := 0;
				empty <= '1';
				ready := true;
			elsif iReady = '1' and ready then
				case collect is
					when 0 =>
						outV0   <= inV;
						temp := "0"&inAttributes.r;
						outAttributes.r0 <= signed(temp);
						temp := "0"&inAttributes.g;
						outAttributes.g0 <= signed(temp);
						temp := "0"&inAttributes.b;
						outAttributes.b0 <= signed(temp);										
						
						outAttributes.z0 <= inV.z(15 downto 0);
						collect := 1;
						empty <= '0';
					when 1 =>
						outV2   <= inV;
						temp := "0"&inAttributes.r;											
						outAttributes.r2 <= signed(temp);																	
						temp := "0"&inAttributes.g;
						outAttributes.g2 <= signed(temp);
						temp := "0"&inAttributes.b;
						outAttributes.b2 <= signed(temp);						
						outAttributes.z2 <= inV.z(15 downto 0);
						
						collect := 2;
					when 2 =>
						outV1   <= inV;
						temp := "0"&inAttributes.r;
						outAttributes.r1 <= signed(temp);
						temp := "0"&inAttributes.g;
						outAttributes.g1 <= signed(temp);
						temp := "0"&inAttributes.b;						
						outAttributes.b1 <= signed(temp);						
						outAttributes.z1 <= inV.z(15 downto 0);
						
						collect := 0;
						oReady  <= '1';
						ready := false;
				end case;
			end if;

			if ready then
				iWaiting <= '1';
			else
				iWaiting <= '0';
			end if;
		end if;
	end process collectVertexes;

end architecture RTL;
