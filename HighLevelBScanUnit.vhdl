library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GPUTypes.all;


entity HighLevelBscanUnit is
  
  port (
    clk, res : in std_logic;
    triangle : in TriangleType;
    bounding : in BoundingBoxType;
    iReady, oWaiting : in std_logic;
    oReady, iWaiting : buffer std_logic
    );

end HighLevelBscanUnit;

architecture HighLevelBScanUnit_RTL of HighLevelBscanUnit is
type states is (idle,scan, scan0);
begin  -- HighLevelBScanUnit_RTL

  clockProcess: process (clk, res)
    variable currentTriangle : TriangleType;
    variable currentBoundingBox : BoundingBoxType;
    variable currentState : states := idle;
    variable A0, A1, A2, suA0, suA1, suA2, sdA0, sdA1, sdA2 : VectorElement;
    variable clipCodes : bit_vector(2 downto 0);
    variable savedUp, saveDown : bit := false;
  begin  -- process clockProcess
    if res = '0' then                   -- asynchronous reset (active low)
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      case currentState is
        when idle =>
          if iReady='1' then
            currentTriangle := triangle;
            currentBoundingBox := bounding;            
            currentState := scan;

            A0 := Mul32(currentBoundingBox.xMin,currentTriangle.a0) + Mul32(currentBoundingBox.yMin,currentTriangle.b0) + currentTriangle.g0 ;
            A1 := Mul32(currentBoundingBox.xMin,currentTriangle.a1) + Mul32(currentBoundingBox.yMin,currentTriangle.b1) + currentTriangle.g0 ;
            A2 := Mul32(currentBoundingBox.xMin,currentTriangle.a2) + Mul32(currentBoundingBox.yMin,currentTriangle.b2) + currentTriangle.g0 ;                        
          end if;          
        when scan0 =>
          clipCodes := (others => '0');

          --clipCodes(0) := A0 + currentTriangle.a0 >= 0 | A0 + currentTriangle.a0 + currentTriangle.b0 >= 0;
          --clipCodes(1) := A1 + currentTriangle.a1 >= 0 | A1 + currentTriangle.a0 + currentTriangle.b1 >= 0;
          --clipCodes(2) := A2 + currentTriangle.a2 >= 0 | A2 + currentTriangle.a2 + currentTriangle.b2 >= 0; 

          if clipCodes(0) or clipCodes(1) or clipCodes(2) then
          -- Export
          else
            if savedUp then
              A0 := suA0;
              A1 := suA1;
              A2 := suA2;
            elsif saveDown then
              A0 := sdA0;
              A1 := sdA1;
              A2 := sdA2;
            end if;
          end if;
          
        when others => null;
      end case;
    end if;
  end process clockProcess;

end HighLevelBScanUnit_RTL;
