-------------------------------------------------------------------------------
-- Title      : Barycentric scan unit
-- Project    : 
-------------------------------------------------------------------------------
-- File       : BScanUnit.vhdl
-- Author     : Michael Oates  <michael@michael-desktop>
-- Company    : 
-- Created    : 2013-08-14
-- Last update: 2013-08-15
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: Scans the screen using barycentric coordinates
-------------------------------------------------------------------------------
-- Copyright (c) 2013 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2013-08-14  1.0      michael Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity BScanUnit is
  generic (
  	TILE_WIDTH : natural := 3;
    scanWidth : natural := 1;
    scanHeight : natural := 1
    );
  
  port (
    clk, res         : in     std_logic;
    iReady, oWaiting : in     std_logic;
    iWaiting, oReady : buffer std_logic;
    triangle         : in     TriangleType;
    startCoords		 : in BarycentricCoords;
    barycentricAreas : out BarycentricCoordsArray(scanWidth*scanHeight-1 downto 0);
    barycentricAreasEnabled : out std_logic_vector(scanWidth*scanHeight-1 downto 0);
    inAreaAddress : in Address;
    areaAddress : out Address;
    empty : out std_logic
    );

end BScanUnit;

architecture BScanUnit_RTL of BScanUnit is
  type state is (idle, scan);
  
  constant TILE_SIZE : natural := 2**TILE_WIDTH;
begin  -- BScanUnitRTL

  scanProcess : process (clk, res)
    variable ScanX           : integer range 0 to TILE_SIZE-1;
    variable ScanY           : integer range 0 to TILE_SIZE-1;
    variable currentTriangle : TriangleType;
    variable currentState    : state         := idle;
    variable currentCoords   : BarycentricCoordsArray(scanWidth*scanHeight-1 downto 0) := (others => (others => (others => '0'))) ;
    variable currentEnabled  : std_logic_vector(scanWidth*scanHeight-1 downto 0);
    variable anyEnabled : std_logic;
    variable nextValues, nextRowValues : BarycentricCoords := (others => (others => '0'));
    variable currentAreaAddress : Address;
    variable ready : boolean := true;
    variable finished : boolean := false;
  begin  -- process scanProcess
    if clk'event and clk = '1' then  -- rising clock edge
      if iReady='1' and iWaiting='1' then
        iWaiting <= '0';
      end if;

      if oReady='1' and oWaiting='1' then
        oReady <= '0';
      end if;
      
      case currentState is
        when idle =>
          if iReady = '1' then
            currentTriangle := triangle;
            empty <= '0';
            
            currentState    := scan;
            currentAreaAddress := inAreaAddress;
            ScanX           := 0;
            ScanY           := 0;

            for x in 0 to scanWidth-1 loop              
            for y in 0 to scanHeight-1 loop
              currentCoords(x+y*scanWidth).A0 := startCoords.A0 + Mul32(currentTriangle.a0,x) + Mul32(currentTriangle.b0,y);
              currentCoords(x+y*scanWidth).A1 := startCoords.A1 + Mul32(currentTriangle.a1,x) + Mul32(currentTriangle.b1,y);
              currentCoords(x+y*scanWidth).A2 := startCoords.A2 + Mul32(currentTriangle.a2,x) + Mul32(currentTriangle.b2,y);
            end loop;  -- i
            end loop;
            
            nextValues.A0(31 downto 0) := Mul32(currentTriangle.a0,scanWidth);
            --nextValues.A0(VS+9 downto 32) := (others => nextValues.A0(31));
			nextValues.A1(31 downto 0) := Mul32(currentTriangle.a1,scanWidth);
			--nextValues.A1(VS+9 downto 32) := (others => nextValues.A1(31));
			nextValues.A2(31 downto 0) := Mul32(currentTriangle.a2,scanWidth);
			--nextValues.A2(VS+9 downto 32) := (others => nextValues.A2(31));
			
			nextRowValues.A0(31 downto 0) := Mul32(currentTriangle.b0,scanHeight) - Mul32(currentTriangle.a0,TILE_SIZE-scanWidth);
			--nextRowValues.A0(VS+9 downto 32) := (others => nextValues.A0(31));
			nextRowValues.A1(31 downto 0) := Mul32(currentTriangle.b1,scanHeight) - Mul32(currentTriangle.a1,TILE_SIZE-scanWidth);
			--nextRowValues.A1(VS+9 downto 32) := (others => nextValues.A1(31));
			nextRowValues.A2(31 downto 0) := Mul32(currentTriangle.b2,scanHeight) - Mul32(currentTriangle.a2,TILE_SIZE-scanWidth);
			--nextRowValues.A2(VS+9 downto 32) := (others => nextValues.A2(31));
			
			iWaiting <= '0';            
          else
            iWaiting <= '1';
            empty <= '1';
          end if;
        when scan =>
          anyEnabled := '0';
          
          for i in 0 to scanWidth*scanHeight-1 loop
            if not (currentCoords(i).A0(31)='1' or currentCoords(i).A1(31)='1' or currentCoords(i).A2(31)='1') then
                      anyEnabled := '1';
                      currentEnabled(i) := '1';
            else
            	currentEnabled(i) := '0';
            end if;
          end loop;  -- i

          if oWaiting = '1' and anyEnabled = '1' then          	
            barycentricAreas <= currentCoords;
            barycentricAreasEnabled <= currentEnabled;
            areaAddress <= std_logic_vector(unsigned(currentAreaAddress) + unsigned(AddrResolve(scanX,scanY)));
            oReady <= '1';
            ready := true;            
          end if;
          
          if ready or anyEnabled = '0' then            	
			ready := false;                                           
            if ScanX = TILE_SIZE-scanWidth then
              if ScanY = TILE_SIZE-scanHeight then
                currentState := idle;
                iWaiting <= '1';
              else                                                         
                ScanY     := ScanY + scanHeight;
                scanX 	  := 0;

                for i in 0 to scanWidth*scanHeight-1 loop                  
                  currentCoords(i).A0 := currentCoords(i).A0 + nextRowValues.A0;
                  currentCoords(i).A1 := currentCoords(i).A1 + nextRowValues.A1;
                  currentCoords(i).A2 := currentCoords(i).A2 + nextRowValues.A2;
                end loop;
              end if;
            else

            for i in 0 to scanWidth*scanHeight-1 loop
            currentCoords(i).A0 := currentCoords(i).A0 + nextValues.A0;
            currentCoords(i).A1 := currentCoords(i).A1 + nextValues.A1;
            currentCoords(i).A2 := currentCoords(i).A2 + nextValues.A2;  
            end loop;  -- i                               
                       -- 
            ScanX := ScanX + scanWidth;
            end if;
          end if;         
      end case;
    end if;
  end process scanProcess;

end BScanUnit_RTL;
