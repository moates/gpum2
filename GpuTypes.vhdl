library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package GpuTypes is

  -- Bits per colour channel
  constant BPC : natural := 5;
  constant BPP : natural := 16;
  constant PPW : natural := 32 / BPP;
  constant BPD : natural := BPP;

  -- Z buffer depth
  constant ZD  : natural := 16;
  constant VS  : natural := 32;
  constant INS : natural := 8;

  -- Screen dimensions
  constant SWidth         : natural := 640;
  constant SHeight        : natural := 480;
  constant TileWidth      : natural := 3;
  constant TileSize       : natural := 2**TileWidth;
  constant SAddressWidth  : natural := 9;
  constant SAddressHeight : natural := 9;

  constant BusWidth : natural := SAddressWidth + SAddressHeight;
  constant FullBusWidth : natural := 19;

  --Transfer constants
  constant BurstWidth         : natural := 128;
  constant ColourBurstWidth   : natural := 128/BPP;
  constant SRAM_ADDRESS_WIDTH : natural := 18;

  subtype VectorElement is signed (VS-1 downto 0);
  subtype LargeVectorElement is signed (VS-1 downto 0);
  subtype ColourElement is unsigned (BPC-1 downto 0);
  subtype DepthElement is unsigned(BPD-1 downto 0);
  subtype Address is std_logic_vector(BusWidth-1 downto 0);
  subtype FullAddress is std_logic_vector(FullBusWidth-1 downto 0);
  subtype InterpolationElement is unsigned(INS-1 downto 0);

  type BoundingBoxType is record
    xMin, xMax, yMin, yMax : VectorElement;
  end record;

  type PackedColourBurst is record
    colour  : std_logic_vector (BurstWidth-1 downto 0);
    enabled : std_logic_vector(ColourBurstWidth-1 downto 0);
    addr    : Address;
  end record;

  type VertexAttributes is record
    z             : unsigned(ZD-1 downto 0);
    r, g, b, u, v : unsigned (BPC-1 downto 0);
  end record;
  
  type VertexPreTransformAttributes is record
    r, g, b, u, v : unsigned (BPC-1 downto 0);
  end record;

  type TriangleAttributes is record
    z0, z1, z2                         : DepthElement;
    r0, r1, r2, g0, g1, g2, b0, b1, b2 : signed(BPC downto 0);
  end record;

  type Vector4Type is record            -- 4 component homogenous vector
    x, y, z, w : VectorElement;
  end record;

  type BarycentricCoords is record
    A0, A1, A2 : LargeVectorElement;
  end record;

  type InterpolationCoords is record
    B0, B1, B2 : InterpolationElement;
  end record;

  type BarycentricCoordsArray is array (integer range <>) of BarycentricCoords;
  
  type std_logic_vector_array is array (integer range <>) of std_logic_vector(31 downto 0);
  subtype InputVertex is std_logic_vector_array(3 downto 0);


  type Colour3Type is record
    r, g, b : ColourElement;
  end record;

 type ColourVectorArray is array(integer range<>) of Colour3Type;

  --  Triangle description in terms of area coordinates
  type TriangleType is record
    a0, b0, g0 : VectorElement;
    a1, b1, g1 : VectorElement;
    a2, b2, g2 : VectorElement;
  end record;
  
  type BoxType is record
  	xMin, xMax : integer range 0 to SWidth;
  	yMin, yMax : integer range 0 to SHeight;
  end record;

  function Mul32 (
    a, b : VectorElement)
    return VectorElement;

  function AddrResolve (
    x : integer range 0 to SWidth-1;
    y : integer range 0 to SHeight-1)
    return Address;
  
  function Mul32 (
    a : VectorElement;
    b : integer)
    return VectorElement;
    
    function Mul42 (
    a : VectorElement;
    b : integer)
    return LargeVectorElement;    
  
  function log2(i : natural)
    return integer;
    
  function and_reduce(s : std_logic_vector)
  	return std_logic;
  	
  function or_reduce(s : std_logic_vector)
  	return std_logic;
  
end GpuTypes;

package body GpuTypes is

  -- purpose: Multiply two 32 bit values, return a 32 bit value
  function Mul32 (
    a, b : VectorElement)
    return VectorElement is
    variable temp : signed(VS*2-1 downto 0);
  begin  -- Mul32
    temp := a * b;
    return temp(VS-1 downto 0);
  end Mul32;
  
  -- purpose: Multiply two 32 bit values, return a 32 bit value
  function Mul42 (
    a : VectorElement;
    b : integer)
    return LargeVectorElement is
    variable temp : signed(VS*2-1 downto 0);
  begin  -- Mul32
    temp := a * b;
    return temp(VS-1 downto 0);
  end Mul42;

  -- purpose: Multiply two 32 bit values, return a 32 bit value
  function Mul32 (
    a : VectorElement;
    b : integer)
    return VectorElement is
    variable temp : signed(VS*2-1 downto 0);
  begin  -- Mul32
    temp := a * b;
    return temp(VS-1 downto 0);
  end Mul32;

  function AddrResolve (
    x : integer range 0 to SWidth-1;
    y : integer range 0 to SHeight-1)
    return Address is
    variable xSLV        : unsigned (SAddressWidth downto 0);
    variable ySLV        : unsigned (SAddressHeight-1 downto 0);
    variable ySLV80      : unsigned (SAddressHeight*2-1 downto 0);
    variable returnValue : Address;
    variable tilePosition : unsigned(TileWidth*2-2 downto 0);
    
  begin  -- AddrResolve
    xSLV := to_unsigned(x, SAddressWidth+1);
    ySLV := to_unsigned(y, SAddressHeight);

    tilePosition := ySLV(TileWidth-1 downto 0) & xSLV(TileWidth-1 downto 1);
    
    ySLV := ySLV / (2**TileWidth);
    xSLV := xSLV / (2**TileWidth);
    
    ySLV80 := ySLV * 80 + xSLV;

    returnValue := std_logic_vector(ySLV80(12 downto 0)) & std_logic_vector(tilePosition);

    return returnValue;
    
  end AddrResolve;

  function log2(i : natural) return integer is
    variable temp    : integer := i;
    variable ret_val : integer := 0;
  begin
    while temp > 1 loop
      ret_val := ret_val + 1;
      temp    := temp / 2;
    end loop;

    return ret_val;
  end function;
  
  function and_reduce (s : std_logic_vector)
  	return std_logic is
  	variable temp : std_logic  := '1';
  begin
  	
  	for i in s'low to s'high loop
  		temp := temp and s(i);
  	end loop;
  	
  	return temp;
  end function and_reduce;
  
  function or_reduce (s : std_logic_vector)
  	return std_logic is
  	variable temp : std_logic  := '0';
  begin
  	
  	for i in s'low to s'high loop
  		temp := temp or s(i);
  	end loop;
  	
  	return temp;
  end function or_reduce;
  

end package body GpuTypes;
