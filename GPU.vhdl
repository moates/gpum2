-------------------------------------------------------------------------------
-- Title      : GPU Top Level
-- Project    : 
-------------------------------------------------------------------------------
-- File       : GPU.vhdl
-- Author     : Michael Oates  <michael@michael-desktop>
-- Company    : 
-- Created    : 2013-08-14
-- Last update: 2013-08-14
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2013-08-14  1.0      michael Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity GPU is
	port(
		VGA_R, VGA_B, VGA_G                             : out   std_logic_vector(9 downto 0);
		VGA_HS, VGA_VS, VGA_CLK, VGA_BLANK, VGA_SYNC    : out   std_logic;
		CLOCK_50                                        : in    std_logic;
		SW                                              : in    std_logic_vector(17 downto 0);

		-- SRAM
		SRAM_DQ                                         : inout std_logic_vector(31 downto 0);
		SRAM_ADDR                                       : out   std_logic_vector(18 downto 0);
		SRAM_CLK, SRAM_ADSC_N, SRAM_ADSP_N, SRAM_ADV_N  : out   std_logic;
		SRAM_BE_N                                       : out   std_logic_vector(3 downto 0);
		SRAM_CE1_N, SRAM_CE2, SRAM_CE3_N                : out   std_logic;
		SRAM_GW_N, SRAM_OE_N, SRAM_WE_N                 : out   std_logic;

		-- LED
		LEDR                                            : out   std_logic_vector(17 downto 0);

		-- DRAM 0
		DRAM0_ADDR                                      : out   std_logic_vector(12 downto 0);
		DRAM0_BA_0, DRAM0_BA_1, DRAM0_CAS_N             : out   std_logic;
		DRAM0_CKE, DRAM0_CLK, DRAM0_CS_N                : out   std_logic;
		DRAM0_LDQM, DRAM0_RAS_N, DRAM0_UDQM, DRAM0_WE_N : out   std_logic;

		-- DRAM 1
		DRAM1_ADDR                                      : out   std_logic_vector(12 downto 0);
		DRAM1_BA_0, DRAM1_BA_1, DRAM1_CAS_N             : out   std_logic;
		DRAM1_CKE, DRAM1_CLK, DRAM1_CS_N                : out   std_logic;
		DRAM1_LDQM, DRAM1_RAS_N, DRAM1_UDQM, DRAM1_WE_N : out   std_logic;

		DRAM_DQ                                         : inout std_logic_vector(31 downto 0)
	);

end GPU;

architecture GPU_RTL of GPU is
	component SSRAM
		port(memory_clk, fpga_clk, vga_clk                               : in    std_logic;
			 ssram_data                                                  : inout std_logic_vector(31 downto 0);
			 ssram_address                                               : out   std_logic_vector(18 downto 0);
			 ssram_ce1_n, ssram_ce2, ssram_ce3_n                         : out   std_logic;
			 ssram_gwe, ssram_bwe                                        : out   std_logic;
			 ssram_adsp, ssram_adsc                                      : out   std_logic;
			 ssram_bw                                                    : out   std_logic_vector(3 downto 0);
			 ssram_adv, ssram_oe                                         : out   std_logic;
			 vga_readAddress                                             : in    Address;
			 vga_readData                                                : out   std_logic_vector(31 downto 0);
			 outputCache_writeData                                       : in    std_logic_vector(31 downto 0);
			 zBuffer_readAddress                                         : in    Address;
			 zBuffer_readData                                            : out   std_logic_vector(31 downto 0);
			 zBuffer_writeData                                           : in    std_logic_vector(31 downto 0);
			 vga_read, vga_request, outputCache_write                    : in    std_logic;
			 zBuffer_read, zBuffer_request, zBuffer_write                : in    std_logic;
			 outputCache_be                                              : in    std_logic_vector(1 downto 0);
			 outputCache_writeAddress                                    : in    Address;
			 vga_readEmpty                                               : out   std_logic;
			 zBuffer_be                                                  : in    std_logic_vector(1 downto 0);
			 zBuffer_writeAddress                                        : in    Address;
			 zBuffer_readEmpty, zBuffer_writeFull, outputCache_writeFull : out   std_logic;
			 activeBuffer : in std_logic);
	end component SSRAM;

	component vga
		generic(groupSize    : integer := 2;
			    bpp          : integer := 16;
			    screenWidth  : integer := 10;
			    screenHeight : integer := 9);
		port(vga_address                            : buffer Address;
			 data                               : in  std_logic_vector(31 downto 0);
			 memory_read, memory_request        : out std_logic;
			 memory_read_empty                  : in  std_logic;
			 pixel                              : out std_logic_vector(bpp - 1 downto 0);
			 clk, res                           : in  std_logic;
			 vsync, hsync, vga_clk, blank, sync : out std_logic);
	end component vga;

	component GPUPipeline
		port(clk, res                                                : in  std_logic;
			 gpu_output, zBuffer_writeData                           : out std_logic_vector(31 downto 0);
			 zBuffer_readData                                        : in  std_logic_vector(31 downto 0);
			 gpu_address, zBuffer_writeAddress, zBuffer_readAddress  : out Address;
			 gpu_enabled, zBuffer_enabled                            : out std_logic_vector(1 downto 0);
			 gpu_write, zBuffer_write, zBuffer_read, zBuffer_request : out std_logic;
			 gpu_writeFull, zBuffer_writeFull, zBuffer_readEmpty     : in  std_logic;
			 gpu_iWaiting                                            : buffer std_logic;
			 gpu_iReady                                              : in  std_logic;
			 gpu_iData                                               : in  std_logic_vector(31 downto 0);
			 active_buffer                                           : out std_logic);
	end component GPUPipeline;

	component ppl
		port(
			inclk0 : IN  STD_LOGIC := '0';
			c0, c1 : OUT STD_LOGIC);
	end component;

	component MicroInterface
		port(clk             : in    std_logic;
			 rst             : in    std_logic;
			 oWaiting        : in    std_logic;
			 oReady          : buffer std_logic                    := '0';
			 instruction     : out   std_logic_vector(31 downto 0);
			 sdram_bus_addr  : out   std_logic_vector(12 downto 0); -- addr
			 sdram_bus_ba    : out   std_logic_vector(1 downto 0); -- ba
			 sdram_bus_cas_n : out   std_logic; -- cas_n
			 sdram_bus_cke   : out   std_logic; -- cke
			 sdram_bus_cs_n  : out   std_logic; -- cs_n
			 sdram_bus_dq    : inout std_logic_vector(31 downto 0) := (others => 'X'); -- dq
			 sdram_bus_dqm   : out   std_logic_vector(3 downto 0); -- dqm
			 sdram_bus_ras_n : out   std_logic; -- ras_n
			 sdram_bus_we_n  : out   std_logic -- we_n
		);
	end component MicroInterface;

	signal outputCache_writeData                                  : std_logic_vector(31 downto 0);
	signal outputCache_writeAddress : Address;
	signal vga_address                                         : Address;
	signal clk, res      : std_logic;
	signal vga_pixel                                           : std_logic_vector(bpp - 1 downto 0);
	signal micro_oWaiting, micro_oReady                        : std_logic;
	signal micro_oData                                         : std_logic_vector(31 downto 0);

	-- DRAM signals  
	signal sdram_bus_addr  : std_logic_vector(12 downto 0); -- addr
	signal sdram_bus_ba    : std_logic_vector(1 downto 0); -- ba
	signal sdram_bus_cas_n : std_logic; -- cas_n
	signal sdram_bus_cke   : std_logic; -- cke
	signal sdram_bus_cs_n  : std_logic; -- cs_n
	signal sdram_bus_dqm   : std_logic_vector(3 downto 0); -- dqm
	signal sdram_bus_ras_n : std_logic; -- ras_n
	signal sdram_bus_we_n  : std_logic; -- we_n
	signal sdram_clk       : std_logic;
	signal zBuffer_writeData : std_logic_vector(31 downto 0);
	signal zBuffer_readData : std_logic_vector(31 downto 0);
	signal zBuffer_writeAddress : Address;
	signal zBuffer_readAddress : Address;
	signal zBuffer_write : std_logic;
	signal zBuffer_read : std_logic;
	signal zBuffer_request : std_logic;
	signal outputCache_write : std_logic;
	signal outputCache_writeFull : std_logic;
	signal zBuffer_writeFull : std_logic;
	signal zBuffer_readEmpty : std_logic;
	signal active_buffer : std_logic;
	signal outputCache_be : std_logic_vector(1 downto 0);
	signal zBuffer_enabled : std_logic_vector(1 downto 0);
	signal vga_read : std_logic;
	signal vga_request : std_logic;
	signal vga_data : std_logic_vector(31 downto 0);
	signal vga_readEmpty : std_logic;

begin                                   -- GPU_RTL

	res      <= SW(0);
	SRAM_CLK <= CLOCK_50;
	clk      <= CLOCK_50;

	DRAM0_ADDR  <= sdram_bus_addr;
	DRAM0_BA_0  <= sdram_bus_ba(0);
	DRAM0_BA_1  <= sdram_bus_ba(1);
	DRAM0_CAS_N <= sdram_bus_cas_n;
	DRAM0_CKE   <= sdram_bus_cke;
	DRAM0_CLK   <= sdram_clk;
	DRAM0_CS_N  <= sdram_bus_cs_n;
	DRAM0_LDQM  <= sdram_bus_dqm(0);
	DRAM0_RAS_N <= sdram_bus_ras_n;
	DRAM0_UDQM  <= sdram_bus_dqm(1);
	DRAM0_WE_N  <= sdram_bus_we_n;

	DRAM1_ADDR  <= sdram_bus_addr;
	DRAM1_BA_0  <= sdram_bus_ba(0);
	DRAM1_BA_1  <= sdram_bus_ba(1);
	DRAM1_CAS_N <= sdram_bus_cas_n;
	DRAM1_CKE   <= sdram_bus_cke;
	DRAM1_CLK   <= sdram_clk;
	DRAM1_CS_N  <= sdram_bus_cs_n;
	DRAM1_LDQM  <= sdram_bus_dqm(2);
	DRAM1_RAS_N <= sdram_bus_ras_n;
	DRAM1_UDQM  <= sdram_bus_dqm(3);
	DRAM1_WE_N  <= sdram_bus_we_n;

	SSRAMInstance: component SSRAM
		port map(memory_clk               => clk,
			     fpga_clk                 => clk,
			     vga_clk 				  => clk,
			     ssram_data               => SRAM_DQ,
			     ssram_address            => SRAM_ADDR,
			     ssram_ce1_n              => SRAM_CE1_N,
			     ssram_ce2                => SRAM_CE2,
			     ssram_ce3_n              => SRAM_CE3_N,
			     ssram_gwe                => SRAM_GW_N,
			     ssram_bwe                => SRAM_WE_N,
			     ssram_adsp               => SRAM_ADSP_N,
			     ssram_adsc               => SRAM_ADSC_N,
			     ssram_bw                 => SRAM_BE_N,
			     ssram_adv                => SRAM_ADV_N,
			     ssram_oe                 => SRAM_OE_N,
			     vga_readAddress          => vga_address,
			     vga_readData             => vga_data,
			     outputCache_writeData    => outputCache_writeData,
			     zBuffer_readAddress      => zBuffer_readAddress,
			     zBuffer_readData         => zBuffer_readData,
			     zBuffer_writeData        => zBuffer_writeData,
			     vga_read                 => vga_read,
			     vga_request              => vga_request,
			     outputCache_write        => outputCache_write,
			     zBuffer_read             => zBuffer_read,
			     zBuffer_request          => zBuffer_request,
			     zBuffer_write            => zBuffer_write,
			     outputCache_be           => outputCache_be,
			     outputCache_writeAddress => outputCache_writeAddress,
			     vga_readEmpty            => vga_readEmpty,
			     zBuffer_be               => zBuffer_enabled,
			     zBuffer_writeAddress     => zBuffer_writeAddress,
			     zBuffer_readEmpty        => zBuffer_readEmpty,
			     zBuffer_writeFull        => zBuffer_writeFull,
			     outputCache_writeFull    => outputCache_writeFull,
			     activeBuffer => active_buffer);


	vga_1 : vga
		generic map(groupSize    =>  ColourBurstWidth,
					bpp          => BPP,
					screenWidth  => SWidth,
					screenHeight => SHeight)
		port map(vga_address           => vga_address,
			     data              => vga_data,
			     memory_read       => vga_read,
			     memory_request    => vga_request,
			     memory_read_empty => vga_readEmpty,
			     pixel             => vga_pixel,
			     clk               => clk,
			     res               => res,
			     vsync             => VGA_VS,
			     hsync             => VGA_HS,
			     vga_clk           => VGA_CLK,
			     blank             => VGA_BLANK,
			     sync              => VGA_SYNC);

	GPUPipeline_1 : GPUPipeline
		port map(clk                  => clk,
			     res                  => res,
			     gpu_output           => outputCache_writeData,
			     zBuffer_writeData    => zBuffer_writeData,
			     zBuffer_readData     => zBuffer_readData,
			     gpu_address          => outputCache_writeAddress,
			     zBuffer_writeAddress => zBuffer_writeAddress,
			     zBuffer_readAddress  => zBuffer_readAddress,
			     gpu_enabled          => outputCache_be,
			     zBuffer_enabled      => zBuffer_enabled,
			     gpu_write            => outputCache_write,
			     zBuffer_write        => zBuffer_write,
			     zBuffer_read         => zBuffer_read,
			     zBuffer_request      => zBuffer_request,
			     gpu_writeFull        => outputCache_writeFull,
			     zBuffer_writeFull    => zBuffer_writeFull,
			     zBuffer_readEmpty    => zBuffer_readEmpty,
			     gpu_iWaiting         => micro_oWaiting,
			     gpu_iReady           => micro_oReady,
			     gpu_iData            => micro_oData,
			     active_buffer        => active_buffer);

	MicroProcessor : component MicroInterface
		port map(clk             => clk,
			     rst             => res,
			     oWaiting        => micro_oWaiting,
			     oReady          => micro_oReady,
			     instruction     => micro_oData,
			     sdram_bus_addr  => sdram_bus_addr, -- addr
			     sdram_bus_ba    => sdram_bus_ba, -- ba
			     sdram_bus_cas_n => sdram_bus_cas_n, -- cas_n
			     sdram_bus_cke   => sdram_bus_cke, -- cke
			     sdram_bus_cs_n  => sdram_bus_cs_n, -- cs_n
			     sdram_bus_dq    => DRAM_DQ, -- dq
			     sdram_bus_dqm   => sdram_bus_dqm, -- dqm
			     sdram_bus_ras_n => sdram_bus_ras_n, -- ras_n
			     sdram_bus_we_n  => sdram_bus_we_n);

	ppl_1 : ppl
		port map(
			inclk0 => CLOCK_50,
			c0     => sdram_clk,
			c1     => open);
			
	

	-----------------------------------------------------------------------------
	-- VGA bit depth mappings - extend the 5 bits to the full range by using as
	-- the upper and lower bytes. This will give a more even mapping
	-----------------------------------------------------------------------------
	VGA_R <= vga_pixel(14 downto 10) & vga_pixel(14 downto 10);
	VGA_G <= vga_pixel(9 downto 5) & vga_pixel(9 downto 5);
	VGA_B <= vga_pixel(4 downto 0) & vga_pixel(4 downto 0);

	ledr(0) <= active_buffer;

end GPU_RTL;
