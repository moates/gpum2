library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

ENTITY OutputBuffer is
  
  generic (
    words      : natural := 2;
    wordWidth  : natural := 1);

  port (
    clk, res         : in     std_logic;
    outputData       : out    PackedColourBurst;
    pixelData        : in     ColourVectorArray(wordWidth*2-1 downto 0);
    pixelAddress     : in     Address;
    pixelsEnabled    : in     std_logic_vector(wordWidth*2-1 downto 0);
    oReady, iWaiting : buffer std_logic;
    iReady, oWaiting : in std_logic
    );
  
end OutputBuffer;

architecture OutputBuffer_RTL of OutputBuffer is
  signal OUTPUT_iWaiting : std_logic := '1';
begin  -- OutputBuffer_RTL

	iWaiting <= OUTPUT_iWaiting;

  process (clk, res)
    variable currentData : PackedColourBurst := (enabled => (others => '0'),  others => (others => '0'));
    variable index : integer range 0 to ColourBurstWidth-1;
    variable colourIndex : integer range 0 to BurstWidth-1;
  begin  -- process      
    if clk'event and clk = '1' then  -- rising clock edge
      if iReady = '1' and iWaiting = '1' then
        OUTPUT_iWaiting <= '0';
      end if;

      if oReady = '1' and oWaiting = '1' then
        oReady <= '0';
        OUTPUT_iWaiting <= '1';
      end if;

      if iReady = '1' then
        if pixelAddress(Address'high downto log2(ColourBurstWidth)-1) = currentData.addr(Address'high downto log2(ColourBurstWidth)-1) then                      
           OUTPUT_iWaiting <= '1';
        else
          outputData <= currentData;
          currentData.addr := pixelAddress;
          currentData.enabled := (others => '0');
          oReady <= '1';
        end if;

        index := to_integer(unsigned(pixelAddress(log2(ColourBurstWidth)-2 downto wordWidth-1)));
        
        for i in 0 to wordWidth*2-1 loop
             colourIndex := (index * 2**wordWidth) + i;
             currentData.colour(colourIndex*16+14 downto colourIndex*16+10) := std_logic_vector(pixelData(i).r);
             currentData.colour(colourIndex*16+9 downto colourIndex*16+5) := std_logic_vector(pixelData(i).g);
             currentData.colour(colourIndex*16+4 downto colourIndex*16) := std_logic_vector(pixelData(i).b);
             currentData.enabled(colourIndex) := pixelsEnabled(i);
           end loop;  -- i
      end if;
        
    end if;
  end process;

end OutputBuffer_RTL;
