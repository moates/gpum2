library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity MicroInterface is
	port(
		clk             : in    std_logic;
		rst             : in    std_logic;
		oWaiting        : in    std_logic;
		oReady          : buffer std_logic                    := '0';
		instruction     : out   std_logic_vector(31 downto 0);
		sdram_bus_addr  : out   std_logic_vector(12 downto 0); -- addr
		sdram_bus_ba    : out   std_logic_vector(1 downto 0); -- ba
		sdram_bus_cas_n : out   std_logic; -- cas_n
		sdram_bus_cke   : out   std_logic; -- cke
		sdram_bus_cs_n  : out   std_logic; -- cs_n
		sdram_bus_dq    : inout std_logic_vector(31 downto 0) := (others => 'X'); -- dq
		sdram_bus_dqm   : out   std_logic_vector(3 downto 0); -- dqm
		sdram_bus_ras_n : out   std_logic; -- ras_n
		sdram_bus_we_n  : out   std_logic -- we_n
	);
end entity MicroInterface;

architecture RTL of MicroInterface is
	component Microcontroller is
		port(
			clk_clk                    : in    std_logic                     := 'X'; -- clk
			gpu_0_gpu_output_data      : out   std_logic_vector(31 downto 0); -- data
			gpu_0_gpu_output_write     : out   std_logic; -- write
			gpu_0_gpu_output_fifo_full : in    std_logic                     := 'X'; -- fifo_full
			gpu_0_gpu_output_size      : in    std_logic_vector(31 downto 0) := (others => 'X'); -- size
			sdram_addr                 : out   std_logic_vector(12 downto 0); -- addr
			sdram_ba                   : out   std_logic_vector(1 downto 0); -- ba
			sdram_cas_n                : out   std_logic; -- cas_n
			sdram_cke                  : out   std_logic; -- cke
			sdram_cs_n                 : out   std_logic; -- cs_n
			sdram_dq                   : inout std_logic_vector(31 downto 0) := (others => 'X'); -- dq
			sdram_dqm                  : out   std_logic_vector(3 downto 0); -- dqm
			sdram_ras_n                : out   std_logic; -- ras_n
			sdram_we_n                 : out   std_logic -- we_n
		);
	end component Microcontroller;

	component InstructionFifo
		port(clock : IN  STD_LOGIC;
			 data  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			 rdreq : IN  STD_LOGIC;
			 wrreq : IN  STD_LOGIC;
			 empty : OUT STD_LOGIC;
			 full  : OUT STD_LOGIC;
			 q     : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
			 usedw : OUT STD_LOGIC_VECTOR(7 DOWNTO 0));
	end component InstructionFifo;

	signal write                 : std_logic;
	signal data, capacity        : std_logic_vector(31 downto 0);
	signal usedw                 : STD_LOGIC_VECTOR(7 DOWNTO 0);
	signal fifo_full : STD_LOGIC;
	signal size                  : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(256, 32));

	type fifo_type is array (255 downto 0) of std_logic_vector(31 downto 0);
	signal fifo_data : fifo_type := (others => (others => '0'));
begin
	capacity <= std_logic_vector(unsigned(size) - unsigned(usedw));

	name : process(clk) is
		variable fifo_read_index, fifo_write_index : integer range 255 downto 0 := 0;
		variable fifo_current_used                 : integer range 256 downto 0 := 0;
	begin
		if rising_edge(clk) then
			if oReady = '1' and oWaiting = '1' then
				if fifo_current_used /= 0 then
					if fifo_read_index = 255 then
						fifo_read_index := 0;
					else
						fifo_read_index := fifo_read_index + 1;
					end if;

					fifo_current_used := fifo_current_used - 1;
				end if;
				
				oReady <= '0';
			end if;

			if write = '1' and fifo_current_used /= 255 then
				fifo_data(fifo_write_index) <= data;
				fifo_current_used           := fifo_current_used + 1;

				if fifo_read_index = fifo_write_index then
					instruction <= data;
				else
					instruction <= fifo_data(fifo_read_index);
				end if;

				if fifo_write_index = 255 then
					fifo_write_index := 0;
				else
					fifo_write_index := fifo_write_index + 1;
				end if;
			else
				instruction <= fifo_data(fifo_read_index);
			end if;			
			
			usedw <= std_logic_vector(to_unsigned(fifo_current_used,8));

			if fifo_current_used /= 0 then
				oReady <= '1';
			end if;
			
			if fifo_current_used = 255 then
				fifo_full <= '1';
			else
				fifo_full <= '0';
			end if;
		end if;
	end process name;

	u0 : component Microcontroller
		port map(
			clk_clk                    => clk, --           clk.clk
			gpu_0_gpu_output_size      => capacity, -- gpu_0_gpu_out.size
			gpu_0_gpu_output_fifo_full => fifo_full, --              .fifo_full
			gpu_0_gpu_output_write     => write, --              .write
			gpu_0_gpu_output_data      => data, --              .data
			sdram_addr                 => sdram_bus_addr, -- addr
			sdram_ba                   => sdram_bus_ba, -- ba
			sdram_cas_n                => sdram_bus_cas_n, -- cas_n
			sdram_cke                  => sdram_bus_cke, -- cke
			sdram_cs_n                 => sdram_bus_cs_n, -- cs_n
			sdram_dq                   => sdram_bus_dq, -- dq
			sdram_dqm                  => sdram_bus_dqm, -- dqm
			sdram_ras_n                => sdram_bus_ras_n, -- ras_n
			sdram_we_n                 => sdram_bus_we_n
		);

end architecture RTL;
