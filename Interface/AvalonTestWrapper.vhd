library ieee;
use ieee.std_logic_1164.all;

entity AvalonTestWrapper is
	port(
		clk_clk                    : in    std_logic                     := 'X'; -- clk
		reset_reset_n              : in    std_logic                     := 'X'; -- reset_n
		gpu_0_gpu_output_data      : out   std_logic_vector(31 downto 0); -- data
		gpu_0_gpu_output_write     : out   std_logic; -- write
		gpu_0_gpu_output_fifo_full : in    std_logic                     := 'X'; -- fifo_full
		gpu_0_gpu_output_size      : in    std_logic_vector(31 downto 0) := (others => 'X') -- size
	);
end entity AvalonTestWrapper;

architecture RTL of AvalonTestWrapper is
	component AvalonMMInterface
		port(clk, reset                 : in  std_logic;
			 address                    : in  std_logic_vector(1 downto 0);
			 read, write                : in  std_logic;
			 waitrequest, readdatavalid : out std_logic;
			 readdata                   : out std_logic_vector(31 downto 0);
			 writedata                  : in  std_logic_vector(31 downto 0);
			 gpu_data                   : out std_logic_vector(31 downto 0);
			 gpu_write                  : out std_logic;
			 gpu_fifo_full              : in  std_logic;
			 gpu_size                   : std_logic_vector(31 downto 0));
	end component AvalonMMInterface;
	signal address : std_logic_vector(1 downto 0);
	signal read : std_logic;
	signal write : std_logic;
	signal waitrequest : std_logic;
	signal readdatavalid : std_logic;
	signal readdata : std_logic_vector(31 downto 0);
	signal writedata : std_logic_vector(31 downto 0);
begin
	u0 : component AvalonMMInterface
		port map(clk           => clk_clk,
			     reset         => reset_reset_n,
			     address       => address,
			     read          => read,
			     write         => write,
			     waitrequest   => waitrequest,
			     readdatavalid => readdatavalid,
			     readdata      => readdata,
			     writedata     => writedata,
			     gpu_data      => gpu_0_gpu_output_data,
			     gpu_write     => gpu_0_gpu_output_write,
			     gpu_fifo_full => gpu_0_gpu_output_fifo_full,
			     gpu_size      => gpu_0_gpu_output_size);
end architecture RTL;
