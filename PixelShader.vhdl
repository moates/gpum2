library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity PixelShader is
  port (
    clk, res : in std_logic;
    ba0, ba1 : in VectorElement;
    triangle : in TriangleAttributes;
    pixel : out std_logic_vector(BPP-1 downto 0)
    );
end PixelShader;

architecture PixelShader_RTL of PixelShader is

begin  -- PixelShader_RTL

  shadeStep: process (clk, res)
  begin  -- process shadeStep
    if res = '0' then                   -- asynchronous reset (active low)
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      
      
      pixel(15 downto 10) <= triangle.r0 * ba0 + triangle.r1 * ba1 + triangle.r2;
      pixel(9 downto 5) <= triangle.g0 * ba0 + triangle.g1 * ba1 + triangle.g2;
      pixel(4 downto 0) <= triangle.b0 * ba0 + triangle.b1 * ba1 + triangle.b2;
    end if;
  end process shadeStep;

end PixelShader_RTL;
