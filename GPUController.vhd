library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;

use work.GpuTypes.all;

entity GPUController is
	port(
		clk                                                                           : in  std_logic;
		indata                                                                        : in  std_logic_vector(31 downto 0);
		iReady                                                                        : in  std_logic;
		iWaiting                                                                      : buffer std_logic := '1';
		Rasterizer_Empty, Triangle_Unit_Empty, Vertex_Unit_Empty                      : in  std_logic;
		Output_Cache_Empty, Pipeline_Waiting, ZBuffer_Buffer_Empty                    : in  std_logic;
		Pipeline_Ready, Output_Cache_Flush_Buffers, loadMatrix, ZBuffer_Flush_Buffers : buffer std_logic := '0';
		Triangle_Unit_Force_Empty                                                     : out std_logic    := '0';
		Active_Buffer                                                                 : buffer std_logic := '0';
		outVertexAttribute                                                            : out VertexPreTransformAttributes;
		outV                                                                          : out std_logic_vector(31 downto 0);
		outColour                                                                     : out std_logic_vector(15 downto 0);
		outDepth                                                                      : out std_logic_vector(15 downto 0);
		outDepthCheck                                                                 : out std_logic_vector(1 downto 0) := (others => '0')
	);
end entity GPUController;

architecture GPUController_RTL of GPUController is
	type state is (Idle, Clearing_Waiting, Clearing, Vertex, LoadingMatrix);

begin
	controllerProcess : process(clk) is
		variable currentState                   : state                     := Idle;
		variable currentData                    : std_logic_vector(31 downto 0);
		variable loadMatrixCounter              : integer range 15 downto 0 := 0;
		variable loadVertexCounter              : integer range 3 downto 0  := 0;
		variable instruction                    : std_logic_vector(2 downto 0);
		variable outputLine                     : line;
		variable zBufferClear, outputCacheClear : boolean;
		variable clearFlags : std_logic_vector(1 downto 0) := (others => '0');
	begin
		if rising_edge(clk) then
			if iWaiting = '1' and iReady = '1' then
				iWaiting <= '0';
			end if;

			if Pipeline_Ready = '1' and Pipeline_Waiting = '1' then
				Pipeline_Ready <= '0';
			end if;

			case currentState is
				when Idle =>
					loadMatrix  <= '0';
					currentData := indata;
					instruction := indata(31 downto 29);
					iWaiting    <= '1';

					if iReady = '1' then
						case instruction is
							when "001" =>
								write(outputLine, string'("Begin clear operation"));
								writeline(output, outputLine);
								currentState := Clearing_Waiting;
								clearFlags := indata(1 downto 0);
								iWaiting     <= '0';
							when "010" => -- Start Matrix
								write(outputLine, string'("Started loading matrix"));
								writeline(output, outputLine);
								currentState      := LoadingMatrix;
								loadMatrixCounter := 0;
								loadMatrix        <= '1';
							when "011" => -- Load Attribute
								write(outputLine, string'("Started Loading Attributes"));
								writeline(output, outputLine);
								outVertexAttribute.r <= unsigned(currentData(14 downto 10));
								outVertexAttribute.g <= unsigned(currentData(9 downto 5));
								outVertexAttribute.b <= unsigned(currentData(4 downto 0));
								iWaiting             <= '1';
							when "100" =>
								write(outputLine, string'("Started Loading Attributes"));
								writeline(output, outputLine);
								outVertexAttribute.u <= unsigned(currentData(9 downto 5));
								outVertexAttribute.v <= unsigned(currentData(4 downto 0));
								iWaiting             <= '1';
							when "101" => -- Start Vertex
								write(outputLine, string'("Load vertex"));
								writeline(output, outputLine);
								loadVertexCounter := 0;
								currentState      := Vertex;
							when "110" =>
								write(outputLine, string'("Loading new clear colour"));
								writeline(output, outputLine);
								outColour <= currentData(15 downto 0);
								iWaiting  <= '1';
							when "111" =>
								write(outputLine, string'("Setting depth buffer clear depth and depth test"));
								writeline(output, outputLine);
								outDepth      <= currentData(15 downto 0);
								outDepthCheck <= currentData(17 downto 16);
							when others => null;
						end case;
					end if;

				when Clearing_Waiting =>
					if Vertex_Unit_Empty = '1' and Triangle_Unit_Empty = '0' then
						Triangle_Unit_Force_Empty <= '1';
					else
						Triangle_Unit_Force_Empty <= '0';
					end if;

					if Rasterizer_Empty = '1' and Triangle_Unit_Empty = '1' and Vertex_Unit_Empty = '1' then
						if clearFlags(0) = '1' then 
							Output_Cache_Flush_Buffers <= '1';
						end if;
						
						if clearFlags(1) = '1' then
							ZBuffer_Flush_Buffers      <= '1';
						end if;
						currentState               := Clearing;
					end if;
				when Clearing =>
					if ZBuffer_Buffer_Empty = '1' then
						ZBuffer_Flush_Buffers <= '0';
						zBufferClear          := true;
					end if;

					if Output_Cache_Empty = '1' then
						Output_Cache_Flush_Buffers <= '0';
						Active_Buffer              <= not Active_Buffer;
						outputCacheClear           := true;
					end if;

					if (zBufferClear or clearFlags(1) = '0') and (outputCacheClear or clearFlags(0) = '0') then
						currentState     := Idle;
						iWaiting         <= '1';
						outputCacheClear := false;
						zBufferClear     := false;
						clearFlags := (others => '0');
					end if;
				when Vertex =>
					if Pipeline_Waiting = '1' then
						iWaiting <= '1';

						if iReady = '1' then
							Pipeline_Ready <= '1';
							outV           <= indata;

							if loadVertexCounter = 3 then
								currentState      := Idle;
								loadVertexCounter := 0;
							else
								loadVertexCounter := loadVertexCounter + 1;
							end if;
						else
							Pipeline_Ready <= '0';
						end if;
					else
						iWaiting <= '0';
					end if;
				when LoadingMatrix =>
					if Pipeline_Waiting = '1' then
						iWaiting <= '1';

						if iReady = '1' then
							Pipeline_Ready <= '1';
							outV           <= indata;

							if loadMatrixCounter = 15 then
								currentState      := Idle;
								loadMatrixCounter := 0;
							else
								loadMatrixCounter := loadMatrixCounter + 1;
							end if;
						else
							Pipeline_Ready <= '0';
						end if;
					else
						iWaiting <= '0';
					end if;
			end case;
		end if;
	end process controllerProcess;

end architecture GPUController_RTL;
