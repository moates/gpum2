-------------------------------------------------------------------------------
-- Title      : Barycentric scan unit
-- Project    : 
-------------------------------------------------------------------------------
-- File       : BScanUnit.vhdl
-- Author     : Michael Oates  <michael@michael-desktop>
-- Company    : 
-- Created    : 2013-08-14
-- Last update: 2013-08-15
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: Scans the screen using barycentric coordinates
-------------------------------------------------------------------------------
-- Copyright (c) 2013 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2013-08-14  1.0      michael Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity BScanUnitTop is
	generic(
		TILE_WIDTH : natural := 3
	);

	port(
		clk, res                : in  std_logic;
		iReady, oWaiting        : in  std_logic;
		iWaiting, oReady        : buffer std_logic;
		triangle                : in  TriangleType;
		triangleOut             : out TriangleType;
		barycentricAreas        : out BarycentricCoords;
		boundingBox             : in  BoxType;
		areaAddress             : out Address;
		empty : out std_logic
	);

end BScanUnitTop;

architecture BScanUnit_Top_RTL of BScanUnitTop is
	constant TILE_SIZE : natural := 2 ** TILE_WIDTH;
	type state is (idle, scan);
begin                                   -- BScanUnitRTL

	scanProcess : process(clk, res)
		variable ScanX                     : integer range 0 to SWidth - 1;
		variable ScanY                     : integer range 0 to SHeight - 1;
		variable currentTriangle           : TriangleType;
		variable currentState              : state             := idle;
		variable currentCoords             : BarycentricCoords := (others => (others => '0'));
		variable nextValues, nextRowValues : BarycentricCoords := (others => (others => '0'));
		variable currentBoundingBox        : BoxType;
	begin                               -- process scanProcess
		if clk'event and clk = '1' then -- rising clock edge
			if iReady = '1' and iWaiting = '1' then
				iWaiting <= '0';
			end if;

			if oReady = '1' and oWaiting = '1' then
				oReady <= '0';
			end if;

			case currentState is
				when idle =>
					if iReady = '1' then
						empty <= '0';
						currentTriangle := triangle;
						currentState    := scan;
						currentBoundingBox := boundingBox;
						
						
						ScanX           := currentBoundingBox.xMin;
						ScanY           := currentBoundingBox.yMin;

						currentCoords.A0 := currentTriangle.g0 + Mul42(currentTriangle.a0, currentBoundingBox.xMin) + Mul42(currentTriangle.b0, currentBoundingBox.yMin);
						currentCoords.A1 := currentTriangle.g1 + Mul42(currentTriangle.a1, currentBoundingBox.xMin) + Mul42(currentTriangle.b1, currentBoundingBox.yMin);
						currentCoords.A2 := currentTriangle.g2 + Mul42(currentTriangle.a2, currentBoundingBox.xMin) + Mul42(currentTriangle.b2, currentBoundingBox.yMin);

						nextValues.A0(31 downto 0) := Mul32(currentTriangle.a0,TILE_SIZE);
						--nextValues.A0(VS+9 downto 32) := (others => nextValues.A0(31));
						nextValues.A1(31 downto 0) := Mul32(currentTriangle.a1,TILE_SIZE);
						--nextValues.A1(VS+9 downto 32) := (others => nextValues.A1(31));
						nextValues.A2(31 downto 0) := Mul32(currentTriangle.a2,TILE_SIZE);
						--nextValues.A2(VS+9 downto 32) := (others => nextValues.A2(31));
						
						nextRowValues.A0(31 downto 0) := Mul32(currentTriangle.b0,TILE_SIZE) - Mul32(currentTriangle.a0,currentBoundingBox.xMax-currentBoundingBox.xMin - TILE_SIZE);
						--nextRowValues.A0(VS+9 downto 32) := (others => nextRowValues.A0(31));
						nextRowValues.A1(31 downto 0) := Mul32(currentTriangle.b1,TILE_SIZE) - Mul32(currentTriangle.a1,currentBoundingBox.xMax-currentBoundingBox.xMin - TILE_SIZE);
						--nextRowValues.A1(VS+9 downto 32) := (others => nextRowValues.A1(31));
						nextRowValues.A2(31 downto 0) := Mul32(currentTriangle.b2,TILE_SIZE) - Mul32(currentTriangle.a2,currentBoundingBox.xMax-currentBoundingBox.xMin - TILE_SIZE);
						--nextRowValues.A2(VS+9 downto 32) := (others => nextRowValues.A2(31));
					else
						empty <= '1';
						iWaiting <= '1';
					end if;
				when scan =>					
					barycentricAreas        <= currentCoords;
					triangleOut 			<= currentTriangle;
					areaAddress             <= AddrResolve(scanX, scanY);
					oReady                  <= '1';
					
					if oWaiting = '1' then
						if ScanX = currentBoundingBox.xMax - TILE_SIZE then
							if ScanY = currentBoundingBox.yMax - TILE_SIZE then
								currentState := idle;
								iWaiting     <= '1';
								oReady 		 <= '0';
							else
								ScanY := ScanY + TILE_SIZE;
								ScanX := currentBoundingBox.xMin;

								currentCoords.A0 := currentCoords.A0 + nextRowValues.A0;
								currentCoords.A1 := currentCoords.A1 + nextRowValues.A1;
								currentCoords.A2 := currentCoords.A2 + nextRowValues.A2;
							end if;
						else
							currentCoords.A0 := currentCoords.A0 + nextValues.A0;
							currentCoords.A1 := currentCoords.A1 + nextValues.A1;
							currentCoords.A2 := currentCoords.A2 + nextValues.A2;
							
							ScanX := ScanX + TILE_SIZE;
						end if;
					end if;
			end case;
		end if;
	end process scanProcess;

end BScanUnit_Top_RTL;
