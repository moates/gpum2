library ieee;
use ieee.std_logic_1164.all;

use work.GpuTypes.all;

package TransformUnitTypes is

	constant L_FPADD : natural := 7;
	constant L_FPMUL : natural := 6;
	constant L_FPDIV : natural := 6;
	constant L_FPCON : natural := 6;
	type element is (x, y, z, w);

	type PipelineStage is record
		enabled        : std_logic;
		vector_element : element;
	end record;

	type dataBuffer is record
		color : Colour3Type;
	end record;

	component fpadd
		port(clk_en : IN  STD_LOGIC;
			 clock  : IN  STD_LOGIC;
			 dataa  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			 datab  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			 result : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
	end component fpadd;

	component fpmult
		port(clk_en : IN  STD_LOGIC;
			 clock  : IN  STD_LOGIC;
			 dataa  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			 datab  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			 result : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
	end component fpmult;

	component fpdiv
		port(clk_en : IN  STD_LOGIC;
			 clock  : IN  STD_LOGIC;
			 dataa  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			 datab  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			 result : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
	end component fpdiv;

	component fpconv
		port(clk_en : IN  STD_LOGIC;
			 clock  : IN  STD_LOGIC;
			 dataa  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			 result : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
	end component fpconv;

	component SingleChannelTransformUnit
		port(clk               : in  std_logic;
			 inv               : in  std_logic_vector(31 downto 0);
			 outv              : out std_logic_vector(31 downto 0);
			 outPipeline       : out PipelineStage;
			 clken             : in  std_logic;
			 iReady            : in  std_logic;
			 iWaiting, iAccept : out std_logic;
			 loadMatrix		   : in std_logic);
	end component SingleChannelTransformUnit;

	component SingleChannelScaleUnit
		port(clk         : in  std_logic;
			 inv         : in  std_logic_vector(31 downto 0);
			 outv        : out std_logic_vector(31 downto 0);
			 inPipeline  : in  PipelineStage;
			 outPipeline : out PipelineStage;
			 clken       : in  std_logic);
	end component SingleChannelScaleUnit;

	component SingleChannelConvertUnit
		port(clk         : in  std_logic;
			 inv         : in  std_logic_vector(31 downto 0);
			 outv        : out std_logic_vector(31 downto 0);
			 inPipeline  : in  PipelineStage;
			 outPipeline : out PipelineStage;
			 clken       : in  std_logic);
	end component SingleChannelConvertUnit;

	component SingleChannelCollectUnit
		port(clk        : in  std_logic;
			 inv        : in  std_logic_vector(31 downto 0);
			 outv       : out Vector4Type;
			 inPipeline : in  PipelineStage;
			 ready      : out std_logic;
			 clken      : in  std_logic);
	end component SingleChannelCollectUnit;

	component VectorAttributeFIFO
		generic(capacity : natural := 2);
		port(clk           : in  std_logic;
			 rst           : in  std_logic;
			 write         : in  std_logic;
			 read          : in  std_logic;
			 inAttributes  : in  VertexPreTransformAttributes;
			 outAttributes : out VertexPreTransformAttributes;
			 empty         : out std_logic);
	end component VectorAttributeFIFO;	
end package TransformUnitTypes;
