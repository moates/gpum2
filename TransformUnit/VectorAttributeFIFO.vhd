library ieee;
use ieee.std_logic_1164.all;

use work.GpuTypes.all;

entity VectorAttributeFIFO is
	generic (
		capacity : natural := 2
	);
	port (
		clk : in std_logic;
		rst : in std_logic;
		write : in std_logic;
		read : in std_logic;
		inAttributes : in VertexPreTransformAttributes;
		outAttributes : out VertexPreTransformAttributes;
		empty : out std_logic
	);
end entity VectorAttributeFIFO;

architecture RTL of VectorAttributeFIFO is
	type FIFO is array (capacity-1 downto 0) of VertexPreTransformAttributes;
	
	signal internalMemory : FIFO;
begin
	name : process (clk) is
		variable readPtr, writePtr : integer range capacity-1 downto 0 := 0;
		variable size : integer range capacity-1 downto 0 := capacity-1; 
	begin
		if rising_edge(clk) then					
			if read = '1' then
				size := size + 1;				
			
				if readPtr = capacity - 1 then
					readPtr  :=  0;
				else
					readPtr := readPtr + 1;
				end if;								
			end if;
								
			if write = '1' then
				size := size - 1;
				internalMemory(writePtr)  <= inAttributes;
			
				if writePtr = capacity - 1 then
					writePtr  :=  0;
				else
					writePtr := writePtr + 1;
				end if;							
			end if;							
			
			outAttributes <= internalMemory(readPtr);	
			
			if size = capacity-1 then
				empty <= '1';
			else
				empty <= '0';
			end if;						
		end if;
	end process name;
	
end architecture RTL;
