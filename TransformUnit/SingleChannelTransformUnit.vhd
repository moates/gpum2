library ieee;
use ieee.std_logic_1164.all;

use work.GpuTypes.all;
use work.TransformUnitTypes.all;

entity SingleChannelTransformUnit is
	port (
		clk : in std_logic;
		inv : in std_logic_vector(31 downto 0);
		outv: out std_logic_vector(31 downto 0);
		outPipeline : out PipelineStage;
		clken : in std_logic;
		iReady, loadMatrix : in std_logic;
		iWaiting, iAccept : buffer std_logic := '0'
	);
end entity SingleChannelTransformUnit;

architecture RTL of SingleChannelTransformUnit is
	type resultArray is array (integer range <>) of std_logic_vector(31 downto 0);
	type matrixColumn is array (3 downto 0) of std_logic_vector(31 downto 0);
	type matrix is array (3 downto 0) of matrixColumn;	
	type vectorInputBuffer is array(1 downto 0) of matrixColumn;
	
	signal stage_0 : resultArray(3 downto 0);
	signal stage_1 : resultArray(1 downto 0);
	signal vectorInputIndex : integer range 0 to 1;
	
	signal internalMatrix : matrix;
	signal matrixIndex : integer range 3 downto 0;
	signal stage_3_input_a, stage_3_input_b, stage_3, stage_2  : STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal vectorInput : vectorInputBuffer;
	
begin	

	pipelineImpl : process (clk) is
		type pipelineStages is array(L_FPMUL+L_FPADD*2+L_FPDIV+2 downto 0) of PipelineStage;
		constant L_MATRIX : natural := L_FPADD*2+L_FPMUL+1;
		constant FP_1 : std_logic_vector(31 downto 0) := "00111111100000000000000000000000";
		
		variable pipeline : pipelineStages := (others => (vector_element => x, enabled => '0'));
		variable inputProcess, pipelineProcess : integer range 15 downto 0 := 0;
		variable elementW	   : std_logic_vector(31 downto 0);	
		variable ready : boolean;
		
	begin
		if rising_edge(clk) then
			if iAccept = '1' then
				iAccept <= '0';
			end if;
		
			if clken = '1' then							
				for i in pipeline'high downto 1 loop
					pipeline(i) := pipeline(i-1);
				end loop;				
				
				if iReady = '1' then		
					if loadMatrix = '1' then
						internalMatrix(inputProcess/4)(inputProcess mod 4) <= inv;
					
						if inputProcess = 15 then
							inputProcess := 0;
						else
							inputProcess := inputProcess + 1;
						end if;
					else	
						if inputProcess = 0 then
							vectorInput(vectorInputIndex)(3) <= inv;													
							inputProcess := 1;
							iAccept <= '1';
						elsif inputProcess = 1 then
							vectorInput(vectorInputIndex)(2) <= inv;
							inputProcess := 2;
						elsif inputProcess = 2 then
							vectorInput(vectorInputIndex)(1) <= inv;
							inputProcess := 3;
						elsif inputProcess = 3 then
							vectorInput(vectorInputIndex)(0) <= inv;
							vectorInputIndex <= 1 - vectorInputIndex;
							ready := true;
							inputProcess := 0;
						end if;		
					end if;
				end if;
				
				if ready then
					if pipelineProcess = 0 then												
							pipeline(0) := (vector_element => w, enabled => '1');
							matrixIndex <= 3;
							pipelineProcess := 1;
						elsif pipelineProcess = 1 then
							pipeline(0) := (vector_element => z, enabled => '1');
							matrixIndex <= 2;
							pipelineProcess := 2;
						elsif pipelineProcess = 2 then
							pipeline(0) := (vector_element => y, enabled => '1');
							matrixIndex <= 1;
							pipelineProcess := 3;
						elsif pipelineProcess = 3 then
							ready := false;
							pipeline(0) := (vector_element => x, enabled => '1');
							matrixIndex <= 0;
							pipelineProcess := 0;
						end if;	
						
				else
					pipeline(0) := (vector_element => x, enabled => '0');	
				end if;		
				
				if pipeline(L_MATRIX).enabled = '1' then
					if pipeline(L_MATRIX).vector_element = w then
						elementW := stage_2;
						stage_3_input_b <= FP_1;
						stage_3_input_a <= elementW;
					else
						stage_3_input_b <= elementW;
						stage_3_input_a <= stage_2;						
					end if;
				end if;				
				
				outv <= stage_3;
				outPipeline <= pipeline(pipeline'high);
				
				
				iWaiting <= '1';																
			else
				iWaiting <= '0';
			end if;
		end if;
	end process pipelineImpl;
	
	
	
	matrixMult_Column : for i in 0 to 3 generate
		signal matrixElement : std_logic_vector(31 downto 0);
		signal vectorElement : std_logic_vector(31 downto 0);
	begin
		matrixElement <= internalMatrix(matrixIndex)(i);
		vectorElement <= vectorInput(1-vectorInputIndex)(i);
	
			mult_stage_0 : fpmult       -- Latency of 6
				port map(clk_en => clken,
					     clock  => clk,
					     dataa  => matrixElement,
					     datab  => vectorElement,
					     result => stage_0(i));
	end generate matrixMult_Column;
		
	-- Reduce
	
	add_stage_1_0 : fpadd
		port map(clk_en => clken,
			     clock  => clk,
			     dataa  => stage_0(0),
			     datab  => stage_0(1),
			     result => stage_1(0));
			     
	add_stage_1_1 : fpadd
		port map(clk_en => clken,
			     clock  => clk,
			     dataa  => stage_0(2),
			     datab  => stage_0(3),
			     result => stage_1(1));
			     
	add_stage_2 : fpadd
		port map(clk_en => clken,
			     clock  => clk,
			     dataa  => stage_1(0),
			     datab  => stage_1(1),
			     result => stage_2);
			     
	-- Scale by W
	div_stage_3 : fpdiv
		port map(clk_en => clken,
			     clock  => clk,
			     dataa  => stage_3_input_a,
			     datab  => stage_3_input_b,
			     result => stage_3);
	
end architecture RTL;
