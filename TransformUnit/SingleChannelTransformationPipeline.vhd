library ieee;
use ieee.std_logic_1164.all;

use work.TransformUnitTypes.all;
use work.GpuTypes.all;

entity SingleChannelTransformationPipeline is
	port(
		clk              : in  std_logic;
		rst              : in  std_logic;
		iReady, oWaiting : in  std_logic;
		iWaiting, oReady : buffer std_logic := '0';
		inAttributes     : in VertexPreTransformAttributes;
		outAttributes    : out VertexPreTransformAttributes;
		inV              : in std_logic_vector(31 downto 0);
		outV             : out Vector4Type;
		empty            : out std_logic;
		loadMatrix		 : in std_logic
	);
end entity SingleChannelTransformationPipeline;

architecture RTL of SingleChannelTransformationPipeline is
	signal accept_vertex, give_vertex                           : std_logic;
	signal clken                                                : std_logic;
	signal transform_outv, scale_outv, convert_outv             : std_logic_vector(31 downto 0);
	signal transform_pipeline, scale_pipeline, convert_pipeline : PipelineStage;
begin
	clken       <= oWaiting or not oReady;
	give_vertex <= oReady and oWaiting and clken;
	

	attributes : VectorAttributeFIFO
		generic map(capacity => 20)
		port map(clk           => clk,
			     rst           => rst,
			     write         => accept_vertex,
			     read          => give_vertex,
			     inAttributes  => inAttributes,
			     outAttributes => outAttributes,
			     empty         => empty);

	transform : SingleChannelTransformUnit
		port map(clk         => clk,
			     inv         => inv,
			     outv        => transform_outv,
			     outPipeline => transform_pipeline,
			     clken       => clken,
			     iReady      => iReady,
			     iWaiting    => iWaiting,
			     iAccept     => accept_vertex,
			     loadMatrix  => loadMatrix);

	scale : SingleChannelScaleUnit
		port map(clk         => clk,
			     inv         => transform_outv,
			     outv        => scale_outv,
			     inPipeline  => transform_pipeline,
			     outPipeline => scale_pipeline,
			     clken       => clken);

	convert : SingleChannelConvertUnit
		port map(clk         => clk,
			     inv         => scale_outv,
			     outv        => convert_outv,
			     inPipeline  => scale_pipeline,
			     outPipeline => convert_pipeline,
			     clken       => clken);

	collect : SingleChannelCollectUnit
		port map(clk        => clk,
			     inv        => convert_outv,
			     outv       => outv,
			     inPipeline => convert_pipeline,
			     ready      => oReady,
			     clken      => clken);

end architecture RTL;
