library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.TransformUnitTypes.all;
use work.GpuTypes.all;

entity SingleChannelCollectUnit is
	port(
		clk        : in  std_logic;
		inv        : in  std_logic_vector(31 downto 0);
		outv       : out Vector4Type;
		inPipeline : in  PipelineStage;
		ready      : buffer std_logic := '0';
		clken      : in  std_logic
	);
end entity SingleChannelCollectUnit;

architecture RTL of SingleChannelCollectUnit is
begin
	collect : process(clk) is
	begin
		if rising_edge(clk) then				
			if clken = '1' then	
				if ready = '1' then
					ready <= '0';
				end if;			
			
				if inPipeline.enabled = '1' then
					case inPipeline.vector_element is 
						when x =>
							outv.x <= signed(inv);
							ready <= '1';
						when y =>
							outv.y <= signed(inv);
						when z =>
							outv.z <= signed(inv);
						when w =>
							outv.w <= signed(inv);							
					end case;
				end if;
			end if;
		end if;
	end process collect;

end architecture RTL;
