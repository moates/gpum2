library ieee;
use ieee.std_logic_1164.all;

use work.TransformUnitTypes.all;

entity SingleChannelScaleUnit is
	port(
		clk         : in  std_logic;
		inv         : in  std_logic_vector(31 downto 0);
		outv        : out std_logic_vector(31 downto 0);
		inPipeline  : in  PipelineStage;
		outPipeline : out PipelineStage;
		clken       : in  std_logic
	);
end entity SingleChannelScaleUnit;

architecture RTL of SingleChannelScaleUnit is
	signal stage_0_input_a, stage_0_input_b, stage_0 : STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal stage_1_input_a, stage_1                  : STD_LOGIC_VECTOR(31 DOWNTO 0);
begin
	pipelineImpl : process(clk) is
		type pipelineStages is array (L_FPMUL + L_FPADD +2 downto 0) of PipelineStage;
		variable pipeline : pipelineStages                := (others => (enabled => '0', vector_element => w));
		constant X_SCALE  : std_logic_vector(31 downto 0) := "01000011101000000000000000000000";
		constant Y_SCALE  : std_logic_vector(31 downto 0) := "01000011011100000000000000000000";
		constant Z_SCALE  : std_logic_vector(31 downto 0) := "01000100000000000000000000000000";
		constant W_SCALE  : std_logic_vector(31 downto 0) := "00111111100000000000000000000000";
	begin
		if rising_edge(clk) then
			if clken = '1' then
				for i in pipeline'high downto 1 loop
					pipeline(i) := pipeline(i - 1);
				end loop;

				pipeline(0) := inPipeline;

				case inPipeline.vector_element is
					when x =>
						stage_0_input_b <= X_SCALE;
						stage_0_input_a <= inv;
					when y =>
						stage_0_input_b <= Y_SCALE;
						stage_0_input_a <= not inv(31) & inv(30 downto 0);
					when z =>
						stage_0_input_b <= Z_SCALE;
						stage_0_input_a <= inv;
					when w =>
						stage_0_input_b <= W_SCALE;
						stage_0_input_a <= inv;
				end case;

				
				
				
				
				case pipeline(L_FPMUL).vector_element is
					when x =>
						stage_1_input_a <= X_SCALE;
					when y =>
						stage_1_input_a <= Y_SCALE;
					when z =>
						stage_1_input_a <= Z_SCALE;
					when w =>
						stage_1_input_a <= (others => '0');
				end case;
				
				outPipeline <= pipeline(pipeline'high);
				outV <= stage_1;				

			end if;
		end if;
	end process pipelineImpl;

	stage_0_fpmult : component fpmult
		port map(clk_en => clken,
			     clock  => clk,
			     dataa  => stage_0_input_a,
			     datab  => stage_0_input_b,
			     result => stage_0);
	stage_1_fp_add : component fpadd
		port map(clk_en => clken,
			     clock  => clk,
			     dataa  => stage_1_input_a,
			     datab  => stage_0,
			     result => stage_1);

end architecture RTL;
