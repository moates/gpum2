library ieee;
use ieee.std_logic_1164.all;

use work.TransformUnitTypes.all;

entity SingleChannelConvertUnit is
	port(
		clk         : in  std_logic;
		inv         : in  std_logic_vector(31 downto 0);
		outv        : out std_logic_vector(31 downto 0);
		inPipeline  : in  PipelineStage;
		outPipeline : out PipelineStage;
		clken       : in  std_logic
	);
end entity SingleChannelConvertUnit;

architecture RTL of SingleChannelConvertUnit is
	signal stage_0_input : STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal stage_0 : STD_LOGIC_VECTOR(31 DOWNTO 0);
	
begin
	pipelineImpl : process(clk) is
		type pipelineStages is array (L_FPCON downto 0) of PipelineStage;
		variable pipeline : pipelineStages                := (others => (enabled => '0', vector_element => w));
	begin
		if rising_edge(clk) then
			if clken = '1' then
				for i in pipeline'high downto 1 loop
					pipeline(i) := pipeline(i - 1);
				end loop;
				
				pipeline(0) := inPipeline;
				outPipeline <= pipeline(pipeline'high);

				stage_0_input <= inv;
				outV <= stage_0;				
			end if;
		end if;
	end process pipelineImpl;	
	
	stage_0_fpconv : fpconv
		port map(clk_en => clken,
			     clock  => clk,
			     dataa  => stage_0_input,
			     result => stage_0);
end architecture RTL;
