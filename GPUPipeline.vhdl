library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity GPUPipeline is
	port(
		clk, res                                                : in  std_logic;
		gpu_output, zBuffer_writeData                           : out std_logic_vector(31 downto 0);
		zBuffer_readData                                        : in  std_logic_vector(31 downto 0);
		gpu_address, zBuffer_writeAddress, zBuffer_readAddress  : out Address;
		gpu_enabled, zBuffer_enabled                            : out std_logic_vector(1 downto 0);
		gpu_write, zBuffer_write, zBuffer_read, zBuffer_request : out std_logic;
		gpu_writeFull, zBuffer_writeFull, zBuffer_readEmpty     : in  std_logic;
		gpu_iWaiting                                            : buffer std_logic;
		gpu_iReady                                              : in  std_logic;
		gpu_iData                                               : in  std_logic_vector(31 downto 0);
		active_buffer                                           : out std_logic);

end GPUPipeline;

architecture GPUPipeline_RTL of GPUPipeline is

	-------------------------------------------------------------------------------
	-- Component declarations
	-------------------------------------------------------------------------------

	component GPUController
		port(clk                                                                           : in  std_logic;
			 indata                                                                        : in  std_logic_vector(31 downto 0);
			 iReady                                                                        : in  std_logic;
			 iWaiting                                                                      : out std_logic    := '1';
			 Rasterizer_Empty, Triangle_Unit_Empty, Vertex_Unit_Empty                      : in  std_logic;
			 Output_Cache_Empty, Pipeline_Waiting, ZBuffer_Buffer_Empty                    : in  std_logic;
			 Pipeline_Ready, Output_Cache_Flush_Buffers, loadMatrix, ZBuffer_Flush_Buffers : out std_logic    := '0';
			 Active_Buffer                                                                 : buffer std_logic := '0';
			 Triangle_Unit_Force_Empty                                                     : out std_logic    := '0';
			 outVertexAttribute                                                            : out VertexPreTransformAttributes;
			 outV                                                                          : out std_logic_vector(31 downto 0);
			 outColour                                                                     : out std_logic_vector(15 downto 0);
			 outDepth                                                                      : out std_logic_vector(15 downto 0);
			 outDepthCheck                                                                 : out std_logic_vector(1 downto 0));
	end component GPUController;

	component TriangleUnit
		port(clk                       : in  std_logic;
			 rst                       : in  std_logic;
			 inV                       : in  Vector4Type;
			 inAttributes              : in  VertexPreTransformAttributes;
			 outTriangle               : out TriangleType;
			 outAttributes             : out TriangleAttributes;
			 outBoundingBox            : out BoxType;
			 iReady, oWaiting          : in  std_logic;
			 Triangle_Unit_Force_Empty : in  std_logic;
			 oReady, iWaiting, empty   : buffer std_logic := '1');
	end component TriangleUnit;

	component Rasterizer
		generic(scanWidth  : natural := 2;
			    scanHeight : natural := 1);
		port(clk, res                                                                   : in  std_logic;
			 triangle                                                                   : in  TriangleType;
			 triangleAttrs                                                              : in  TriangleAttributes;
			 pixelData                                                                  : out ColourVectorArray(scanWidth * scanHeight - 1 downto 0);
			 pixelAddress                                                               : out Address;
			 pixelsEnabled                                                              : out std_logic_vector(scanWidth * scanHeight - 1 downto 0);
			 iReady, oWaiting, clear, zBuffer_readEmpty, zBuffer_writeFull              : in  std_logic;
			 boundingBox                                                                : in  BoxType;
			 iWaiting, oReady                                                           : buffer std_logic;
			 empty, zBuffer_read, zBuffer_write, zBuffer_beginRead, zBuffer_bufferEmpty : out std_logic;
			 zBuffer_readAddress, zBuffer_writeAddress                                  : out Address;
			 zBuffer_readData                                                           : in  std_logic_vector(31 downto 0);
			 zBuffer_writeData                                                          : out std_logic_vector(31 downto 0);
			 clearDepth                                                                 : in  std_logic_vector(BPD - 1 downto 0);
			 comparisonType                                                             : in  std_logic_vector(1 downto 0));
	end component Rasterizer;

	component InputQueue
		port(address : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
			 clken   : IN  STD_LOGIC := '1';
			 clock   : IN  STD_LOGIC := '1';
			 q       : OUT STD_LOGIC_VECTOR(31 DOWNTO 0));
	end component InputQueue;

	component SingleChannelTransformationPipeline
		port(clk              : in  std_logic;
			 rst              : in  std_logic;
			 iReady, oWaiting : in  std_logic;
			 iWaiting, oReady : buffer std_logic := '0';
			 inAttributes     : in  VertexPreTransformAttributes;
			 outAttributes    : out VertexPreTransformAttributes;
			 inV              : in  std_logic_vector(31 downto 0);
			 outV             : out Vector4Type;
			 empty            : out std_logic;
			 loadMatrix       : in  std_logic);
	end component SingleChannelTransformationPipeline;

	component OutputCache
		generic(ROWS_WIDTH           : natural := 4;
			    ROW_SIZE             : natural := 1;
			    IDENTIFIER_WIDTH     : natural := 9;
			    SET_WIDTH            : natural := 3;
			    SET_COUNT_WIDTH      : natural := 4;
			    BURST_WIDTH          : natural := 128;
			    BURST_COUNT_WIDTH    : natural := 3;
			    BPP                  : natural := 16;
			    TILE_WIDTH           : natural := 6;
			    CLOCK_WIDTH          : natural := 2;
			    INPUT_WIDTH          : natural := 1;
			    TILES_PER_WORD_WIDTH : natural := 4);
		port(clk                      : in  std_logic;
			 iReady, oWaiting, iClear : in  std_logic;
			 iWaiting, oReady, empty  : buffer std_logic := '0';
			 inData                   : in  std_logic_vector(BPP * 2 ** INPUT_WIDTH - 1 downto 0);
			 inAddress                : in  std_logic_vector(IDENTIFIER_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - INPUT_WIDTH - 1 downto 0);
			 inEnabled                : in  std_logic_vector(2 ** INPUT_WIDTH - 1 downto 0);
			 inClearColour            : in  std_logic_vector(15 downto 0);
			 oAddress                 : buffer std_logic_vector(IDENTIFIER_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH - 1 downto 0);
			 oData                    : out std_logic_vector(BURST_WIDTH - 1 downto 0);
			 oEnabled                 : out std_logic_vector(2 ** BURST_COUNT_WIDTH - 1 downto 0));
	end component OutputCache;

	component OutputCacheMemoryAdapter
		port(clk                     : in  std_logic;
			 inData                  : in  std_logic_vector(127 downto 0);
			 outData                 : out std_logic_vector(31 downto 0);
			 inAddress               : in  Address;
			 outAddress              : out Address;
			 inEnabled               : in  std_logic_vector(7 downto 0);
			 outEnabled              : out std_logic_vector(1 downto 0);
			 memoryWriteFull, iReady : in  std_logic;
			 memoryWrite, iWaiting   : buffer std_logic := '0');
	end component OutputCacheMemoryAdapter;

	component Triangles
		port(address : IN  STD_LOGIC_VECTOR(4 DOWNTO 0);
			 clock   : IN  STD_LOGIC := '1';
			 q       : OUT STD_LOGIC_VECTOR(431 DOWNTO 0));
	end component Triangles;

	-------------------------------------------------------------------------------
	-- Signals
	-------------------------------------------------------------------------------

	-- Handshake signals
	signal Rasterizer_Inst_oReady     : std_logic;
	signal OutputBuffer_Inst_iWaiting : std_logic;

	signal Rasterizer_Inst_Output_PixelAddress                                         : Address;
	signal Rasterizer_Inst_Output_PixelMask                                            : std_logic_vector(1 downto 0);
	signal Rasterizer_Inst_Output_PixelData                                            : ColourVectorArray(1 downto 0);
	signal Output_Cache_Address                                                        : Address;
	signal Output_Cache_OutputData                                                     : std_logic_vector(BurstWidth - 1 downto 0);
	signal Output_Cache_Enabled                                                        : std_logic_vector(ColourBurstWidth - 1 downto 0);
	signal Output_Cache_Input_Data                                                     : std_logic_vector(BPP * 2 - 1 downto 0);
	signal Output_Address                                                              : std_logic_vector(BusWidth - 3 downto 0);
	signal Rasterizer_Inst_Empty                                                       : std_logic;
	signal Output_Cache_Empty                                                          : std_logic;
	signal Pipeline_Waiting                                                            : std_logic;
	signal Output_Cache_Flush_Buffers                                                  : std_logic;
	signal Pipeline_Ready                                                              : std_logic;
	signal GPU_Controller_Active_Buffer                                                : std_logic;
	signal Transformation_Unit_Empty, Triangle_Unit_Empty                              : std_logic;
	signal Transformation_Unit_Output_Waiting, Transformation_Unit_Output_Ready        : std_logic;
	signal Triangle_Unit_Output_Waiting, Triangle_Unit_Output_Ready                    : std_logic;
	signal Transformation_Unit_Input_Attributes, Transformation_Unit_Output_Attributes : VertexPreTransformAttributes;
	signal Transformation_Unit_Input_Data                                              : std_logic_vector(31 downto 0);
	signal Transformation_Unit_Output_Vertex                                           : Vector4Type;
	signal Triangle_Unit_Output_Triangle                                               : TriangleType;
	signal Triangle_Unit_Output_Bounding_Box                                           : BoxType;
	signal Triangle_Unit_Output_Attributes                                             : TriangleAttributes;
	signal Transformation_Unit_Load_Matrix                                             : std_logic;
	signal Output_Cache_Input_Clear_Colour                                             : std_logic_vector(15 downto 0);
	signal Triangle_Unit_Force_Empty                                                   : std_logic;
	signal OutputCache_Inst_oWaiting                                                   : std_logic;
	signal OutputCache_Inst_oReady                                                     : std_logic;
	signal zBuffer_clearDepth                                                          : std_logic_vector(BPD - 1 downto 0);
	signal zBuffer_comparisonType                                                      : std_logic_vector(1 downto 0);
	signal ZBuffer_Flush_Buffers                                                       : std_logic;
	signal ZBuffer_Buffer_Empty                                                        : std_logic;
begin                                   -- GPUPipeline_RTL

	GPU_Controller_Inst : component GPUController
		port map(clk                        => clk,
			     indata                     => gpu_iData,
			     iReady                     => gpu_iReady,
			     iWaiting                   => gpu_iWaiting,
			     Rasterizer_Empty           => Rasterizer_Inst_Empty,
			     Triangle_Unit_Empty        => Triangle_Unit_Empty,
			     Vertex_Unit_Empty          => Transformation_Unit_Empty,
			     Output_Cache_Empty         => Output_Cache_Empty,
			     Pipeline_Waiting           => Pipeline_Waiting,
			     Output_Cache_Flush_Buffers => Output_Cache_Flush_Buffers,
			     Triangle_Unit_Force_Empty  => Triangle_Unit_Force_Empty,
			     Pipeline_Ready             => Pipeline_Ready,
			     Active_Buffer              => active_buffer,
			     outVertexAttribute         => Transformation_Unit_Input_Attributes,
			     outV                       => Transformation_Unit_Input_Data,
			     outColour                  => Output_Cache_Input_Clear_Colour,
			     outDepth                   => zBuffer_clearDepth,
			     outDepthCheck              => zBuffer_comparisonType,
			     loadMatrix                 => Transformation_Unit_Load_Matrix,
			     ZBuffer_Flush_Buffers      => ZBuffer_Flush_Buffers,
			     ZBuffer_Buffer_Empty       => ZBuffer_Buffer_Empty);

	Transformation_Inst : component SingleChannelTransformationPipeline
		port map(clk           => clk,
			     rst           => res,
			     iReady        => Pipeline_Ready,
			     oWaiting      => Transformation_Unit_Output_Waiting,
			     iWaiting      => Pipeline_Waiting,
			     oReady        => Transformation_Unit_Output_Ready,
			     inAttributes  => Transformation_Unit_Input_Attributes,
			     outAttributes => Transformation_Unit_Output_Attributes,
			     inV           => Transformation_Unit_Input_Data,
			     outV          => Transformation_Unit_Output_Vertex,
			     loadMatrix    => Transformation_Unit_Load_Matrix,
			     empty         => Transformation_Unit_Empty);

	TriangleUnit_Inst : component TriangleUnit
		port map(clk                       => clk,
			     rst                       => res,
			     inV                       => Transformation_Unit_Output_Vertex,
			     inAttributes              => Transformation_Unit_Output_Attributes,
			     outTriangle               => Triangle_Unit_Output_Triangle,
			     outAttributes             => Triangle_Unit_Output_Attributes,
			     outBoundingBox            => Triangle_Unit_Output_Bounding_Box,
			     iReady                    => Transformation_Unit_Output_Ready,
			     oWaiting                  => Triangle_Unit_Output_Waiting,
			     oReady                    => Triangle_Unit_Output_Ready,
			     iWaiting                  => Transformation_Unit_Output_Waiting,
			     empty                     => Triangle_Unit_Empty,
			     Triangle_Unit_Force_Empty => Triangle_Unit_Force_Empty);

	Rasterizer_Inst : component Rasterizer
		generic map(scanWidth  => 2,
			        scanHeight => 1)
		port map(clk                  => clk,
			     res                  => res,
			     triangle             => Triangle_Unit_Output_Triangle,
			     triangleAttrs        => Triangle_Unit_Output_Attributes,
			     pixelData            => Rasterizer_Inst_Output_PixelData,
			     pixelAddress         => Rasterizer_Inst_Output_PixelAddress,
			     pixelsEnabled        => Rasterizer_Inst_Output_PixelMask,
			     iReady               => Triangle_Unit_Output_Ready,
			     oWaiting             => OutputBuffer_Inst_iWaiting,
			     clear                => ZBuffer_Flush_Buffers,
			     zBuffer_bufferEmpty  => ZBuffer_Buffer_Empty,
			     zBuffer_readEmpty    => zBuffer_readEmpty,
			     zBuffer_writeFull    => zBuffer_writeFull,
			     boundingBox          => Triangle_Unit_Output_Bounding_Box,
			     iWaiting             => Triangle_Unit_Output_Waiting,
			     oReady               => Rasterizer_Inst_oReady,
			     empty                => Rasterizer_Inst_Empty,
			     zBuffer_read         => zBuffer_read,
			     zBuffer_write        => zBuffer_write,
			     zBuffer_beginRead    => zBuffer_request,
			     zBuffer_readAddress  => zBuffer_readAddress,
			     zBuffer_writeAddress => zBuffer_writeAddress,
			     zBuffer_readData     => zBuffer_readData,
			     zBuffer_writeData    => zBuffer_writeData,
			     clearDepth           => zBuffer_clearDepth,
			     comparisonType       => zBuffer_comparisonType);

	zBuffer_enabled <= (others => '1');

	OutputCache_Inst : OutputCache
		generic map(ROWS_WIDTH        => 4,
			        ROW_SIZE          => 1,
			        IDENTIFIER_WIDTH  => 10,
			        SET_WIDTH         => 3,
			        SET_COUNT_WIDTH   => 3,
			        BURST_WIDTH       => BurstWidth,
			        BURST_COUNT_WIDTH => log2(ColourBurstWidth),
			        BPP               => BPP,
			        TILE_WIDTH        => TileWidth * 2,
			        CLOCK_WIDTH       => 2,
			        INPUT_WIDTH       => 1)
		port map(clk           => clk,
			     iReady        => Rasterizer_Inst_oReady,
			     iClear        => Output_Cache_Flush_Buffers,
			     empty         => Output_Cache_Empty,
			     oWaiting      => OutputCache_Inst_oWaiting,
			     iWaiting      => OutputBuffer_Inst_iWaiting,
			     oReady        => OutputCache_Inst_oReady,
			     inData        => Output_Cache_Input_Data,
			     inAddress     => Rasterizer_Inst_Output_PixelAddress,
			     inEnabled     => Rasterizer_Inst_Output_PixelMask,
			     oAddress      => Output_Address,
			     oData         => Output_Cache_OutputData,
			     oEnabled      => Output_Cache_Enabled,
			     inClearColour => Output_Cache_Input_Clear_Colour);

	OutputCache_Adapter_Inst : OutputCacheMemoryAdapter
		port map(clk             => clk,
			     inData          => Output_Cache_OutputData,
			     outData         => gpu_output,
			     inAddress       => Output_Cache_Address,
			     outAddress      => gpu_address,
			     inEnabled       => Output_Cache_Enabled,
			     outEnabled      => gpu_enabled,
			     memoryWriteFull => gpu_writeFull,
			     iReady          => OutputCache_Inst_oReady,
			     memoryWrite     => gpu_write,
			     iWaiting        => OutputCache_Inst_oWaiting);

	adapter : for i in 0 to 1 generate
		Output_Cache_Input_Data(BPP * i + 14 downto BPP * i + 10) <= std_logic_vector(Rasterizer_Inst_Output_PixelData(i).r);
		Output_Cache_Input_Data(BPP * i + 9 downto BPP * i + 5)   <= std_logic_vector(Rasterizer_Inst_Output_PixelData(i).g);
		Output_Cache_Input_Data(BPP * i + 4 downto BPP * i)       <= std_logic_vector(Rasterizer_Inst_Output_PixelData(i).b);
	end generate adapter;

	Output_Cache_Address <= Output_Address & "00";

end GPUPipeline_RTL;
