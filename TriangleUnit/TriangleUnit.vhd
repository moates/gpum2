library ieee;
use ieee.std_logic_1164.all;

use work.GpuTypes.all;
use work.TriangleTypes.all;

entity TriangleUnit is
	port(
		clk              : in  std_logic;
		rst              : in  std_logic;
		inV              : in  Vector4Type;
		inAttributes     : in  VertexPreTransformAttributes;
		outTriangle		 : out TriangleType;
		outAttributes	 : out TriangleAttributes;
		outBoundingBox	 : out BoxType;
		iReady, oWaiting : in  std_logic;
		Triangle_Unit_Force_Empty : in std_logic;
		oReady, iWaiting, empty : buffer std_logic := '1'
	);
end entity TriangleUnit;

architecture RTL of TriangleUnit is
	signal V0, V1, V2             : Vector4Type;
	signal collect_output_waiting : std_logic;
	signal collect_output_ready   : std_logic;
	signal collect_output_empty : std_logic;
	signal triangle_prep_empty : std_logic;
	signal collect_output_attributes : TriangleAttributes;
begin

	empty <= collect_output_empty and triangle_prep_empty;	

	collectTriangle_Inst : component CollectTriangle
		port map(clk           => clk,
			     rst           => rst,
			     inV           => inV,
			     inAttributes  => inAttributes,
			     outV0         => V0,
			     outV1         => V1,
			     outV2         => V2,
			     outAttributes => collect_output_attributes,
			     iReady        => iReady,
			     oWaiting      => collect_output_waiting,
			     iWaiting      => iWaiting,
			     oReady        => collect_output_ready,
			     empty         => collect_output_empty,
			     Triangle_Unit_Force_Empty => Triangle_Unit_Force_Empty);

	trianglePrep_Inst : component TrianglePrep
		port map(clk              => clk,
			     res              => rst,
			     V0               => V0,
			     V1               => V1,
			     V2               => V2,
			     triangle         => outTriangle,
			     triangleAttribs  => collect_output_attributes,
			     oTriangleAttribs => outAttributes,
			     oBoundingBox     => outBoundingBox,
			     iReady           => collect_output_ready,
			     oWaiting         => oWaiting,
			     oReady           => oReady,
			     iWaiting         => collect_output_waiting,
			     empty            => triangle_prep_empty);
end architecture RTL;
