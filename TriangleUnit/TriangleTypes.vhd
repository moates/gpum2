library ieee;
use ieee.std_logic_1164.all;
use work.GpuTypes.all;

package TriangleTypes is
	component TrianglePrep
		port(clk, res                : in  std_logic;
			 V0, V1, V2              : in  Vector4Type;
			 triangle                : out TriangleType;
			 triangleAttribs         : in  TriangleAttributes;
			 oTriangleAttribs        : out TriangleAttributes;
			 oBoundingBox            : out BoxType;
			 iReady, oWaiting        : in  std_logic;
			 oReady, iWaiting, empty : buffer std_logic);
	end component TrianglePrep;

	component CollectTriangle
		port(clk                     : in  std_logic;
			 rst                     : in  std_logic;
			 inV                     : in  Vector4Type;
			 inAttributes            : in  VertexPreTransformAttributes;
			 outV0, outV1, outV2     : out Vector4Type;
			 outAttributes           : out TriangleAttributes;
			 iReady, oWaiting        : in  std_logic;
			 Triangle_Unit_Force_Empty : in std_logic;
			 iWaiting, oReady, empty : buffer std_logic := '0');
	end component CollectTriangle;
end package TriangleTypes;

package body TriangleTypes is
end package body TriangleTypes;
