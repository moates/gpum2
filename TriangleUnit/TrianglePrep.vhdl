library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity TrianglePrep is
	port(
		clk, res                : in  std_logic;
		V0, V1, V2              : in  Vector4Type;
		triangle                : out TriangleType;
		triangleAttribs         : in  TriangleAttributes;
		oTriangleAttribs        : out TriangleAttributes;
		oBoundingBox            : out BoxType;
		iReady, oWaiting        : in  std_logic;
		oReady, iWaiting 		: buffer std_logic;
		empty					: out std_logic := '1');

end TrianglePrep;

architecture TrianglePrep_RTL of TrianglePrep is
	signal wAwB, yAyB, xAxB, xByA, xAyB, alpha, beta, gamma     : VectorElement;
	signal c0_0, c1_0, c2_0, c0_1, c1_1, c2_1, c0_2, c1_2, c2_2 : signed(BPC downto 0);
	signal VA, VB                                               : Vector4Type;
	type PipelineType is array (2 downto 0) of std_logic;
	signal pipeline                                                                                : PipelineType := (others => '0');
	signal max_1_1, min_1_1, max_1_2, min_1_2, max_2, min_2, m_0_0, m_0_1, m_0_2, max_0_3, min_0_3 : VectorELement;

	function max(left, right : VectorElement) return VectorElement is
	begin
		if left > right then
			return left;
		else
			return right;
		end if;
	end function max;

	function min(left, right : VectorElement) return VectorElement is
	begin
		if left < right then
			return left;
		else
			return right;
		end if;
	end function min;

	function roundDownToTile(value : VectorElement) return integer is
	begin
		return (to_integer(value(value'high downto 3))) * 8;
	end function roundDownToTile;

	function roundUpToTile(value : VectorElement) return integer is
	begin
		if value > (value(value'high downto 3) & "000") then
			return (to_integer(value(value'high downto 3))) * 8 + TileSize;
		else
			return (to_integer(value(value'high downto 3))) * 8;
		end if;
	end function roundUpToTile;
	signal z0_1, z0_2 : DepthElement;

begin                                   -- TrianglePrep_RTL

	process(clk, res)
		variable outputProcess, inputProcess : integer range 2 downto 0 := 0;
		variable VC, VTemp                   : Vector4Type              := (others => (others => '0'));
		variable pipelineFull                : std_logic                := '0';
		variable pipelineContents            : integer range 0 to 4     := 0;
	begin                               -- process
		if res = '0' then               -- asynchronous reset (active low)
			pipelineFull  := '0';
			pipeline      <= (others => '0');
			oReady        <= '0';
			iWaiting      <= '1';
			outputProcess := 0;
			inputProcess  := 0;
		elsif clk'event and clk = '1' then -- rising clock edge
			if outputProcess = 0 then
				if pipeline(pipeline'low) = '1' then
					if oReady = '1' and oWaiting = '0' then
						pipelineFull := '1';
					else
						pipelineFull := '0';
						triangle.a0  <= alpha;
						triangle.b0  <= beta;
						triangle.g0  <= gamma;

						oTriangleAttribs.r0 <= c0_2;
						oTriangleAttribs.r1 <= c1_2;
						oTriangleAttribs.r2 <= c2_2;
						oTriangleAttribs.z1 <= z0_2;

						oBoundingBox.xMax <= roundUpToTile(max_2);
						oBoundingBox.xMin <= roundDownToTile(min_2);
						outputProcess     := 1;
					end if;
				end if;
			elsif outputProcess = 1 then
				triangle.a1 <= alpha;
				triangle.b1 <= beta;
				triangle.g1 <= gamma;
				
				oTriangleAttribs.g0 <= c0_2;
				oTriangleAttribs.g1 <= c1_2;
				oTriangleAttribs.g2 <= c2_2;
				oTriangleAttribs.z2 <= z0_2;

				oBoundingBox.yMax <= roundUpToTile(max_2);
				oBoundingBox.yMin <= roundDownToTile(min_2);
				outputProcess     := 2;
			else
				triangle.a2 <= alpha;
				triangle.b2 <= beta;
				triangle.g2 <= gamma;

				oTriangleAttribs.b0 <= c0_2;
				oTriangleAttribs.b1 <= c1_2;
				oTriangleAttribs.b2 <= c2_2;
				oTriangleAttribs.z0 <= z0_2;

				outputProcess    := 0;
				oReady           <= '1';				
			end if;

			if pipelineFull = '0' then
				--First Stage
				yAyB    <= VA.y - VB.y;
				xAxB    <= VB.x - VA.x;
				wAwB    <= Mul32(VA.w, VB.w);
				xByA    <= Mul32(VA.y, VB.x);
				xAyB    <= Mul32(VA.x, VB.y);
				c0_1    <= c0_0;
				c1_1    <= c1_0;
				c2_1    <= c2_0;
				z0_1    <= unsigned(VA.z(BPD-1 downto 0));
				max_1_1 <= max(max(m_0_0, m_0_1), m_0_2);
				max_1_2 <= max_0_3;
				min_1_1 <= min(min(m_0_0, m_0_1), m_0_2);
				min_1_2 <= min_0_3;

				--SecondStage
				alpha <= Mul32(yAyB, wAwB);
				beta  <= Mul32(xAxB, wAwB);
				gamma <= Mul32((xAyB - xByA), wAwB);
				c0_2  <= c0_1;          --c0_1 - C2_1;
				c1_2  <= c1_1;          --c1_1 - c2_1;
				c2_2  <= c2_1;
				z0_2 <= z0_1;
				max_2 <= min(max_1_1, max_1_2);
				min_2 <= max(min_1_1, min_1_2);

				for i in pipeline'low to pipeline'high - 1 loop
					pipeline(i) <= pipeline(i + 1);
				end loop;               -- i

				if inputProcess = 0 then
					if iReady = '1' then
						iWaiting         <= '1';						

						VA <= V1;
						VB <= V2;
						VC := V0;

						c0_0 <= signed(triangleAttribs.r0);
						c1_0 <= signed(triangleAttribs.r1);
						c2_0 <= signed(triangleAttribs.r2);

						m_0_0   <= V0.x;
						m_0_1   <= V1.x;
						m_0_2   <= V2.x;
						max_0_3 <= to_signed(SWidth - 1, 32);
						min_0_3 <= to_signed(0, 32);

						inputProcess            := 1;
						pipeline(pipeline'high) <= '1';
					end if;
				elsif inputProcess = 1 then
					VTemp := VA;
					VA    <= VB;
					VB    <= VC;
					VC    := VTemp;

					c0_0 <= signed(triangleAttribs.g0);
					c1_0 <= signed(triangleAttribs.g1);
					c2_0 <= signed(triangleAttribs.g2);

					m_0_0   <= V0.y;
					m_0_1   <= V1.y;
					m_0_2   <= V2.y;
					max_0_3 <= to_signed(SHeight - 1, 32);
					min_0_3 <= to_signed(0, 32);

					inputProcess            := 2;
					pipeline(pipeline'high) <= '0';
				else
					VA <= VB;
					VB <= VC;

					c0_0 <= signed(triangleAttribs.b0);
					c1_0 <= signed(triangleAttribs.b1);
					c2_0 <= signed(triangleAttribs.b2);

					inputProcess := 0;
				end if;
			end if;

			if iReady = '1' and iWaiting = '1' then
				iWaiting <= '0';
				pipelineContents := pipelineContents + 1;
			end if;

			if oWaiting = '1' and oReady = '1' then
				oReady <= '0';
				pipelineContents := pipelineContents - 1;
			end if;

			if pipelineContents = 0 then
				empty <= '1';
			else
				empty <= '0';
			end if;

		end if;
	end process;

end TrianglePrep_RTL;
