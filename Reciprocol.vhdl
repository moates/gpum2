library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity Reciprocal is
  
  generic (
    precision : natural := 3;
    stages : natural := 4);

  port (
    clk, res : in std_logic;
    value  : in  std_logic_vector(2**precision-1 downto 0);
    result : out std_logic_vector(2**precision-1 downto 0));

end Reciprocal;

architecture Reciprocal_RTL of Reciprocal is
                              
  type ReciprocalTable is array(2**precision-1 downto 0) of unsigned(2**precision*2-1 downto 0);
  type Temporaries is array (Stages-1 downto 0) of unsigned(2**precision*2-1 downto 0);
  signal InitialValues : ReciprocalTable;
  signal Xi, Vi : Temporaries := (others => (others => '0'));
begin
  reciprocalInitialValues: for i in 0 to 2**precision-1 generate
    constant x : REAL := real(1) / (real(2**i));
    constant xn : unsigned(2**precision*2-1 downto 0) := to_unsigned(integer(x*real(2**(precision*2))), 2**precision*2);
  begin
    InitialValues(i) <= xn;                                    
  end generate reciprocalInitialValues;                             

  process (clk, res)
    variable Xt : unsigned(2**precision*4-1 downto 0);
    variable Xtl : unsigned(2**precision*6-1 downto 0);
    variable InitialIndex : integer range 0 to 2**precision-1;
  begin  -- process
    if res = '0' then                   -- asynchronous reset (active low)
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      for i in Stages-1 downto 1 loop      
        Xt := to_unsigned(2**precision,2**precision*2) - Xi(i-1) * Vi(i-1);
        Xtl := Xt * Xi(i-1);
        Xi(i) <= Xtl(2**precision*6-1 downto 2**precision*4);
        Vi(i) <= Vi(i-1);
      end loop;  -- i

      for i in 2**precision-1  to 0 loop
        if value(i)='1' then
          InitialIndex := i;
          exit;
        end if;          
      end loop;
                    
      Xi(Xi'low) <= InitialValues(InitialIndex);
      Vi(Vi'low) <= (2**precision-1 downto 0 => '0')&unsigned(value);
    end if;
  end process;

end Reciprocal_RTL;
