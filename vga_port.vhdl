library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity vga is
	generic(
		groupSize    : integer := 2;
		bpp          : integer := 16;
		screenWidth  : integer := 10;
		screenHeight : integer := 9
	);

	port(
		vga_address                        : buffer Address; -- Address for next group of pixels
		data                               : in  std_logic_vector(31 downto 0); -- Group of 2 pixels
		memory_read, memory_request        : out std_logic;
		memory_read_empty                  : in  std_logic;
		pixel                              : out std_logic_vector(bpp - 1 downto 0);
		clk, res                           : in  std_logic;
		vsync, hsync, vga_clk, blank, sync : out std_logic);

end vga;

architecture vga_rtl of vga is
	signal pixelData : std_logic_vector(31 downto 0) := (others => '0');
	type timing is (front_porch, sync_pulse, back_porch, visible);
	signal pixel_index                               : integer range 1 downto 0 := 0;
	signal vsync_output, hsync_output, pixel_enabled : std_logic                := '0';
begin                                   -- vga_rtl

	pixel <= pixelData(bpp * (pixel_index + 1) - 1 downto bpp * pixel_index);
	vsync <= not vsync_output;
	hsync <= not hsync_output;
	sync  <= vsync_output nor hsync_output;
	blank <= pixel_enabled;
	--vga_clk <= clk;

	-- purpose: Coordinates loading pixels from memory
	-- type   : sequential
	-- inputs : clk, res
	-- outputs: address
	videoSignal : process(clk, res)
		variable horizontal, vertical : timing                                          := visible;
		variable horizontal_counter   : integer range 0 to 640                          := 0;
		variable vertical_counter     : integer range 0 to 480                          := 0;
		variable t                    : integer range 0 to 1                            := 0;
		variable incrementVPos        : std_logic                                       := '0';
		variable x_slv                : std_logic_vector(17 downto 0);
		variable pixelIndex           : integer range ColourBurstWidth * 2 - 1 downto 0 := 0;
		variable input_counter        : integer range 0 to 31                           := 0;
		type states is (Init, Loading, Started);
		variable currentState : states := Init;
	begin                               -- process videoSignal
		if res = '0' then               -- asynchronous reset (active low)

		elsif clk'event and clk = '1' then -- rising clock edge
			memory_read    <= '0';
			memory_request <= '0';

			case currentState is
				when Init =>
					vga_address <= (others => '0');
					memory_request <= '1';
					currentState := Loading;
				when Loading =>
					if memory_read_empty = '0' then
						currentState := Started;
					end if;
				when Started =>
					if t = 0 then
						t       := 1;
						vga_clk <= '0';

						case horizontal is
							when front_porch =>
								if horizontal_counter = 15 then
									horizontal         := sync_pulse;
									horizontal_counter := 0;
									hsync_output       <= '1';
								else
									horizontal_counter := horizontal_counter + 1;
								end if;
							when sync_pulse =>
								if horizontal_counter = 95 then
									horizontal         := back_porch;
									horizontal_counter := 0;
									hsync_output       <= '0';
								else
									horizontal_counter := horizontal_counter + 1;
								end if;
							when back_porch =>
								if horizontal_counter = 47 then
									horizontal         := visible;
									horizontal_counter := 0;
									pixelIndex         := 0;
								else
									horizontal_counter := horizontal_counter + 1;
								end if;
							when visible =>
								

								if vertical = visible then									
									if pixelIndex = 0 then
										--pixelBuffer <= 1 - pixelBuffer;									
										if input_counter = 0 then
											memory_request <= '1';
											if horizontal_counter = SWidth - 64 then
												if vertical_counter = SHeight - 1 then
													x_slv := AddrResolve(0, 0);
												else
													x_slv := AddrResolve(0, vertical_counter + 1);
												end if;
											else
												x_slv := AddrResolve(horizontal_counter + 32, vertical_counter);
											end if;
											vga_address <= x_slv(17 downto 0);
											input_counter := 1;
										elsif input_counter = 31 then
											input_counter := 0;
										else
											input_counter := input_counter + 1;
										end if;
										
										memory_read   <= '1';
										pixelData     <= data;										
									end if;
									
									if pixelIndex = 1 then
										pixelIndex := 0;

									else
										pixelIndex := 1;
									end if;
								end if;
								
								if horizontal_counter = 639 then
									horizontal         := front_porch;
									horizontal_counter := 0;
									incrementVPos      := '1';
								else
									horizontal_counter := horizontal_counter + 1;
								end if;
						end case;

						if incrementVPos = '1' then
							incrementVPos := '0';

							case vertical is
								when front_porch =>
									if vertical_counter = 9 then
										vertical         := sync_pulse;
										vertical_counter := 0;
										vsync_output     <= '1';
									else
										vertical_counter := vertical_counter + 1;
									end if;
								when sync_pulse =>
									if vertical_counter = 1 then
										vertical         := back_porch;
										vertical_counter := 0;
										vsync_output     <= '0';
									else
										vertical_counter := vertical_counter + 1;
									end if;
								when back_porch =>
									if vertical_counter = 32 then
										vertical         := visible;
										vertical_counter := 0;
									else
										vertical_counter := vertical_counter + 1;
									end if;
								when visible =>
									if vertical_counter = 479 then
										vertical         := front_porch;
										vertical_counter := 0;
									else
										vertical_counter := vertical_counter + 1;
									end if;
								when others => null;
							end case;
						end if;

						pixel_index <= pixelIndex;

						if vertical = visible and horizontal = visible then
							pixel_enabled <= '1';
						else
							pixel_enabled <= '0';
						end if;
					else
						t       := 0;
						vga_clk <= '1';
					end if;
			end case;

		end if;
	end process videoSignal;

end vga_rtl;
