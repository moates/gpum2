# GPU Mark - 2 #

My second attempt at a 3D graphics processor capable of running on an FPGA at high speed. Tested and designed for DE270, this code is quite complex and has some bugs/quirks. Hopefully some of the components (e.g. the fill algorithm) are useful.

Features:

* Fixed function graphics pipeline
* Single pipelined transformation unit (1 triangle / 12 clock cycles)
* 2n configurable pixel fill units
* Tile based renderer
* 4 16x16 onboard textures
* Intelligent output cache algorithm
* Memory read/write scheduler
* VGA controller
* Nios II Connection
* Basic OpenGL 1.x support (in separate repository)

# Contacts #
Contact me if you want any further information. 

This project is licensed under the Apache License, use this stuff to build cool things :). Please mention me / let me know if you make anything cool with it!

# License #
Copyright 2014 Michael Oates

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.