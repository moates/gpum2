library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_bit.all;

use work.GpuTypes.all;

package OutputCacheTypes is
	component OutputCacheMemory
		generic(BYTE_WIDTH     : natural := 16;
			    BYTEEN_A_WIDTH : natural := 2;
			    WORD_A_WIDTH   : natural := 32;
			    ADDR_A_WIDTH   : natural := 11;
			    WORD_B_WIDTH   : natural := 128;
			    ADDR_B_WIDTH   : natural := 9);
		port(address_a : IN  STD_LOGIC_VECTOR(ADDR_A_WIDTH - 1 DOWNTO 0);
			 address_b : IN  STD_LOGIC_VECTOR(ADDR_B_WIDTH - 1 DOWNTO 0);
			 byteena_a : IN  STD_LOGIC_VECTOR(BYTEEN_A_WIDTH - 1 DOWNTO 0) := (OTHERS => '1');
			 clock     : IN  STD_LOGIC                                          := '1';
			 data_a    : IN  STD_LOGIC_VECTOR(WORD_A_WIDTH - 1 DOWNTO 0);
			 data_b    : IN  STD_LOGIC_VECTOR(WORD_B_WIDTH - 1 DOWNTO 0);
			 wren_a    : IN  STD_LOGIC                                          := '0';
			 wren_b    : IN  STD_LOGIC                                          := '0';
			 q_b       : OUT STD_LOGIC_VECTOR(WORD_B_WIDTH - 1 DOWNTO 0));
	end component OutputCacheMemory;

	component OutputCacheSet
		generic(
			IDENTIFIER_WIDTH : natural;
			SET_WIDTH        : natural;
			CLOCK_WIDTH      : natural);
		port(
			clk                                 : in  std_logic;
			iAddress                            : in  std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
			oAddress, tIndex                    : out std_logic_vector(SET_WIDTH - 1 downto 0);
			tAddress                            : out std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
			iReady, oWaiting, tComplete, tClear : in  std_logic;
			iWaiting, oReady, tReady            : out std_logic;
			empty                               : out std_logic);
	end component;

	component TransferUnit
		generic(
			IDENTIFER_WIDTH   : natural;
			SET_WIDTH         : natural;
			SET_COUNT_WIDTH   : natural;
			BURST_WIDTH       : natural;
			BURST_COUNT_WIDTH : natural;
			BPP               : natural;
			TILE_WIDTH        : natural);
		port(
			clk                          : in  std_logic;
			iReady, tBlockCleared : in  std_logic;
			oWaiting                     : in  std_logic;
			iData                        : in  std_logic_vector(BURST_WIDTH - 1 downto 0);
			iAddress                     : in  std_logic_vector(IDENTIFER_WIDTH - 1 downto 0);
			iIndex                       : in  std_logic_vector(SET_WIDTH - 1 downto 0);
			iSetIndex                    : in  std_logic_vector(SET_COUNT_WIDTH - 1 downto 0);
			oIndex                       : out std_logic_vector(SET_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH - 1 downto 0);
			clearColour                  : in  std_logic_vector(BPP - 1 downto 0);
			oData                        : out std_logic_vector(BURST_WIDTH - 1 downto 0);
			oEnabled                     : out std_logic_vector(2 ** BURST_COUNT_WIDTH - 1 downto 0);
			oAddress,oTileAddress                     : out std_logic_vector(IDENTIFER_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH - 1 downto 0);
			oReady, iWaiting, cacheClear, tComplete, tClearBlock : out std_logic);
	end component;

	component BitEnabledCache
		generic(
			WORD_A_WIDTH : natural := 2;
			ADDR_A_WIDTH : natural := 9;
			WORD_B_WIDTH : natural := 8;
			ADDR_B_WIDTH : natural := 11);
		port(address_a : IN  STD_LOGIC_VECTOR(ADDR_A_WIDTH - 1 DOWNTO 0);
			 address_b : IN  STD_LOGIC_VECTOR(ADDR_B_WIDTH - 1 DOWNTO 0);
			 clock     : IN  STD_LOGIC := '1';
			 data_a    : IN  STD_LOGIC_VECTOR(WORD_A_WIDTH - 1 DOWNTO 0);
			 data_b    : IN  STD_LOGIC_VECTOR(WORD_B_WIDTH - 1 DOWNTO 0);
			 wren_a    : IN  STD_LOGIC := '0';
			 wren_b    : IN  STD_LOGIC := '0';
			 q_a       : OUT STD_LOGIC_VECTOR(WORD_A_WIDTH - 1 DOWNTO 0);
			 q_b       : OUT STD_LOGIC_VECTOR(WORD_B_WIDTH - 1 DOWNTO 0);
			 byteena_a : IN  STD_LOGIC_VECTOR(WORD_A_WIDTH - 1 DOWNTO 0);
			 byteena_b : IN  STD_LOGIC_VECTOR(WORD_B_WIDTH - 1 DOWNTO 0));
	end component BitEnabledCache;
	
	component TileCacheController
		generic(TILE_WIDTH           : natural := 3;
			    PIXEL_ADDR_WIDTH     : natural := 1;
			    TILES_PER_WORD_WIDTH : natural := 4;
			    BURST_WIDTH          : natural := 128;
			    ADDRESS_WIDTH        : natural := 18;
			    BURST_COUNT_WIDTH    : natural := 3);
		port(clk                     : in  std_logic;
			 addr                    : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
			 clear, tileCacheEmpty   : buffer std_logic                                                    := '0';
			 clearTile, flushBuffers : in  std_logic;
			 clearColour             : in  std_logic_vector(15 downto 0);
			 oData                   : out std_logic_vector(BURST_WIDTH - 1 downto 0);
			 oAddress                : buffer std_logic_vector(ADDRESS_WIDTH - 1 downto 0) := (others => '0');
			 oWaiting                : in  std_logic;
			 oReady                  : buffer std_logic                                                    := '0');
	end component TileCacheController;
	
	component OutputCacheMemoryAdapter
		port(clk                     : in  std_logic;
			 inData                  : in  std_logic_vector(127 downto 0);
			 outData                 : out std_logic_vector(31 downto 0);
			 inAddress               : in  Address;
			 outAddress              : out Address;
			 inEnabled               : in  std_logic_vector(7 downto 0);
			 outEnabled              : out std_logic_vector(1 downto 0);
			 memoryWriteFull, iReady : in  std_logic;
			 memoryWrite, iWaiting   : buffer std_logic := '0');
	end component OutputCacheMemoryAdapter;

end package OutputCacheTypes;

package body OutputCacheTypes is
end package body OutputCacheTypes;

