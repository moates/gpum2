LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY altera_mf;
USE altera_mf.all;

ENTITY OutputCacheMemory IS
	GENERIC (
		BYTE_WIDTH : natural  := 16;
		BYTEEN_A_WIDTH : natural := 2;
		WORD_A_WIDTH : natural  := 32;
		ADDR_A_WIDTH : natural  := 11;
		WORD_B_WIDTH : natural  := 128;
		ADDR_B_WIDTH : natural := 9 
	);	
	PORT
	(
		address_a		: IN STD_LOGIC_VECTOR (ADDR_A_WIDTH-1 DOWNTO 0);
		address_b		: IN STD_LOGIC_VECTOR (ADDR_B_WIDTH-1 DOWNTO 0);
		byteena_a		: IN STD_LOGIC_VECTOR (BYTEEN_A_WIDTH-1 DOWNTO 0) :=  (OTHERS => '1');
		clock		: IN STD_LOGIC  := '1';
		data_a		: IN STD_LOGIC_VECTOR (WORD_A_WIDTH-1 DOWNTO 0);
		data_b		: IN STD_LOGIC_VECTOR (WORD_B_WIDTH-1 DOWNTO 0);
		wren_a		: IN STD_LOGIC := '0';
		wren_b		: IN STD_LOGIC  := '0';
		q_b		: OUT STD_LOGIC_VECTOR (WORD_B_WIDTH-1 DOWNTO 0)
	);
END OutputCacheMemory;


ARCHITECTURE SYN OF OutputCacheMemory IS

	SIGNAL sub_wire1	: STD_LOGIC_VECTOR (127 DOWNTO 0);



	COMPONENT altsyncram
	GENERIC (
		address_reg_b		: STRING;
		byte_size		: NATURAL;
		clock_enable_input_a		: STRING;
		clock_enable_input_b		: STRING;
		clock_enable_output_a		: STRING;
		clock_enable_output_b		: STRING;
		indata_reg_b		: STRING;
		intended_device_family		: STRING;
		lpm_type		: STRING;
		numwords_a		: NATURAL;
		numwords_b		: NATURAL;
		operation_mode		: STRING;
		outdata_aclr_a		: STRING;
		outdata_aclr_b		: STRING;
		outdata_reg_a		: STRING;
		outdata_reg_b		: STRING;
		power_up_uninitialized		: STRING;
		read_during_write_mode_mixed_ports		: STRING;
		widthad_a		: NATURAL;
		widthad_b		: NATURAL;
		width_a		: NATURAL;
		width_b		: NATURAL;
		width_byteena_a		: NATURAL;
		width_byteena_b		: NATURAL;
		wrcontrol_wraddress_reg_b		: STRING
	);
	PORT (
			address_a		: IN STD_LOGIC_VECTOR (ADDR_A_WIDTH-1 DOWNTO 0);
			address_b		: IN STD_LOGIC_VECTOR (ADDR_B_WIDTH-1 DOWNTO 0);
			byteena_a		: IN STD_LOGIC_VECTOR (BYTEEN_A_WIDTH-1 DOWNTO 0) :=  (OTHERS => '1');
			clock0		: IN STD_LOGIC  := '1';
			data_a		: IN STD_LOGIC_VECTOR (WORD_A_WIDTH-1 DOWNTO 0);
			data_b		: IN STD_LOGIC_VECTOR (WORD_B_WIDTH-1 DOWNTO 0);
			wren_a		: IN STD_LOGIC  := '0';
			wren_b		: IN STD_LOGIC  := '0';
			q_a		: OUT STD_LOGIC_VECTOR (WORD_A_WIDTH-1 DOWNTO 0);
			q_b		: OUT STD_LOGIC_VECTOR (WORD_B_WIDTH-1 DOWNTO 0)
	);
	END COMPONENT;

BEGIN
	q_b    <= sub_wire1(127 DOWNTO 0);

	altsyncram_component : altsyncram
	GENERIC MAP (
		address_reg_b => "CLOCK0",
		byte_size => 8,
		clock_enable_input_a => "BYPASS",
		clock_enable_input_b => "BYPASS",
		clock_enable_output_a => "BYPASS",
		clock_enable_output_b => "BYPASS",
		indata_reg_b => "CLOCK0",
		intended_device_family => "Cyclone II",
		lpm_type => "altsyncram",
		numwords_a => 2**ADDR_A_WIDTH,
		numwords_b => 2**ADDR_B_WIDTH,
		operation_mode => "BIDIR_DUAL_PORT",
		outdata_aclr_a => "NONE",
		outdata_aclr_b => "NONE",
		outdata_reg_a => "CLOCK0",
		outdata_reg_b => "CLOCK0",
		power_up_uninitialized => "FALSE",
		read_during_write_mode_mixed_ports => "DONT_CARE",
		widthad_a => ADDR_A_WIDTH,
		widthad_b => ADDR_B_WIDTH,
		width_a => WORD_A_WIDTH,
		width_b => WORD_B_WIDTH,
		width_byteena_a => BYTEEN_A_WIDTH,
		width_byteena_b => 1,
		wrcontrol_wraddress_reg_b => "CLOCK0"
	)
	PORT MAP (
		byteena_a => byteena_a,
		clock0 => clock,
		wren_a => wren_a,
		address_b => address_b,
		data_b => data_b,
		wren_b => wren_b,
		address_a => address_a,
		data_a => data_a,
		q_b => sub_wire1,
		q_a => open		
	);



END SYN;
