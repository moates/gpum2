library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;
use work.OutputCacheTypes.all;

entity OutputCache is
	generic(
		ROWS_WIDTH        : natural := 4;
		ROW_SIZE          : natural := 1;
		IDENTIFIER_WIDTH  : natural := 9;
		SET_WIDTH         : natural := 3;
		SET_COUNT_WIDTH   : natural := 4;
		BURST_WIDTH       : natural := 128;
		BURST_COUNT_WIDTH : natural := 3;
		BPP               : natural := 16;
		TILE_WIDTH        : natural := 6;
		CLOCK_WIDTH       : natural := 2;
		INPUT_WIDTH       : natural := 1;
		TILES_PER_WORD_WIDTH : natural := 4);

	port(
		clk              : in  std_logic;
		iReady, oWaiting, iClear : in  std_logic;
		iWaiting, oReady, empty : buffer std_logic := '0';
		inData           : in  std_logic_vector(BPP * 2 ** INPUT_WIDTH - 1 downto 0);
		inAddress        : in  std_logic_vector(IDENTIFIER_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - INPUT_WIDTH - 1 downto 0);
		inEnabled        : in  std_logic_vector(2 ** INPUT_WIDTH - 1 downto 0);
		inClearColour	 : in std_logic_vector(15 downto 0);
		--oAddress         : out std_logic_vector(IDENTIFIER_WIDTH+SET_COUNT_WIDTH+TILE_WIDTH-BURST_COUNT_WIDTH-1 downto 0);
		oAddress         : buffer std_logic_vector(IDENTIFIER_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH - 1 downto 0);
		oData            : out std_logic_vector(BURST_WIDTH - 1 downto 0);
		oEnabled         : out std_logic_vector(2 ** BURST_COUNT_WIDTH - 1 downto 0)		
	);

end OutputCache;

architecture OutputCache of OutputCache is
	constant BYTES_PER_PIXEL : natural := 32 / BPP;
	constant ADDRESS_WIDTH : natural  := IDENTIFIER_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH;

	-- Type declarations
	type CacheAddressArrayType is array (2 ** SET_COUNT_WIDTH - 1 downto 0) of std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
	type CacheIndexArrayType is array (2 ** SET_COUNT_WIDTH - 1 downto 0) of std_logic_vector(SET_WIDTH - 1 downto 0);
	type FlushBufferStates is (WaitingState,FlushingBuffersState,ClearingTilesState);
	subtype CacheSignalType is std_logic_vector(2 ** SET_COUNT_WIDTH - 1 downto 0);

	-- Signals
	signal CACHE_byteena_a_0, CACHE_byteena_a_1                     : std_logic_vector(2 ** INPUT_WIDTH * BYTES_PER_PIXEL - 1 downto 0) := (others => '1');
	signal CACHE_wraddress                                          : std_logic_vector(SET_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - INPUT_WIDTH - 1 downto 0);
	signal CACHE_wren_a                                             : std_logic                                                         := '0';
	signal SET_iAddress_0, SET_iAddress_1                           : std_logic_vector(IDENTIFIER_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - INPUT_WIDTH - 1 downto 0);
	signal SET_tReady, SET_oReady, SET_iWaiting                     : CacheSignalType                                                   := (others => '0');
	signal SET_oAddress, SET_tIndex                                 : CacheIndexArrayType                                               := (others => (others => '0'));
	signal SET_tAddress                                             : CacheAddressArrayType                                             := (others => (others => '0'));
	signal SET_activeWrite_0, SET_activeWrite_1, SET_activeTransfer : integer range 2 ** SET_COUNT_WIDTH downto 0                       := SET_COUNT_WIDTH;
	signal Transfer_Ready, Transfer_Complete                        : std_logic                                                         := '0';
	signal Transfer_Unit_Input_Address                              : std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
	signal Transfer_Unit_Input_Index                                : std_logic_vector(SET_WIDTH - 1 downto 0);
	signal CACHE_WriteData_0, CACHE_WriteData_1                     : std_logic_vector(2 ** INPUT_WIDTH * BPP - 1 downto 0);
	signal Transfer_Unit_Output_Index                               : std_logic_vector(SET_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH - 1 downto 0);
	signal Transfer_Unit_Input_Data                                 : std_logic_vector(BURST_WIDTH - 1 downto 0);
	signal SET_activeTransferIndex                                  : std_logic_vector(SET_COUNT_WIDTH - 1 downto 0);
	signal SET_iReady_0                                             : std_logic;
	signal Transfer_Unit_Input_Waiting   : std_logic;
	signal CACHE_wordena_a_1, CACHE_wordena_a_0                     : STD_LOGIC_VECTOR(2 ** INPUT_WIDTH - 1 DOWNTO 0);
	signal CACHE_wren_b                                             : STD_LOGIC;
	signal Transfer_Unit_Input_Block_Cleared, Transfer_Unit_Output_Clear_Block : std_logic;
	signal Tile_Cache_Output_Tile_Cache_Empty, Tile_Cache_Input_Flush_Buffers, Set_All_Empty : std_logic;
	signal SET_empty : std_logic_vector(2**SET_COUNT_WIDTH-1 downto 0);	
	signal Clearing_State : FlushBufferStates := WaitingState;
	signal SET_tClear : std_logic;
	signal Tile_Cache_Output_Ready : std_logic;
	signal Tile_Cache_Output_Data : std_logic_vector(BURST_WIDTH - 1 downto 0);
	signal Tile_Cache_Output_Address : std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
	signal Transfer_Unit_Output_Waiting : std_logic;
	signal Transfer_Unit_Output_Data : std_logic_vector(BURST_WIDTH - 1 downto 0);
	signal Transfer_Unit_Output_Enabled : std_logic_vector(2 ** BURST_COUNT_WIDTH - 1 downto 0);
	signal Transfer_Unit_Output_Address : std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
	signal Transfer_Unit_Output_Ready : std_logic;
	signal Tile_Cache_Output_Waiting : std_logic;
	signal Clear_Colour : std_logic_vector(15 downto 0) := (others => '0');
	signal Transfer_Unit_Output_Tile_Address : std_logic_vector(IDENTIFIER_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH - 1 downto 0);

begin                                   -- OutputCache
	Clear_Colour <= inClearColour;
	Set_All_Empty <= and_reduce(SET_empty);
	
	flushProcess : process (clk) is
	begin
		if rising_edge(clk) then
			if iClear = '0' then
				empty <= '0';	
			end if;
		
			if iClear = '1' and empty = '0' then
				case Clearing_State is 
					when WaitingState =>
						Clearing_State <= FlushingBuffersState;
						SET_tClear <= '1';
					when FlushingBuffersState =>
						if Set_All_Empty = '1' then
							Clearing_State <= ClearingTilesState;
							SET_tClear <= '0';
							Tile_Cache_Input_Flush_Buffers <= '1';
						end if;
					when ClearingTilesState =>
						if Tile_Cache_Output_Tile_Cache_Empty = '1' then
							Tile_Cache_Input_Flush_Buffers <= '0';
							empty <= '1';
							Clearing_State <= WaitingState;
						end if;
				end case; 	
			else
				SET_tClear <= '0';
				Tile_Cache_Input_Flush_Buffers <= '0';
			end if;			
		end if;
	end process flushProcess;
	

	readCacheProcess : process(clk)
		variable transfering                : std_logic                               := '0';
		variable transferIndex              : integer range 0 to 2 ** SET_COUNT_WIDTH := 0;		
	begin                               -- process      
		if clk'event and clk = '1' then -- rising clock edge
			if Transfer_Complete = '1' then
				transfering := '0';
				Transfer_Ready <= '0';
			end if;		
					
			if transfering = '0' then
				for i in 0 to 2 ** SET_COUNT_WIDTH - 1 loop
					if SET_tReady(i) = '1' and transferIndex /= i then
						transferIndex  := i;
						transfering    := '1';
						Transfer_Ready <= '1';
						exit;
					end if;
				end loop;               -- i        
			end if;

			if transfering = '0' then
				transferIndex := 2 ** SET_COUNT_WIDTH;
			else
				Transfer_Unit_Input_Address <= SET_tAddress(transferIndex);
				Transfer_Unit_Input_Index   <= SET_tIndex(transferIndex);
			end if;

			SET_activeTransfer <= transferIndex;
		end if;
	end process;
	
	iWaiting <= SET_iWaiting(to_integer(unsigned(inAddress(SET_COUNT_WIDTH+TILE_WIDTH-INPUT_WIDTH-1 downto TILE_WIDTH-INPUT_WIDTH))));

	writeCacheProcess : process(clk) is
		variable waiting : std_logic := '0';
	begin
		if rising_edge(clk) then

			-- Determine if we are ready for our next block of data
			-- Can only occur when all caches are waitiing
			if Clearing_State /= WaitingState then
				waiting  := '0';
			elsif SET_activeWrite_0 = 2**SET_COUNT_WIDTH then
				waiting := '1';
			else
				waiting := SET_iWaiting(SET_activeWrite_0);
			end if;        			

			if waiting = '1' then
				-- If we are waiting and data is ready, clock it into the pipeline  	  
				if iReady = '1' then
					CACHE_writeData_0 <= inData;
					CACHE_wordena_a_0 <= inEnabled;

					for i in 0 to 2 ** INPUT_WIDTH - 1 loop
						CACHE_writeData_0((i + 1) * BPP - 1) <= inEnabled(i);
						CACHE_byteena_a_0(i * 2)             <= inEnabled(i);
						CACHE_byteena_a_0(i * 2 + 1)         <= inEnabled(i);
					end loop;

					SET_activeWrite_0 <= to_integer(unsigned(inAddress(SET_COUNT_WIDTH+TILE_WIDTH-INPUT_WIDTH-1 downto TILE_WIDTH-INPUT_WIDTH)));

					SET_iAddress_0 <= inAddress;
				else
					SET_activeWrite_0 <= 2**SET_COUNT_WIDTH;
				end if;

				-- Push values through the pipeline 	   	  
				SET_activeWrite_1 <= SET_activeWrite_0;
				SET_iAddress_1    <= SET_iAddress_0;
				CACHE_writeData_1 <= CACHE_writeData_0;
				CACHE_byteena_a_1 <= CACHE_byteena_a_0;
				CACHE_wordena_a_1 <= CACHE_wordena_a_0;

				SET_iReady_0 <= iReady;
			end if;
		end if;
	end process writeCacheProcess;

	CACHE_wraddress <= (others => '0') when SET_activeWrite_1 = 2**SET_COUNT_WIDTH else 
					SET_oAddress(SET_activeWrite_1) & SET_iAddress_1(SET_COUNT_WIDTH + TILE_WIDTH - INPUT_WIDTH - 1 downto 0);
	CACHE_wren_a    <= '0' when SET_activeWrite_1 = 2**SET_COUNT_WIDTH  else SET_oReady(SET_activeWrite_1);

	cacheUnits : for i in 0 to 2 ** SET_COUNT_WIDTH - 1 generate
		signal SET_iReady, SET_tComplete : std_logic;
		signal SET_iAddress              : std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
	begin
		SET_iReady    <= '1' when (SET_activeWrite_0 = i and SET_iReady_0 = '1') else '0';
		SET_tComplete <= '1' when (SET_activeTransfer = i and Transfer_Complete = '1') else '0';
		SET_iAddress  <= SET_iAddress_0(inAddress'high downto inAddress'high - IDENTIFIER_WIDTH + 1);
		OutputCacheSet_Inst : OutputCacheSet
			generic map(
				IDENTIFIER_WIDTH => IDENTIFIER_WIDTH,
				SET_WIDTH        => SET_WIDTH,
				CLOCK_WIDTH      => CLOCK_WIDTH)
			port map(
				clk       => clk,
				iAddress  => SET_iAddress,
				oAddress  => SET_oAddress(i),
				tIndex    => SET_tIndex(i),
				tAddress  => SET_tAddress(i),
				iReady    => SET_iReady,
				oWaiting  => '1',
				tComplete => SET_tComplete,
				iWaiting  => SET_iWaiting(i),
				oReady    => SET_oReady(i),
				tReady    => SET_tReady(i),
				tClear    => SET_tClear,
				empty => SET_empty(i));
	end generate cacheUnits;

	OutputCache_Inst : OutputCacheMemory
		generic map(BYTE_WIDTH     => BPP / 2,
			        BYTEEN_A_WIDTH => 2 ** INPUT_WIDTH * BYTES_PER_PIXEL,
			        WORD_A_WIDTH   => 2 ** INPUT_WIDTH * BPP,
			        ADDR_A_WIDTH   => SET_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - INPUT_WIDTH,
			        WORD_B_WIDTH   => BURST_WIDTH,
			        ADDR_B_WIDTH   => SET_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH)
		port map(byteena_a => CACHE_byteena_a_1,
			     clock     => clk,
			     data_a    => CACHE_WriteData_1,
			     data_b    => (others => '0'),
			     address_b => Transfer_Unit_Output_Index,
			     address_a => CACHE_wraddress,
			     wren_a    => CACHE_wren_a,
			     wren_b    => CACHE_wren_b,
			     q_b       => Transfer_Unit_Input_Data);
			     
	TileCacheController_Inst : TileCacheController
		generic map(TILE_WIDTH           => TILE_WIDTH,
			        PIXEL_ADDR_WIDTH     => 1,
			        TILES_PER_WORD_WIDTH => TILES_PER_WORD_WIDTH,
			        BURST_WIDTH          => BURST_WIDTH,
			        ADDRESS_WIDTH        => IDENTIFIER_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH,
			        BURST_COUNT_WIDTH    => BURST_COUNT_WIDTH)
		port map(clk            => clk,
			     addr           => Transfer_Unit_Output_Tile_Address,
			     clear          => Transfer_Unit_Input_Block_Cleared,
			     tileCacheEmpty => Tile_Cache_Output_Tile_Cache_Empty,
			     clearTile      => Transfer_Unit_Output_Clear_Block,
			     flushBuffers   => Tile_Cache_Input_Flush_Buffers,
			     clearColour    => Clear_Colour,
			     oData          => Tile_Cache_Output_Data,
			     oAddress       => Tile_Cache_Output_Address,
			     oWaiting       => Tile_Cache_Output_Waiting,
			     oReady         => Tile_Cache_Output_Ready);			     

	SET_activeTransferIndex <= std_logic_vector(to_unsigned(SET_activeTransfer, SET_COUNT_WIDTH));

	TransferUnit_Inst : TransferUnit
		generic map(
			IDENTIFER_WIDTH   => IDENTIFIER_WIDTH,
			SET_WIDTH         => SET_WIDTH,
			SET_COUNT_WIDTH   => SET_COUNT_WIDTH,
			BURST_WIDTH       => BURST_WIDTH,
			BURST_COUNT_WIDTH => BURST_COUNT_WIDTH,
			BPP               => BPP,
			TILE_WIDTH        => TILE_WIDTH)
		port map(
			clk           => clk,
			iReady        => Transfer_Ready,
			tBlockCleared => Transfer_Unit_Input_Block_Cleared,
			oWaiting      => Transfer_Unit_Output_Waiting,
			iData         => Transfer_Unit_Input_data,
			iAddress      => Transfer_Unit_Input_Address,
			iIndex        => Transfer_Unit_Input_Index,
			iSetIndex     => SET_activeTransferIndex,
			oIndex        => Transfer_Unit_Output_Index,
			clearColour   => Clear_Colour,
			oData         => Transfer_Unit_Output_Data,
			oEnabled      => Transfer_Unit_Output_Enabled,
			oAddress      => Transfer_Unit_Output_Address,
			oReady        => Transfer_Unit_Output_Ready,
			iWaiting      => Transfer_Unit_Input_Waiting,
			tComplete     => Transfer_Complete,
			tClearBlock => Transfer_Unit_Output_Clear_Block,
			oTileAddress => Transfer_Unit_Output_Tile_Address,
			cacheClear    => CACHE_wren_b);
			
	oEnabled <= Transfer_Unit_Output_Enabled when Tile_Cache_Input_Flush_Buffers = '0'
		else (others => '1');
		
	oReady <= Transfer_Unit_Output_Ready when Tile_Cache_Input_Flush_Buffers = '0'
		else Tile_Cache_Output_Ready;
	
	oData <= Transfer_Unit_Output_Data when Tile_Cache_Input_Flush_Buffers = '0'
		else Tile_Cache_Output_Data;
		
	oAddress <= Transfer_Unit_Output_Address when Tile_Cache_Input_Flush_Buffers = '0'
		else Tile_Cache_Output_Address; 	 	
		
	Tile_Cache_Output_Waiting <= Tile_Cache_Input_Flush_Buffers and oWaiting;
	Transfer_Unit_Output_Waiting <= not Tile_Cache_Input_Flush_Buffers and oWaiting;	

end OutputCache;
