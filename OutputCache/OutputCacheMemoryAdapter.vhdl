library ieee;
use ieee.std_logic_1164.all;

use work.GpuTypes.all;

entity OutputCacheMemoryAdapter is
	port (
		clk : in std_logic;
		inData : in std_logic_vector(127 downto 0);
		outData : out std_logic_vector(31 downto 0);
		inAddress : in Address;
		outAddress : out Address;
		inEnabled : in std_logic_vector(7 downto 0);
		outEnabled : out std_logic_vector(1 downto 0);
		
		memoryWriteFull, iReady : in std_logic;
		memoryWrite, iWaiting : buffer std_logic  := '0'		
	);
end entity OutputCacheMemoryAdapter;

architecture RTL of OutputCacheMemoryAdapter is
	
begin
	mainProcess : process (clk) is
		variable internalDataIndex : integer range 0 to 3;
		variable internalData : std_logic_vector(127 downto 0) := (others => '0');
		variable internalEnabled : std_logic_vector(7 downto 0) := (others => '0');
		variable ready : boolean := false;
	begin
		if rising_edge(clk) then
			if iWaiting = '1' and iReady = '1' then
				iWaiting <= '0';
			end if;
		
			if not ready then
				iWaiting <= '1';
				
				if iReady = '1' then
					internalData := inData;
					internalEnabled := inEnabled;
					outAddress <= inAddress;
					ready := true;
				end if;
			end if;	
			
			if ready then
				if memoryWriteFull = '0' then
					outData <= internalData(32 *(internalDataIndex+1)-1 downto 32*internalDataIndex);
					outEnabled <= internalEnabled(2*(internalDataIndex+1)-1 downto 2*internalDataIndex);
					memoryWrite <= '1';
					
					if internalDataIndex = 3 then
						ready := false;
						internalDataIndex := 0;
					else
						internalDataIndex := internalDataIndex + 1;
					end if;
				else
					memoryWrite <= '0';
				end if;
			else
				memoryWrite <= '0';
			end if;	
			
			
		end if;
	end process mainProcess;
	
end architecture RTL;
