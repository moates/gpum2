library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity TileCacheController is
	generic(
		TILE_WIDTH           : natural := 6;
		PIXEL_ADDR_WIDTH     : natural := 1;
		TILES_PER_WORD_WIDTH : natural := 4;
		BURST_WIDTH          : natural := 128;
		ADDRESS_WIDTH        : natural := 18;
		BURST_COUNT_WIDTH    : natural := 3
	);
	port(
		clk                     : in  std_logic;
		addr                    : in  std_logic_vector(ADDRESS_WIDTH - 1 downto 0);
		clear, tileCacheEmpty   : buffer std_logic                                    := '0';
		clearTile, flushBuffers : in  std_logic;
		clearColour             : in  std_logic_vector(BPP - 1 downto 0);
		oData                   : out std_logic_vector(BURST_WIDTH - 1 downto 0);
		oAddress                : buffer std_logic_vector(ADDRESS_WIDTH - 1 downto 0) := (others => '0');
		oWaiting                : in  std_logic;
		oReady                  : buffer std_logic                                    := '0'
	);
end entity TileCacheController;

architecture RTL of TileCacheController is
	constant ADDR_LOWER_BOUND : natural := ADDRESS_WIDTH - TILE_WIDTH - TILES_PER_WORD_WIDTH + PIXEL_ADDR_WIDTH;
	constant BLOCKS           : natural := 300;

	component TileCache
		port(clock     : IN  STD_LOGIC := '1';
			 data      : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
			 rdaddress : IN  STD_LOGIC_VECTOR(8 DOWNTO 0);
			 wraddress : IN  STD_LOGIC_VECTOR(8 DOWNTO 0);
			 wren      : IN  STD_LOGIC := '0';
			 q         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
	end component TileCache;

	signal tileData, clearData, bitMask          : std_logic_vector(2 ** TILES_PER_WORD_WIDTH - 1 downto 0);
	signal activeTile                            : integer range 0 to 2 ** TILES_PER_WORD_WIDTH - 1;
	signal tileReadAddress, tileClearReadAddress : STD_LOGIC_VECTOR(8 DOWNTO 0);
	signal tileWriteAddress                      : STD_LOGIC_VECTOR(8 DOWNTO 0);
	signal tileClear                             : STD_LOGIC;
	signal clearingBlockAddress                  : STD_LOGIC_VECTOR(8 DOWNTO 0);
	signal clearingTileAddress                   : STD_LOGIC_VECTOR(TILES_PER_WORD_WIDTH - 1 downto 0);
	signal clearingBurstAddress                  : STD_LOGIC_VECTOR(TILE_WIDTH - BURST_COUNT_WIDTH - 1 downto 0);
	signal tileClearWriteEnable                  : std_logic;
	signal tileClearWriteAddress                 : STD_LOGIC_VECTOR(8 DOWNTO 0);

begin
	expandClearData : for i in 0 to BURST_WIDTH / BPP - 1 generate
		oData((i + 1) * BPP - 1 downto i * BPP) <= clearColour;
	end generate expandClearData;

	oAddress <= clearingBlockAddress & clearingTileAddress & clearingBurstAddress;

	decoderProcess : process(activeTile)
	begin
		bitMask             <= (others => '0');
		bitMask(activeTile) <= '1';
	end process decoderProcess;

	clearTilesProcess : process(clk) is
		variable clearing, foundBlock, clearingBlock, finished, finishing : boolean                                                            := false;
		variable blockIndex                                    : integer range 0 to 2 ** TILES_PER_WORD_WIDTH - 1;
		variable currentTileList                               : std_logic_vector(2 ** TILES_PER_WORD_WIDTH - 1 downto 0);
		variable currentTileAddress                            : STD_LOGIC_VECTOR(8 DOWNTO 0);
		variable currentClearingBurstAddress                   : integer range (2 ** (TILE_WIDTH - BURST_COUNT_WIDTH)) - 1 downto 0 := 0;
		variable startingCountdown                             : integer range 0 to 2                                               := 0;
		constant BURST_MAX                                     : natural                                                            := 2 ** (TILE_WIDTH - BURST_COUNT_WIDTH) - 1;
		variable readCountDown : integer range 0 to 3;
	begin
		if rising_edge(clk) then
			if tileClearWriteEnable = '1' then
				tileClearWriteEnable <= '0';
			end if;

			if flushBuffers = '0' then
				finished := false;
				tileCacheEmpty <= '0';
			end if;

			if not finished then
				if flushBuffers = '0' and clearing then
					clearing := false;
				elsif flushBuffers = '1' and not clearing then
					clearing             := true;
					startingCountdown    := 2;
					currentTileList      := (others => '1');
					foundBlock           := false;
					tileClearReadAddress <= (others => '0');
				elsif flushBuffers = '1' and startingCountdown > 0 then
					-- Wait until tile info makes its way to the out port
					startingCountdown := startingCountDown - 1;
				elsif flushBuffers = '1' and tileCacheEmpty = '0' then

					if readCountDown > 0 then
						readCountDown := readCountDown - 1;
					end if;

					-- First, find free blocks
					if not foundBlock then
						--Search for a block						
						if currentTileList /= (2 ** TILES_PER_WORD_WIDTH - 1 downto 0 => '1') then
							--One of the current blocks is filled
							if currentTileList(blockIndex) = '0' then
								currentTileList(blockIndex) := '1';
								foundBlock                  := true;
							else
								blockIndex := blockIndex + 1;
							end if;																				
						elsif readCountDown = 0 then
							if to_integer(unsigned(tileClearReadAddress)) = BLOCKS then
								tileClearWriteAddress <= tileClearReadAddress;
								tileClearWriteEnable  <= '1';
								blockIndex            := 0;
								finishing              := true;
							else
								currentTileList       := tileData;
								currentTileAddress    := tileClearReadAddress;
								tileClearReadAddress  <= std_logic_vector(unsigned(tileClearReadAddress) + 1);
								tileClearWriteAddress <= tileClearReadAddress;
								tileClearWriteEnable  <= '1';
								blockIndex            := 0;
								readCountDown := 3;
							end if;
						end if;
					end if;

					-- Second, clear free blocks
					if clearingBlock then
						if oWaiting = '1' then
							if currentClearingBurstAddress = BURST_MAX then
								if finishing then
									tileCacheEmpty <= '1';
									clearing       := false;
									finished := true;
									finishing := false;
								else
									oReady        <= '0';
									clearingBlock := false;
								end if;
							else
								currentClearingBurstAddress := currentClearingBurstAddress + 1;
							end if;
						end if;
					elsif foundBlock then
						foundBlock                  := false;
						clearingBlock               := true;
						clearingBlockAddress        <= currentTileAddress;
						clearingTileAddress         <= std_logic_vector(to_unsigned(blockIndex, TILES_PER_WORD_WIDTH));
						currentClearingBurstAddress := 0;
						oReady                      <= '1';
					elsif finishing then
						tileCacheEmpty <= '1';
						clearing       := false;
						finished := true;
						finishing := false;
					end if;					
				end if;
			end if;

			clearingBurstAddress <= std_logic_vector(to_unsigned(currentClearingBurstAddress, TILE_WIDTH - BURST_COUNT_WIDTH));
		end if;
	end process clearTilesProcess;

	-- Switch inputs/outputs depending on whether we are clearing
	tileReadAddress <= addr(addr'high downto ADDR_LOWER_BOUND) when flushBuffers = '0'
		else tileClearReadAddress;

	tileWriteAddress <= addr(addr'high downto ADDR_LOWER_BOUND) when flushBuffers = '0'
		else tileClearWriteAddress;

	tileClear <= clearTile when flushBuffers = '0'
		else tileClearWriteEnable;

	clearData <= tileData or bitMask when flushBuffers = '0'
		else (others => '0');

	TileCache_Inst : component TileCache
		port map(clock     => clk,
			     data      => clearData,
			     rdaddress => tileReadAddress,
			     wraddress => tileWriteAddress,
			     wren      => tileClear,
			     q         => tileData);

	activeTile <= to_integer(unsigned(addr(ADDR_LOWER_BOUND - 1 downto ADDR_LOWER_BOUND - TILES_PER_WORD_WIDTH)));

	clear <= tileData(activeTile);

end architecture RTL;
