library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity OutputCacheSet is
	generic(
		IDENTIFIER_WIDTH : natural := 6;
		SET_WIDTH        : natural := 3;
		CLOCK_WIDTH      : natural := 2
	);

	port(
		clk                                 : in  std_logic;
		iAddress                            : in  std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
		oAddress, tIndex                    : out std_logic_vector(SET_WIDTH - 1 downto 0);
		tAddress                            : out std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
		iReady, oWaiting, tComplete, tClear : in  std_logic;
		iWaiting, oReady, tReady            : out std_logic;
		empty                               : out std_logic
	);
end OutputCacheSet;

architecture OutputCacheSet_RTL of OutputCacheSet is
	type Page is record
		used  : std_logic;
		clock : integer range 2 ** CLOCK_WIDTH - 1 downto 0;
		addr  : std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
	end record;

	constant SET_SIZE : natural := 2 ** SET_WIDTH;

	type Set is array (SET_SIZE - 1 downto 0) of Page;
	type state is (Idle, Waiting);
	signal CacheSet                   : Set       := (others => (used => '0', clock => 0, addr => (others => '0')));
	signal Clock                      : integer range 0 to SET_SIZE - 1;
	signal transferReady, outputReady : std_logic := '0';
	signal inputWaiting               : std_logic := '1';

begin                                   -- OutputCacheSet_RTL

	tReady   <= transferReady;
	iWaiting <= inputWaiting;
	oReady   <= outputReady;

	emptyCheck : process(CacheSet)
		variable cacheEmpty : std_logic := '1';
	begin                               -- process emptyCheck
		cacheEmpty := '1';
		for i in SET_SIZE - 1 downto 0 loop
			cacheEmpty := cacheEmpty and not CacheSet(i).used;
		end loop;                       -- i

		empty <= cacheEmpty;
	end process emptyCheck;

	process(clk)
		variable index        : integer range 0 to SET_SIZE - 1;
		variable found, clear : boolean := false;
		variable ready        : boolean := true;
		variable used         : integer range 0 to SET_SIZE;
		variable inputAddress : std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
		variable currentState : state   := Idle;
	begin                               -- process      
		if clk'event and clk = '1' then -- rising clock edge
			if outputReady = '1' and oWaiting = '1' then
				outputReady <= '0';
				ready       := true;
			end if;

			if transferReady = '1' and tComplete = '1' then
				transferReady        <= '0';
				CacheSet(clock).used <= '0';
				used                 := used - 1;
			end if;

			case currentState is
				when Idle =>
					if iReady = '1' then
						inputAddress := iAddress;

						-- Find

						found := false;
						index := 0;

						for i in 0 to SET_SIZE - 1 loop
							if CacheSet(i).addr = inputAddress and CacheSet(i).used = '1' then
								index := i;
								found := true;

								if CacheSet(i).clock /= 2 ** CLOCK_WIDTH - 1 then
									CacheSet(i).clock <= CacheSet(i).clock + 1;
								end if;
								exit;
							end if;
						end loop;       -- i

						if not found then
							for i in 0 to SET_SIZE - 1 loop
								if CacheSet(i).used = '0' then
									CacheSet(i) <= (used => '1', clock => 1, addr => inputAddress);
									found       := true;
									index       := i;
									
									if used /= SET_SIZE then
										used := used + 1;
									end if;

									if used = SET_SIZE - 1 or used = SET_SIZE then
										clear := true;
									end if;								
									exit;
								end if;
							end loop;   -- i
						end if;
						
						if found then
							inputWaiting <= '1';
							outputReady  <= '1';
		
							oAddress <= std_logic_vector(to_unsigned(index, SET_WIDTH));
						else
							inputWaiting <= '0';
							outputReady  <= '0';
							
							currentState := Waiting;
						end if;
					end if;
				when Waiting =>
					found := false;
					 
					for i in 0 to SET_SIZE - 1 loop
								if CacheSet(i).used = '0' then
									CacheSet(i) <= (used => '1', clock => 1, addr => inputAddress);
									found       := true;
									index       := i;

									if used /= SET_SIZE then
										used := used + 1;
									end if;

									if used = SET_SIZE - 1 or used = SET_SIZE  then
										clear := true;
									end if;
									
									exit;
								end if;
							end loop;   -- i						
						
					if found then
						inputWaiting <= '1';
						outputReady  <= '1';
	
						oAddress <= std_logic_vector(to_unsigned(index, SET_WIDTH));
						
						currentState := Idle;						
					end if;
			end case;

			if CacheSet(clock).clock /= 0 or CacheSet(clock).used = '0' then
				if tClear = '1' then
					CacheSet(clock).clock <= 0;
				elsif CacheSet(clock).clock /= 0 then
					CacheSet(clock).clock <= CacheSet(clock).clock - 1;
				end if;

				if clock = SET_SIZE - 1 then
					clock <= 0;
				else
					clock <= clock + 1;
				end if;
			end if;

			if (clear or tClear = '1') and CacheSet(clock).clock = 0 and CacheSet(clock).used = '1' and transferReady = '0' then
				clear         := false;
				transferReady <= '1';
				tIndex        <= std_logic_vector(to_unsigned(clock, SET_WIDTH));
				tAddress      <= CacheSet(clock).addr;
			end if;

		end if;
	end process;

end OutputCacheSet_RTL;
