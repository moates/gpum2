library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TransferUnit is
	generic(
		IDENTIFER_WIDTH   : natural := 7;
		SET_WIDTH         : natural := 2;
		SET_COUNT_WIDTH   : natural := 1;
		BURST_WIDTH       : natural := 128;
		BURST_COUNT_WIDTH : natural := 3;
		BPP               : natural := 16;
		TILE_WIDTH        : natural := 6
	);

	port(
		clk                                     : in  std_logic;
		iReady, tBlockCleared   			    : in  std_logic;
		oWaiting                                : in  std_logic;
		iData                                   : in  std_logic_vector(BURST_WIDTH - 1 downto 0);
		iAddress                                : in  std_logic_vector(IDENTIFER_WIDTH - 1 downto 0);
		iIndex                                  : in  std_logic_vector(SET_WIDTH - 1 downto 0);
		iSetIndex                               : in  std_logic_vector(SET_COUNT_WIDTH - 1 downto 0);
		oIndex                                  : out std_logic_vector(SET_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH - 1 downto 0);
		clearColour                             : in  std_logic_vector(BPP - 1 downto 0);
		oData                                   : out std_logic_vector(BURST_WIDTH - 1 downto 0);
		oEnabled                                : out std_logic_vector(2 ** BURST_COUNT_WIDTH - 1 downto 0);
		oAddress, oTileAddress                    : buffer std_logic_vector(IDENTIFER_WIDTH + SET_COUNT_WIDTH + TILE_WIDTH - BURST_COUNT_WIDTH - 1 downto 0) := (others => '0');
		oReady, iWaiting, cacheClear, tComplete, tClearBlock : buffer std_logic := '0'
	);
end TransferUnit;

architecture TransferUnit_RTL of TransferUnit is
	type state is (Idle, Transfering_0, Transfering_1, Transfering_2, Transfering_3, Transfering_4);
begin                                   -- TransferUnit_RTL

	transferStatemachine : process(clk)
		variable burstIndex   : unsigned(2 downto 0)                                  := (others => '0');
		variable currentState : state                                                 := Idle;
		variable output       : std_logic;
		variable colourOutput : std_logic_vector(BURST_WIDTH - 1 downto 0);
		variable enabled      : std_logic_vector(2 ** BURST_COUNT_WIDTH - 1 downto 0) := (others => '0');
		variable blockCleared : std_logic;
		variable currentAddress : std_logic_vector(IDENTIFER_WIDTH-1 downto 0);
		variable currentSetIndex : std_logic_vector(SET_COUNT_WIDTH-1 downto 0);
		variable currentIndex : std_logic_vector(SET_WIDTH-1 downto 0);
	begin                               -- process transferStatemachine     
		if clk'event and clk = '1' then -- rising clock edge
			if iReady = '1' and iWaiting = '1' then
				iWaiting <= '0';
			end if;

			if oReady = '1' and oWaiting = '1' then
				oReady <= '0';
			end if;			

			case currentState is
				when Idle =>
					tClearBlock <= '0';
					
					if tComplete = '1' then
						tComplete <= '0';
					elsif iReady = '1' then
						burstIndex   := (others => '0');
						currentState := Transfering_0;
						currentAddress := iAddress;
						currentSetIndex := iSetIndex;
						currentIndex := iIndex;
					else
						iWaiting <= '1';
					end if;
				when Transfering_0 =>
					oIndex       <= currentIndex & currentSetIndex & std_logic_vector(burstIndex);	
					oTileAddress   <= currentAddress & currentSetIndex & std_logic_vector(burstIndex);				
					currentState := Transfering_1;
				when Transfering_1 =>
					currentState := Transfering_2;
				when Transfering_2 =>
					currentState := Transfering_3;
				when Transfering_3 =>
					colourOutput := iData;

					for i in 0 to 2 ** BURST_COUNT_WIDTH - 1 loop
						enabled(i) := iData((i + 1) * 16 - 1);
					end loop;

					cacheClear   <= '1';
					blockCleared := tBlockCleared;
					currentState := Transfering_4;
				when Transfering_4 =>
					output     := '1';
					cacheClear <= '0';
					
					if blockCleared = '0' then
						--output := '1';

						for i in 0 to 2 ** BURST_COUNT_WIDTH - 1 loop
							if enabled(i) = '0' then
								colourOutput((i + 1) * BPP - 1 downto i * BPP) := clearColour;
							end if;
							enabled(i) := '1';
						end loop;       -- i
					else
						for i in 0 to 2 ** BURST_COUNT_WIDTH - 1 loop
							if enabled(i) = '1' then
								output := '1';
							end if;
						end loop;       -- i
					end if;

					if output = '1' then						
							oData    <= colourOutput;
							oEnabled <= enabled;	
							oAddress <= currentAddress & currentSetIndex & std_logic_vector(burstIndex);						
							oReady   <= '1';

						if oWaiting = '1' then
							if burstIndex = 7 then
								currentState := Idle;
								iWaiting     <= '1';
								tComplete    <= '1';
								tClearBlock  <= '1';
							else
								burstIndex   := burstIndex + 1;
								currentState := Transfering_0;
							end if;
						end if;
					else
						if burstIndex = 7 then
							currentState := Idle;
							iWaiting     <= '1';
							tComplete    <= '1';
							tClearBlock  <= '1';
						else
							burstIndex   := burstIndex + 1;
							currentState := Transfering_0;
						end if;
					end if;
			end case;
		end if;
	end process transferStatemachine;

end TransferUnit_RTL;
