library ieee;
use ieee.std_logic_1164.all;

entity AvalonMMInterface is
	port (
		clk, reset : in std_logic;
		address : in std_logic_vector(1 downto 0);
		read, write : in std_logic;
		waitrequest, readdatavalid : out std_logic;
		readdata : out std_logic_vector(31 downto 0);
		writedata : in std_logic_vector(31 downto 0);
		gpu_data : out std_logic_vector(31 downto 0);
		gpu_write : out std_logic;
		gpu_fifo_full : in std_logic;
		gpu_size : std_logic_vector(31 downto 0)
		
	);
end entity AvalonMMInterface;

architecture RTL of AvalonMMInterface is
	
begin
	waitrequest <= gpu_fifo_full;
	gpu_data <= writedata;
	readdata <= gpu_size;

	primaryProcess : process (clk) is
	begin
		if rising_edge(clk) then
				if read = '1' then
					readdatavalid <= '1';
				elsif write = '1' and gpu_fifo_full = '0' then
					gpu_write <= '1';
				else
					gpu_write <= '0';
					readdatavalid <= '0';
				end if;
		end if;
	end process primaryProcess;
		
end architecture RTL;
