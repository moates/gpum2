library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity OutputCacheMemoryAdapterTB_tb is
end entity OutputCacheMemoryAdapterTB_tb;

architecture RTL of OutputCacheMemoryAdapterTB_tb is
signal clk, res : std_logic := '0';
component OutputCacheMemoryAdapter
	port(clk                     : in  std_logic;
		 rst                     : in  std_logic;
		 inData                  : in  std_logic_vector(127 downto 0);
		 outData                 : out std_logic_vector(31 downto 0);
		 inAddress               : in  Address;
		 outAddress              : out Address;
		 memoryWriteFull, iReady : in  std_logic;
		 memoryWrite, iWaiting   : buffer std_logic := '0');
end component OutputCacheMemoryAdapter;
signal inData : std_logic_vector(127 downto 0);
signal outData : std_logic_vector(31 downto 0);
signal inAddress : Address;
signal outAddress : Address;
signal memoryWriteFull : std_logic;
signal iReady : std_logic;
signal memoryWrite : std_logic;
signal iWaiting : std_logic;
begin

u0 : OutputCacheMemoryAdapter
	port map(clk             => clk,
		     rst             => res,
		     inData          => inData,
		     outData         => outData,
		     inAddress       => inAddress,
		     outAddress      => outAddress,
		     memoryWriteFull => memoryWriteFull,
		     iReady          => iReady,
		     memoryWrite     => memoryWrite,
		     iWaiting        => iWaiting);

clock: process
begin
	clk <= '0';
	
	wait for 50 ps;
	
	clk <= '1';
	
	wait for 50 ps;
end process clock;

stimuli: process
	variable inputData : std_logic_vector(127 downto 0);
begin
	memoryWriteFull <= '1';
	
	inputData := std_logic_vector(to_unsigned(4,32)) & std_logic_vector(to_unsigned(3,32)) & std_logic_vector(to_unsigned(2,32)) & std_logic_vector(to_unsigned(1,32));
	inData <= inputData;
	inAddress <= (others => '0');
	
	iReady <= '0';
	
	wait for 100 ps;
	
	assert iWaiting = '1' report "Unit should be waiting to recieve data by default" severity error;
	
	iReady <= '1';	
	
	wait for 100 ps;
	assert iWaiting = '0' report "Once unit has accepted data it should no longer accept any more" severity error;
	assert memoryWrite = '0' report "The unit should not write data until there is space in the output queue" severity error;
	inData <= (others => '0');
	memoryWriteFull <= '0';
	iReady <= '0';
	
	wait for 100 ps;
	assert outData = inputData(31 downto 0) report "The first word should be output in the next available cycle";
	assert memoryWrite = '1' report "The unit should start to write to memory" severity error;
	
	wait for 100 ps;
	assert outData = inputData(63 downto 32) report "The second word should be output in the next available cycle";
	
	wait for 100 ps;
	assert outData = inputData(95 downto 64) report "The third word should be output in the next available cycle";
	
	wait for 100 ps;
	assert outData = inputData(127 downto 96) report "The fourth word should be output in the next available cycle";
	assert iWaiting = '1' report "The unit should begin waiting for data";
wait;
end process stimuli;

end architecture RTL;