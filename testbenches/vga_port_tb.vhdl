library ieee;
use ieee.std_logic_1164.all;

entity vga_port_tb is
  
end vga_port_tb;

architecture vga_port_tb_rtl of vga_port_tb is

  component vga
    generic (
      groupSize    : integer;
      bpp          : integer;
      screenWidth  : integer;
      screenHeight : integer);
    port (
      vga_address               : out std_logic_vector(screenWidth+screenHeight-groupSize-1 downto 0);
      data                  : in  std_logic_vector(bpp*groupSize**2-1 downto 0);
      pixel                 : out std_logic_vector(bpp-1 downto 0);
      clk, res, ready       : in  std_logic;
      waiting, vsync, hsync : out std_logic);
  end component;

  constant screenWidth : integer := 10;
  constant screenHeight : integer := 9;
  constant groupSize : integer := 3;
  constant bpp : integer := 16;
  
  signal address : std_logic_vector(16 downto 0);
  signal data : std_logic_vector(127 downto 0) := (0 => '1', 17 => '1', 32 => '1', 33 => '1', 50 => '1', OTHERS => '0');
  signal clk, res, ready, waiting, vsync, hsync : std_logic := '0';
  signal vout : std_logic_vector(bpp-1 downto 0);
  
begin  -- vga_port_tb_rtl

  -- instance "vga_1"
  vga_1: vga
    generic map (
      groupSize    => groupSize,
      bpp          => bpp,
      screenWidth  => screenWidth,
      screenHeight => screenHeight)
    port map (
      vga_address => address,
      data    => data,
      clk     => clk,
      res     => res,
      ready   => ready,
      waiting => waiting,
      vsync   => vsync,
      hsync   => hsync,
      pixel => vout);

  clk_process: process
  begin  -- process clk_process
    clk <= '1';
    ready <= '1';

    wait for 10 ps;

    res <= '1';
    clk <= '0';

    wait for 10 ps;
  end process clk_process;

end vga_port_tb_rtl;
