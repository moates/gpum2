library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;
use work.ZBufferTypes.all;

entity ZBufferTestbench is
end entity ZBufferTestbench;

architecture RTL of ZBufferTestbench is
	component ZTestUnit_tb
		generic(ZBufferLatency : natural := 2);
		port(clk, clken            : in  std_logic;
			 iReady, oWaiting      : in  std_logic;
			 oReady, zRequest      : buffer std_logic := '0';
			 empty                 : out std_logic    := '1';
			 pixelData             : in  ZBufferCheckType;
			 outData               : out ZBufferCheckType;
			 inZBufferFetchAddress : out Address;
			 inZBufferData         : in  ZBufferDataArray(PixelChannels - 1 downto 0);
			 outZBufferSaveAddress : out Address;
			 outZBufferData        : out ZBufferDataArray(PixelChannels - 1 downto 0);
			 comparisonType        : in  std_logic_vector(1 downto 0));
	end component ZTestUnit_tb;

	signal iReady, oReady, oWaiting, ZRequest           : std_logic := '0';
	signal pixelData, outData                           : ZBufferCheckType;
	signal inzBufferFetchAddress, outzBufferSaveAddress : Address;
	signal clk, clken                                   : std_logic;
	signal empty                                        : std_logic;
	signal inzBufferData, outzBufferData                : ZBufferDataArray(PixelChannels - 1 downto 0);
	signal comparisonType                               : std_logic_vector(1 downto 0);

	constant ZBufferLatency : natural := 1;
begin
	instance : component ZTestUnit_tb 
	generic map (ZBufferLatency => ZBufferLatency) 
	port map (clk => clk,
		clken => clken,
		iReady => iReady,
		oWaiting => oWaiting,
		oReady => oReady,
		zRequest => zRequest,
		empty => empty,
		pixelData => pixelData,
		outData => outData,
		inZBufferFetchAddress => inZBufferFetchAddress,
		inZBufferData => inZBufferData,
		outZBufferSaveAddress => outZBufferSaveAddress,
		outZBufferData => outZBufferData,
		comparisonType => comparisonType);

	clockprocess : process is
	begin
		clk <= '1';

		wait for 50 ps;

		clk <= '0';

		wait for 50 ps;
	end process clockprocess;

	stimulusProcess : process is
	begin
		assert zRequest = '0' report "Z Request flag should be low when no data is required" severity error;
		assert oReady = '0' report "Z buffer should not have any data ready initially" severity error;
		assert empty = '1' report "Z buffer should be empty initially" severity error;

		iReady                 <= '1';
		clken                  <= '1';
		comparisonType  	   <= "00";
		pixelData.PixelAddress <= (1 => '1', others => '0');
		pixelData.Enabled      <= (others => '1');
		pixelData.Coords(0)    <= (B0 => x"FF", others => (others => '0'));
		pixelData.Coords(1)    <= (B0 => x"FF", others => (others => '0'));

		pixelData.Attributes.z0 <= (others => '0');
		pixelData.Attributes.z1 <= (others => '0');
		pixelData.Attributes.z2 <= (others => '0');

		inzBufferData <= (others => x"FFFF");

		wait for 300 ps;

		iReady <= '0';

		wait for 100 ps;

		assert zRequest = '1' report "Z Request flag should go high when requesting data" severity error;

		iReady <= '0';
		clken  <= '0';

		wait for 100 ps;

		assert zRequest = '1' report "Pipeline should pause while clken is low." severity error;
		clken <= '1';

		wait for 100 ps;

		assert zRequest = '0' report "Z Request flag should go high when requesting data" severity error;

		wait;
	end process stimulusProcess;

end architecture RTL;
