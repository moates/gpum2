library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity Rasterizer_tb is
  
end Rasterizer_tb;

architecture Rasterizer_tb_RTL of Rasterizer_tb is
  component Rasterizer
    generic (
      scanWidth  : natural;
      scanHeight : natural);
    port (
      clk, res         : in     std_logic;
      triangle         : in     TriangleType;
      triangleAttrs    : in     TriangleAttributes;
      pixelData        : out    ColourVectorArray(scanWidth*scanHeight-1 downto 0);
      pixelAddress     : out    Address;
      iReady, oWaiting : in     std_logic;
      iWaiting, oReady : buffer std_logic);
  end component;

  signal clk, res         : std_logic := '0';
  signal triangle         : TriangleType;
  signal triangleAttrs    : TriangleAttributes;
  signal pixelData        : ColourVectorArray(1 downto 0);
  signal pixelAddress     : Address;
  signal iReady, oWaiting : std_logic;
  signal iWaiting, oReady : std_logic;
begin  -- Rasterizer_tb_RTL

  Rasterizer_1: Rasterizer
    generic map (
      scanWidth  => 2,
      scanHeight => 1)
    port map (
      clk           => clk,
      res           => res,
      triangle      => triangle,
      triangleAttrs => triangleAttrs,
      pixelData     => pixelData,
      pixelAddress  => pixelAddress,
      iReady        => iReady,
      oWaiting      => oWaiting,
      iWaiting      => iWaiting,
      oReady        => oReady);

  process
  begin  -- process
    clk <= '0';

    wait for 10 ps;

    clk <= '1';
    res <= '1';
    wait for 10 ps;
  end process;

  process
  begin  -- process
    triangle <= (a0 => to_signed(-100,32), b0 => to_signed(-100,32), g0 => to_signed(41000,32),
                 a1 => to_signed(250,32), b1 => to_signed(-360,32), g1 => to_signed(0,32),
                 a2 => to_signed(-150,32), b2 => to_signed(460,32), g2 => to_signed(31000,32));

    triangleAttrs <= (r0 => to_unsigned(31,5), g1 => to_unsigned(31,5), b2 => to_unsigned(31,5), others => (others => '0'));
    
    oWaiting <= '1';
    iReady <= '1';
    wait;
  end process;

  

end Rasterizer_tb_RTL;
