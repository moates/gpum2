library ieee;
use ieee.std_logic_1164.all;
use work.GpuTypes.all;

entity vga_tb is
end entity vga_tb;

architecture RTL of vga_tb is
	component vga
		generic(groupSize    : integer := 2;
			    bpp          : integer := 16;
			    screenWidth  : integer := 10;
			    screenHeight : integer := 9);
		port(vga_address                        : buffer Address;
			 data                               : in  std_logic_vector(31 downto 0);
			 memory_read, memory_request        : out std_logic;
			 memory_read_empty                  : in  std_logic;
			 pixel                              : out std_logic_vector(bpp - 1 downto 0);
			 clk, res                           : in  std_logic;
			 vsync, hsync, vga_clk, blank, sync : out std_logic);
	end component vga;
	
	component SSRAM
		port(memory_clk, fpga_clk, vga_clk                               : in    std_logic;
			 ssram_data                                                  : inout std_logic_vector(31 downto 0);
			 ssram_address                                               : out   std_logic_vector(18 downto 0);
			 ssram_ce1_n, ssram_ce2, ssram_ce3_n                         : out   std_logic;
			 ssram_gwe, ssram_bwe                                        : out   std_logic;
			 ssram_adsp, ssram_adsc                                      : out   std_logic;
			 ssram_bw                                                    : out   std_logic_vector(3 downto 0);
			 ssram_adv, ssram_oe                                         : out   std_logic;
			 vga_readAddress                                             : in    Address;
			 vga_readData                                                : out   std_logic_vector(31 downto 0);
			 outputCache_writeData                                       : in    std_logic_vector(31 downto 0);
			 zBuffer_readAddress                                         : in    Address;
			 zBuffer_readData                                            : out   std_logic_vector(31 downto 0);
			 zBuffer_writeData                                           : in    std_logic_vector(31 downto 0);
			 vga_read, vga_request, outputCache_write                    : in    std_logic;
			 zBuffer_read, zBuffer_request, zBuffer_write                : in    std_logic;
			 outputCache_be                                              : in    std_logic_vector(1 downto 0);
			 outputCache_writeAddress                                    : in    Address;
			 vga_readEmpty                                               : out   std_logic;
			 zBuffer_be                                                  : in    std_logic_vector(1 downto 0);
			 zBuffer_writeAddress                                        : in    Address;
			 zBuffer_readEmpty, zBuffer_writeFull, outputCache_writeFull : out   std_logic;
			 activeBuffer                                                : in    std_logic);
	end component SSRAM;

signal clk, res : std_logic := '0';
signal ssram_adsc : std_logic;
signal ssram_address : std_logic_vector(18 downto 0);
signal ssram_data : std_logic_vector(31 downto 0);
signal pixel : std_logic_vector(bpp - 1 downto 0);
signal vga_readAddress : Address;
signal vga_readData : std_logic_vector(31 downto 0);
signal vga_read : std_logic;
signal vga_request : std_logic;
begin

ssram_0 : SSRAM
	port map(memory_clk               => clk,
		     fpga_clk                 => clk,
		     vga_clk                  => clk,
		     ssram_data               => ssram_data,
		     ssram_address            => ssram_address,
		     ssram_ce1_n              => open,
		     ssram_ce2                => open,
		     ssram_ce3_n              => open,
		     ssram_gwe                => open,
		     ssram_bwe                => open,
		     ssram_adsp               => open,
		     ssram_adsc               => ssram_adsc,
		     ssram_bw                 => open,
		     ssram_adv                => open,
		     ssram_oe                 => open,
		     vga_readAddress          => vga_readAddress,
		     vga_readData             => vga_readData,
		     outputCache_writeData    => (others => '0'),
		     zBuffer_readAddress      => (others => '0'),
		     zBuffer_readData         => open,
		     zBuffer_writeData        => (others => '0'),
		     vga_read                 => vga_read,
		     vga_request              => vga_request,
		     outputCache_write        => '0',
		     zBuffer_read             => '0',
		     zBuffer_request          => '0',
		     zBuffer_write            => '0',
		     outputCache_be           => (others => '0'),
		     outputCache_writeAddress => (others => '0'),
		     vga_readEmpty            => open,
		     zBuffer_be               => (others => '0'),
		     zBuffer_writeAddress     => (others => '0'),
		     zBuffer_readEmpty        => open,
		     zBuffer_writeFull        => open,
		     outputCache_writeFull    => open,
		     activeBuffer             => '1');
		     
	vga_0: component vga
		generic map(groupSize    => 2,
			        bpp          => 16,
			        screenWidth  => 9,
			        screenHeight => 9)
		port map(vga_address       => vga_readAddress,
			     data              => vga_readData,
			     memory_read       => vga_read,
			     memory_request    => vga_request,
			     memory_read_empty => '0',
			     pixel             => pixel,
			     clk               => clk,
			     res               => res,
			     vsync             => open,
			     hsync             => open,
			     vga_clk           => open,
			     blank             => open,
			     sync              => open);		     

clock: process
begin
	clk <= '0';
	
	wait for 50 ps;
	
	clk <= '1';
	res <= '1';
	wait for 50 ps;
end process clock;

stimuli: process(clk)
	type requestPipeline is array(0 to 3) of std_logic_vector(18 downto 0);
	variable pipeline : requestPipeline  := (others => (others => '0'));	
begin
	if rising_edge(clk) then
		for i in 0 to 1 loop
			pipeline(i) := pipeline(i+1);
		end loop;
		
		pipeline(2) := ssram_address;
		ssram_data <= (31 downto 19  => '0') & pipeline(0);
	end if;
end process stimuli;
end architecture RTL;