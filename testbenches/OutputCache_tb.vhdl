-------------------------------------------------------------------------------
-- Title      : Testbench for design "OutputCache"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : OutputCache_tb.vhdl
-- Author     : Michael Oates  <michael@michael-desktop>
-- Company    : 
-- Created    : 2013-08-19
-- Last update: 2013-08-19
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2013-08-19  1.0      michael	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity OutputCache_tb is

end OutputCache_tb;

-------------------------------------------------------------------------------

architecture OutputCache_tb_RTL of OutputCache_tb is

  component OutputCache
    generic (
      ROWS_WIDTH        : natural;
      ROW_SIZE          : natural;     
      IDENTIFIER_WIDTH  : natural;
      SET_WIDTH         : natural;
      SET_COUNT_WIDTH   : natural;
      BURST_WIDTH       : natural;
      BURST_COUNT_WIDTH : natural;
      BPP               : natural;
      TILE_WIDTH        : natural;
      CLOCK_WIDTH       : natural;
      INPUT_WIDTH       : natural;
      TILES_PER_WORD_WIDTH : natural);
    port (
      clk              : in     std_logic;
      iReady, oWaiting : in     std_logic;
      iWaiting, oReady : buffer std_logic := '0';
      inData           : in     std_logic_vector(BPP*2**INPUT_WIDTH-1 downto 0);
      inAddress        : in     std_logic_vector(IDENTIFIER_WIDTH+SET_COUNT_WIDTH+TILE_WIDTH-INPUT_WIDTH downto 0);
      inEnabled        : in     std_logic_vector(2**INPUT_WIDTH-1 downto 0);
      oAddress         : out    std_logic_vector(IDENTIFIER_WIDTH+SET_COUNT_WIDTH+TILE_WIDTH-BURST_COUNT_WIDTH-1 downto 0));
  end component;

  -- component generics
  constant ROWS_WIDTH        : natural := 4;
  constant ROW_SIZE          : natural := 1;
  constant IDENTIFIER_WIDTH  : natural := 8;
  constant SET_WIDTH         : natural := 2;
  constant SET_COUNT_WIDTH   : natural := 2;
  constant BURST_WIDTH       : natural := 128;
  constant BURST_COUNT_WIDTH : natural := 3;
  constant BPP               : natural := 16;
  constant TILE_WIDTH        : natural := 6;
  constant CLOCK_WIDTH       : natural := 2;
  constant INPUT_WIDTH       : natural := 1;
  constant TILES_PER_WORD_WIDTH : natural := 4;

  -- component ports
  signal clk              : std_logic := '1';
  signal iReady, oWaiting : std_logic;
  signal iWaiting, oReady : std_logic := '0';
  signal inData           : std_logic_vector(BPP*2**INPUT_WIDTH-1 downto 0);
  signal inAddress        : std_logic_vector(IDENTIFIER_WIDTH+SET_COUNT_WIDTH+TILE_WIDTH-INPUT_WIDTH-1 downto 0);
  signal inEnabled        : std_logic_vector(2**INPUT_WIDTH-1 downto 0);
  signal oAddress         : std_logic_vector(IDENTIFIER_WIDTH+SET_COUNT_WIDTH+TILE_WIDTH-BURST_COUNT_WIDTH-1 downto 0);

  -- clock

begin  -- OutputCache_tb_RTL

  -- component instantiation
  DUT: OutputCache
    generic map (
      ROWS_WIDTH        => ROWS_WIDTH,
      ROW_SIZE          => ROW_SIZE,
      IDENTIFIER_WIDTH       => IDENTIFIER_WIDTH,
      SET_WIDTH         => SET_WIDTH,
      SET_COUNT_WIDTH   => SET_COUNT_WIDTH,
      BURST_WIDTH       => BURST_WIDTH,
      BURST_COUNT_WIDTH => BURST_COUNT_WIDTH,
      BPP               => BPP,
      TILE_WIDTH        => TILE_WIDTH,
      CLOCK_WIDTH       => CLOCK_WIDTH,
      INPUT_WIDTH       => INPUT_WIDTH,
      TILES_PER_WORD_WIDTH  => TILES_PER_WORD_WIDTH)
    port map (
      clk       => clk,
      iReady    => iReady,
      oWaiting  => oWaiting,
      iWaiting  => iWaiting,
      oReady    => oReady,
      inData    => inData,
      inAddress => inAddress,
      inEnabled => inEnabled,
      oAddress  => oAddress);

  -- clock generation
  Clk <= not Clk after 10 ps;

  -- waveform generation
  WaveGen_Proc: process
  begin
    --wait for 0 ps;
    -- insert signal assignments here
    oWaiting <= '1';
    
    wait for 20 ps;
    iReady <= '1';    
    inAddress <= (others => '0');
    inData <= (others => '1');
    inEnabled <= (others => '1');
    
    wait for 20 ps;
    inAddress <= (0 => '1', others => '0');
    
    wait for 20 ps;
    inAddress <= (1 => '1', others => '0');
    
    wait for 20 ps;
    inAddress <= (0|1 => '1', others => '0');
    
    wait for 20 ps;
    inAddress <= (2 => '1', others => '0');
    
    wait for 20 ps;
    inAddress <= (2|0 => '1', others => '0');
    
    wait for 20 ps;
    inAddress <= (2|1 => '1', others => '0');
    
    wait for 20 ps;
    inAddress <= (2|1|0 => '1', others => '0');
    
    
    wait for 20 ps;
    inAddress <= (6 => '1', others => '0');

    wait for 20 ps;
    inAddress <= (5 => '1', others => '0');
    
    wait for 20 ps;
    inAddress <= (4 => '1', others => '0');

    
    wait;
  end process WaveGen_Proc;

  

end OutputCache_tb_RTL;

-------------------------------------------------------------------------------

configuration OutputCache_tb_OutputCache_tb_RTL_cfg of OutputCache_tb is
  for OutputCache_tb_RTL
  end for;
end OutputCache_tb_OutputCache_tb_RTL_cfg;

-------------------------------------------------------------------------------
