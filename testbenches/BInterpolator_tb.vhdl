library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BInterpolator_tb is
  
end BInterpolator_tb;

architecture BInterpolator_tb_RTL of BInterpolator_tb is
  component BInterpolator
    port (
      clk, res         : in  STD_LOGIC;
      A0, A1, A2       : in  STD_LOGIC_VECTOR(31 downto 0);
      B0, B1           : out STD_LOGIC_VECTOR(7 downto 0);
      iReady, oWaiting : in  STD_LOGIC;
      oReady, iWaiting : out STD_LOGIC);
  end component;

  signal clk, res         : STD_LOGIC;
  signal A0, A1, A2       : STD_LOGIC_VECTOR(31 downto 0);
  signal B0, B1           : STD_LOGIC_VECTOR(7 downto 0);
  signal iReady, oWaiting : STD_LOGIC;
  signal oReady, iWaiting : STD_LOGIC;
begin  -- BInterpolator_tb_RTL

  BInterpolator_1: BInterpolator
    port map (
      clk      => clk,
      res      => res,
      A0       => A0,
      A1       => A1,
      A2       => A2,
      B0       => B0,
      B1       => B1,
      iReady   => iReady,
      oWaiting => oWaiting,
      oReady   => oReady,
      iWaiting => iWaiting);

  process
  begin  -- process
    clk <= '0';

    wait for 10 ps;
    res <= '1';    
    clk <= '1';

    wait for 10 ps;
  end process;

  process
  begin  -- process
    iReady <= '1';
    oWaiting <= '1';

    A0 <= STD_LOGIC_VECTOR(to_unsigned(1,32));
    A1 <= STD_LOGIC_VECTOR(to_unsigned(1,32));
    A2 <= STD_LOGIC_VECTOR(to_unsigned(1,32));


    
    wait for 180 ps;

    oWaiting <= '0';

    wait for 20 ps;

    A0 <= STD_LOGIC_VECTOR(to_unsigned(5,32));

    wait for 20 ps;

    oWaiting <= '1';
    
    wait;
  end process;

end BInterpolator_tb_RTL;
