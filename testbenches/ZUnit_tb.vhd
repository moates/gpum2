library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;
use work.ZBufferTypes.all;

entity ZUnit_tb is
end entity ZUnit_tb;

architecture RTL of ZUnit_tb is
	signal clk, res : std_logic := '0';

	component ZUnit
		generic(TileCount  : natural := 4800;
			    ZBankWidth : natural := 2;
			    ClockWidth : natural := 2;
			    SetWidth   : natural := 4);
		port(clk, clear, res                       : in  std_logic;
			 iReady, oWaiting                      : in  std_logic;
			 iWaiting, oReady, empty               : out std_logic;
			 readMemoryAddress, writeMemoryAddress : out Address;
			 readMemoryData                        : in  std_logic_vector(31 downto 0);
			 writeMemoryData                       : out std_logic_vector(31 downto 0);
			 inputData                             : in  ZBufferCheckType;
			 outputData                            : out ZBufferCheckType;
			 read, write, beginRead                : out std_logic;
			 readEmpty, writeFull                  : in  std_logic;
			 clearDepth                            : in  std_logic_vector(BPD - 1 downto 0);
			 comparisonType                        : in  std_logic_vector(1 downto 0));
	end component ZUnit;
	signal clear              : std_logic;
	signal iReady             : std_logic;
	signal oWaiting           : std_logic;
	signal iWaiting           : std_logic;
	signal oReady             : std_logic;
	signal empty              : std_logic;
	signal readMemoryAddress  : Address;
	signal readMemoryData     : std_logic_vector(31 downto 0);
	signal writeMemoryData    : std_logic_vector(31 downto 0);
	signal writeMemoryAddress : Address;
	signal inputData          : ZBufferCheckType;
	signal outputData         : ZBufferCheckType;
	signal read               : std_logic;
	signal write              : std_logic;
	signal beginRead          : std_logic;
	signal readEmpty          : std_logic;
	signal writeFull          : std_logic;
	signal clearDepth         : std_logic_vector(BPD - 1 downto 0);
	signal comparisonType     : std_logic_vector(1 downto 0);
begin
	zUnitInstance : component ZUnit
		generic map(TileCount  => 4800,
			        ZBankWidth => 2,
			        ClockWidth => 2,
			        SetWidth   => 3)
		port map(clk                => clk,
			     clear              => clear,
			     res                => res,
			     iReady             => iReady,
			     oWaiting           => oWaiting,
			     iWaiting           => iWaiting,
			     oReady             => oReady,
			     empty              => empty,
			     readMemoryAddress  => readMemoryAddress,
			     writeMemoryAddress => writeMemoryAddress,
			     readMemoryData     => readMemoryData,
			     writeMemoryData    => writeMemoryData,
			     inputData          => inputData,
			     outputData         => outputData,
			     read               => read,
			     write              => write,
			     beginRead          => beginRead,
			     readEmpty          => readEmpty,
			     writeFull          => writeFull,
			     clearDepth         => clearDepth,
			     comparisonType     => comparisonType);

	clock : process
	begin
		clk <= '0';

		wait for 50 ps;

		clk <= '1';
		res <= '1';

		wait for 50 ps;
	end process clock;
	
	inCoords : process is
		variable counter : integer := 0;
	begin
		if iWaiting = '1' then	
			inputData.PixelAddress <= (others => '0');
			inputData.Coords(0) <= (B0 => (others => '0'), B1 => (others => '0'), B2 => (others => '0'));
			inputData.Coords(1) <= (B0 => (others => '1'), B1 => (others => '0'), B2 => (others => '0'));
			inputData.Enabled <= (others => '1');
			inputData.Attributes.z0 <= to_unsigned(counter,16);
			inputData.Attributes.z1 <= to_unsigned(0,16);
			inputData.Attributes.z2 <= to_unsigned(0,16);
			counter := counter + 1;
		end if;
		
		wait for 100 ps;
	end process inCoords;
	

	stimuli : process
	begin
		iReady  <= '0';
		oWaiting <= '0';

		wait for 100 ps;
		iReady <= '1';	
		
		writeFull <= '0';
		
		comparisonType <= "01";

				

		wait for 100 ps;
		assert oReady <= '0';
		assert empty <= '0';
		
		wait until beginRead = '1';
		
		wait for 100 ps;

		for i in 0 to 31 loop
			readEmpty <= '0';
			assert read = '1' report "The memory should be waiting for reads" severity error;
			for ii in 0 to 1 loop
				readMemoryData((ii + 1) * 16 - 1 downto ii * 16) <= std_logic_vector(to_unsigned(ii + i * 2 + 1, 16));

			end loop;

			wait for 100 ps;
		end loop;

		readEmpty <= '1';
		wait for 100 ps;			

		wait until oReady = '1';
		
		iReady <= '0';
		
		wait until empty = '1';
	
		
	
		wait;
	end process stimuli;

end architecture RTL;