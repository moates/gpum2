-------------------------------------------------------------------------------
-- Title      : Testbench for design "OutputCacheSet"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : OutputCacheSet_tb.vhdl
-- Author     : Michael Oates  <michael@michael-desktop>
-- Company    : 
-- Created    : 2013-08-18
-- Last update: 2013-08-18
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2013-08-18  1.0      michael Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

-------------------------------------------------------------------------------

entity OutputCacheSet_tb is
end OutputCacheSet_tb;

-------------------------------------------------------------------------------

architecture OutputCacheSet_tb_RTL of OutputCacheSet_tb is
	component OutputCacheSet
		generic(
			IDENTIFIER_WIDTH : natural;
			SET_WIDTH        : natural;
			CLOCK_WIDTH      : natural);
		port(
			clk                                 : in  std_logic;
			iAddress                            : in  std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
			oAddress, tIndex                    : out std_logic_vector(SET_WIDTH - 1 downto 0);
			tAddress                            : out std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
			iReady, oWaiting, tComplete, tClear : in  std_logic;
			iWaiting, oReady, tReady            : out std_logic;
			empty                               : out std_logic);
	end component;

	-- component generics
	constant IDENTIFIER_WIDTH : natural := 6;
	constant SET_WIDTH        : natural := 2;
	constant CLOCK_WIDTH      : natural := 2;

	-- component ports
	signal clk                                 : std_logic := '1';
	signal iAddress, tAddress                  : std_logic_vector(IDENTIFIER_WIDTH - 1 downto 0);
	signal oAddress                            : std_logic_vector(SET_WIDTH - 1 downto 0);
	signal iReady, oWaiting, tComplete, tClear : std_logic;
	signal iWaiting, oReady, tReady, empty     : std_logic;

begin                                   -- OutputCacheSet_tb_RTL

	-- component instantiation
	DUT : OutputCacheSet
		generic map(
			IDENTIFIER_WIDTH => IDENTIFIER_WIDTH,
			SET_WIDTH        => SET_WIDTH,
			CLOCK_WIDTH      => CLOCK_WIDTH)
		port map(
			clk       => clk,
			iAddress  => iAddress,
			oAddress  => oAddress,
			tAddress  => tAddress,
			iReady    => iReady,
			oWaiting  => oWaiting,
			tComplete => tComplete,
			iWaiting  => iWaiting,
			oReady    => oReady,
			tReady    => tReady,
			empty     => empty,
			tClear    => tClear);

	-- clock generation
	Clk <= not Clk after 10 ps;

	-- waveform generation
	WaveGen_Proc : process
	begin
		wait for 10 ps;
		-- insert signal assignments here
		iAddress <= (others => '0');
		iReady   <= '1';
		oWaiting <= '1';
		tClear   <= '0';
		assert empty = '1' report "Buffer should initally be empty";

		wait for 20 ps;

		iAddress <= (others => '1');

		assert oReady = '1' report "Buffer should return as data ready";
		assert oAddress = (1 downto 0 => '0') report "Buffer should initially fill from front to back";
		assert empty = '0' report "Buffer should report as being not empty once it has one block";
		wait for 20 ps;

		iAddress <= std_logic_vector(to_unsigned(1, IDENTIFIER_WIDTH));

		wait for 20 ps;

		iAddress <= std_logic_vector(to_unsigned(0, IDENTIFIER_WIDTH));

		wait for 20 ps;

		assert oReady = '1' report "Buffer should return preselected block";
		assert oAddress = (1 downto 0 => '0') report "Buffer should return already allocated block";

		iAddress <= std_logic_vector(to_unsigned(2, IDENTIFIER_WIDTH));

		wait for 20 ps;

		iAddress <= std_logic_vector(to_unsigned(3, IDENTIFIER_WIDTH));

		wait for 20 ps;

		assert oReady = '0' report "Buffer should return as not ready with data";
		assert iWaiting = '0' report "Buffer should return as not ready for more data";

		wait for 80 ps;

		assert tReady = '1' report "Buffer should flag that it is waiting for a transfer";

		wait for 20 ps;

		tComplete <= '1';

		wait for 40 ps;

		assert oReady = '1' report "Buffer should return as ready for more data once a block is freed";
		assert tReady = '0' report "Once transfer is complete, ready flag should be cleared";
		assert empty = '0' report "Cache should show as being not empty";

		tClear <= '1';
		iReady <= '0';

		wait for 20 ps;

		assert tReady = '1' report "After initiating a transfer, the transfer ready flag should go high";

		wait for 200 ps;

		assert empty = '1' report "After emptying the cache, the empty flag should be raised to signal the cache is empty";

		wait;
	end process WaveGen_Proc;

end OutputCacheSet_tb_RTL;

-------------------------------------------------------------------------------

configuration OutputCacheSet_tb_OutputCacheSet_tb_RTL_cfg of OutputCacheSet_tb is
	for OutputCacheSet_tb_RTL
	end for;
end OutputCacheSet_tb_OutputCacheSet_tb_RTL_cfg;

-------------------------------------------------------------------------------
