library ieee;
use ieee.std_logic_1164.all;

entity MemoryQueueTestbench is	
end entity MemoryQueueTestbench;

architecture RTL of MemoryQueueTestbench is
	
	component MicroInterface
		port(clk             : in    std_logic;
			 rst             : in    std_logic;
			 oWaiting        : in    std_logic;
			 oReady          : buffer std_logic                    := '0';
			 instruction     : out   std_logic_vector(31 downto 0);
			 sdram_bus_addr  : out   std_logic_vector(12 downto 0);
			 sdram_bus_ba    : out   std_logic_vector(1 downto 0);
			 sdram_bus_cas_n : out   std_logic;
			 sdram_bus_cke   : out   std_logic;
			 sdram_bus_cs_n  : out   std_logic;
			 sdram_bus_dq    : inout std_logic_vector(31 downto 0) := (others => 'X');
			 sdram_bus_dqm   : out   std_logic_vector(3 downto 0);
			 sdram_bus_ras_n : out   std_logic;
			 sdram_bus_we_n  : out   std_logic);
	end component MicroInterface;
	
	signal gpu_0_gpu_output_fifo_full, clk,res : std_logic := '0';
	signal gpu_0_gpu_output_data : std_logic_vector(31 downto 0);
	signal gpu_0_gpu_output_write : std_logic;
	signal gpu_0_gpu_output_size : std_logic_vector(31 downto 0);
begin
	
	instance: component MicroInterface
		port map(clk             => clk,
			     rst             => res,
			     oWaiting        => oWaiting,
			     oReady          => oReady,
			     instruction     => instruction,
			     sdram_bus_addr  => sdram_bus_addr,
			     sdram_bus_ba    => sdram_bus_ba,
			     sdram_bus_cas_n => sdram_bus_cas_n,
			     sdram_bus_cke   => sdram_bus_cke,
			     sdram_bus_cs_n  => sdram_bus_cs_n,
			     sdram_bus_dq    => sdram_bus_dq,
			     sdram_bus_dqm   => sdram_bus_dqm,
			     sdram_bus_ras_n => sdram_bus_ras_n,
			     sdram_bus_we_n  => sdram_bus_we_n);
	
	clock_process : process is
	begin
		wait for 50 ps;
		clk <= '1';
		res <= '1';
		wait for 50 ps;
		clk <= '0';
	end process clock_process;
	
	stimulus : process is
	begin
		wait for 100 ps;
		assert gpu_0_gpu_output_size = x"00000100" report "Initial capacity should be 256" severity error;
		assert gpu_0_gpu_output_fifo_full = '0' report "Initial fifo should not be full" severity error;
		
		gpu_0_gpu_output_write <= '1';
		
		
		wait;
	end process stimulus;
	
	
end architecture RTL;
