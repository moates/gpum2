library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity GpuControllerTestBench is
end entity;

architecture RTL of GpuControllerTestBench is
component GPUController
		port(clk                                                      : in  std_logic;
			 indata                                                   : in  std_logic_vector(31 downto 0);
			 iReady                                                   : in  std_logic;
			 iWaiting                                                 : out std_logic    := '1';
			 Rasterizer_Empty, Triangle_Unit_Empty, Vertex_Unit_Empty : in  std_logic;
			 Output_Cache_Empty, Pipeline_Waiting         : in  std_logic;
			 Pipeline_Ready, Output_Cache_Flush_Buffers, loadMatrix               : out std_logic    := '0';
			 Active_Buffer                                            : buffer std_logic := '0';
			 Triangle_Unit_Force_Empty 								 : out std_logic := '0';
			 outVertexAttribute                                       : out VertexPreTransformAttributes;
			 outV                                                     : out std_logic_vector(31 downto 0);
			 outColour   											  : out std_logic_vector(15 downto 0));
	end component GPUController;
	
	signal clk, res : std_logic := '0';	
	signal indata : std_logic_vector(31 downto 0);
	signal iReady, iWaiting : std_logic;
	signal Rasterizer_Empty : std_logic;
	signal Triangle_Unit_Empty : std_logic;
	signal Vertex_Unit_Empty : std_logic;
	signal Output_Cache_Empty : std_logic;
	signal Pipeline_Waiting : std_logic;
	signal Pipeline_Ready : std_logic;
	signal Output_Cache_Flush_Buffers : std_logic;
	signal loadMatrix : std_logic;
	signal Active_Buffer : std_logic;
	signal Triangle_Unit_Force_Empty : std_logic;
	signal outVertexAttribute : VertexPreTransformAttributes;
	signal outV : std_logic_vector(31 downto 0);
	signal outColour : std_logic_vector(15 downto 0);
begin

	instance: GPUController
		port map(clk                        => clk,
			     indata                     => indata,
			     iReady                     => iReady,
			     iWaiting                   => iWaiting,
			     Rasterizer_Empty           => Rasterizer_Empty,
			     Triangle_Unit_Empty        => Triangle_Unit_Empty,
			     Vertex_Unit_Empty          => Vertex_Unit_Empty,
			     Output_Cache_Empty         => Output_Cache_Empty,
			     Pipeline_Waiting           => Pipeline_Waiting,
			     Pipeline_Ready             => Pipeline_Ready,
			     Output_Cache_Flush_Buffers => Output_Cache_Flush_Buffers,
			     loadMatrix                 => loadMatrix,
			     Active_Buffer              => Active_Buffer,
			     Triangle_Unit_Force_Empty  => Triangle_Unit_Force_Empty,
			     outVertexAttribute         => outVertexAttribute,
			     outV                       => outV,
			     outColour                  => outColour);

	clk_process : process is
	begin
		clk <= '0';
		
		wait for 50 ps;
		
		clk <= '1';
		res <= '0';
		
		wait for 50 ps;
	end process clk_process;

	input_process : process is
	begin
		iReady <= '0';
		Rasterizer_Empty <= '0';
		Triangle_Unit_Empty <= '1';
		Vertex_Unit_Empty <= '0';
		Output_Cache_Empty <= '0';		
		Pipeline_Waiting <= '1';
		wait for 100 ps;
		
		iReady <= '1';
		indata <= x"A0000000";
		
		wait for 100 ps;
		
		iReady <= '0';
		indata <= x"00000001";
		
		assert Pipeline_Ready = '0' report "Pipeline should not be ready for data until data has arrived" severity error;
		
		wait for 200 ps;
		
		assert Pipeline_Ready = '0' report "Pipeline should not be ready for data until data has arrived" severity error;
		
		iReady <= '1';		
		
		wait for 100 ps;
		
		assert Pipeline_Ready = '1' report "Pipeline should show data is ready" severity error;		
		assert outV = x"00000001" report "Pipeline should show data is ready" severity error;
		
		iReady <= '0';
		indata <= x"00000002";
		
		wait for 100 ps;
		
		assert Pipeline_Ready = '0' report "Pipeline should not be ready for data until data has arrived";
		
		wait for 100 ps;
		
		assert Pipeline_Ready = '0' report "Pipeline should not be ready for data until data has arrived";
		iReady <= '1';
		
		wait for 100 ps;
		
		assert Pipeline_Ready = '1' report "Pipeline should show data is ready for element v2";
		assert outV = x"00000002" report "Pipeline should show data is ready for element v2" severity error;
		
		iReady <= '0';
		indata <= x"00000003";
		
		wait for 100 ps;
		
		assert Pipeline_Ready = '0' report "Pipeline should not be ready for data until data has arrived";
		
		wait for 100 ps;
		
		assert Pipeline_Ready = '0' report "Pipeline should not be ready for data until data has arrived";
		iReady <= '1';
		
		wait for 100 ps;
		
		assert Pipeline_Ready = '1' report "Pipeline should show data is ready for element v3";
		assert outV = x"00000003" report "Pipeline should show data is ready for element v3" severity error;
		
		iReady <= '0';
		indata <= x"00000004";
		
		wait for 100 ps;
		
		assert Pipeline_Ready = '0' report "Pipeline should not be ready for data until data has arrived";
		
		wait for 100 ps;
		
		assert Pipeline_Ready = '0' report "Pipeline should not be ready for data until data has arrived";
		iReady <= '1';
		
		wait for 100 ps;
		
		assert Pipeline_Ready = '1' report "Pipeline should show data is ready for element v4";
		assert outV = x"00000004" report "Pipeline should show data is ready for element v4" severity error;
		
		wait;
	end process input_process;
	
	
	
	
end architecture RTL;
