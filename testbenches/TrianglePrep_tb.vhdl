library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity TrianglePrep_tb is
  
end TrianglePrep_tb;

architecture TrianglePrep_tb_RTL of TrianglePrep_tb is
  component TrianglePrep
    port (
      clk, res         : in     std_logic;
      V0, V1, V2       : in     Vector4Type;
      triangle         : out    TriangleType;
      iReady, oWaiting : in     std_logic;
      oReady, iWaiting : buffer std_logic);
  end component;

  signal clk, res         : std_logic := '0';
  signal V0, V1, V2       : Vector4Type;
  signal triangle         : TriangleType;
  signal iReady, oWaiting : std_logic;
  signal oReady, iWaiting : std_logic;
begin  -- TrianglePrep_tb_RTL

  TrianglePrep_1: TrianglePrep
    port map (
      clk      => clk,
      res      => res,
      V0       => V0,
      V1       => V1,
      V2       => V2,
      triangle => triangle,
      iReady   => iReady,
      oWaiting => oWaiting,
      oReady   => oReady,
      iWaiting => iWaiting);

  clock: process
  begin  -- process clock
    clk <= '0';
    wait for 10 ps;

    res <= '1';
    clk <= '1';
    wait for 10 ps;
  end process clock;

  process
  begin  -- process
    V0.X <= to_signed(-100,32);
    V0.Y <= to_signed(-100,32);
    V0.W <= to_signed(1,32);

    V1.X <= to_signed(360, 32);
    V1.Y <= to_signed(50, 32);
    V1.W <= to_signed(1,32);

    V2.X <= to_signed(260, 32);
    V2.Y <= to_signed(150, 32);
    V2.W <= to_signed(1,32);

    iReady <= '1';
    oWaiting <= '1';
    
    wait;
  end process;

end TrianglePrep_tb_RTL;
