library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

library std;
use std.textio.all;
use ieee.std_logic_textio.all;

entity GpuTestBench_tb is
end entity GpuTestBench_tb;

architecture RTL of GpuTestBench_tb is
	signal clk, res : std_logic := '0';

	component GPUPipeline
		port(clk, res                                                : in  std_logic;
			 gpu_output, zBuffer_writeData                           : out std_logic_vector(31 downto 0);
			 zBuffer_readData                                        : in  std_logic_vector(31 downto 0);
			 gpu_address, zBuffer_writeAddress, zBuffer_readAddress  : out Address;
			 gpu_enabled, zBuffer_enabled                            : out std_logic_vector(1 downto 0);
			 gpu_write, zBuffer_write, zBuffer_read, zBuffer_request : out std_logic;
			 gpu_writeFull, zBuffer_writeFull, zBuffer_readEmpty     : in  std_logic;
			 gpu_iWaiting                                            : buffer std_logic;
			 gpu_iReady                                              : in  std_logic;
			 gpu_iData                                               : in  std_logic_vector(31 downto 0);
			 active_buffer                                           : out std_logic);
	end component GPUPipeline;

	type zBuffer_memory_type is array (480 * 320 - 1 downto 0) of std_logic_vector(31 downto 0);
	signal zBuffer_memory       : zBuffer_memory_type := (others => (others => '0'));
	signal gpu_output           : std_logic_vector(31 downto 0);
	signal zBuffer_writeData    : std_logic_vector(31 downto 0);
	signal zBuffer_readData     : std_logic_vector(31 downto 0);
	signal gpu_address          : Address;
	signal zBuffer_writeAddress : Address;
	signal zBuffer_readAddress  : Address;
	signal gpu_enabled          : std_logic_vector(1 downto 0);
	signal zBuffer_enabled      : std_logic_vector(1 downto 0);
	signal gpu_write            : std_logic;
	signal zBuffer_write        : std_logic;
	signal zBuffer_read         : std_logic;
	signal zBuffer_request      : std_logic;
	signal gpu_writeFull        : std_logic;
	signal zBuffer_writeFull    : std_logic;
	signal zBuffer_readEmpty    : std_logic;
	signal gpu_iWaiting         : std_logic;
	signal gpu_iReady           : std_logic;
	signal gpu_iData            : std_logic_vector(31 downto 0);
	signal active_buffer        : std_logic;

begin
	clock : process
	begin
		clk <= '0';

		wait for 50 ps;

		clk <= '1';
		res <= '1';
		wait for 50 ps;
	end process clock;
	
	gpu_writeFull <= '0';
	zBuffer_writeFull <= '0';

	pipeline : component GPUPipeline
		port map(clk                  => clk,
			     res                  => res,
			     gpu_output           => gpu_output,
			     zBuffer_writeData    => zBuffer_writeData,
			     zBuffer_readData     => zBuffer_readData,
			     gpu_address          => gpu_address,
			     zBuffer_writeAddress => zBuffer_writeAddress,
			     zBuffer_readAddress  => zBuffer_readAddress,
			     gpu_enabled          => gpu_enabled,
			     zBuffer_enabled      => zBuffer_enabled,
			     gpu_write            => gpu_write,
			     zBuffer_write        => zBuffer_write,
			     zBuffer_read         => zBuffer_read,
			     zBuffer_request      => zBuffer_request,
			     gpu_writeFull        => gpu_writeFull,
			     zBuffer_writeFull    => zBuffer_writeFull,
			     zBuffer_readEmpty    => zBuffer_readEmpty,
			     gpu_iWaiting         => gpu_iWaiting,
			     gpu_iReady           => gpu_iReady,
			     gpu_iData            => gpu_iData,
			     active_buffer        => active_buffer);

	start_process : process(clk, res)
		variable inline : line;
		variable input  : std_logic_vector(31 downto 0);

		file infile : text is in "input_rs_tb.txt"; -- memory file
	begin                               -- process start_process
		if res = '0' then
			gpu_iReady <= '0';
		elsif clk = '1' and clk'event then
			if gpu_iReady = '1' and gpu_iWaiting = '1' then
				gpu_iReady <= '0';
			end if;

			if gpu_iWaiting = '1' and not endfile(infile) then
				report "Loading command";

				readline(infile, inline);
				hread(inline, input);

				gpu_iData <= input;

				gpu_iReady <= '1';
			end if;
		end if;
	end process start_process;

	-- purpose: Output to a file
	-- type   : sequential
	-- inputs : clk, res
	-- outputs:
	output_process : process(clk, res)
		file output_file : text is out "output.txt";
		file zoutput_file : text is out "zoutput.txt";
		variable l : line;
	begin                               -- process output_process
		if res = '0' then               -- asynchronous reset (active low)

		elsif clk'event and clk = '1' then -- rising clock edge
			if gpu_write = '1' then
				hwrite(l, "00" & gpu_address);
				writeline(output_file, l);

				hwrite(l, gpu_output);
				writeline(output_file, l);

				hwrite(l, gpu_enabled);
				writeline(output_file, l);
			end if;

			if zBuffer_write = '1' then
				hwrite(l, "00" & zBuffer_writeAddress);
				writeline(zoutput_file, l);

				hwrite(l, zBuffer_writeData);
				writeline(zoutput_file, l);

				hwrite(l, zBuffer_enabled);
				writeline(zoutput_file, l);

			end if;
		end if;
	end process output_process;

	name : process(clk) is
		variable address : integer range 0 to 320*480-1; 
	begin
		if rising_edge(clk) then
			if zBuffer_request = '1' then
				address := to_integer(unsigned(zBuffer_readAddress));
				zBuffer_readData <= zBuffer_memory(address);
				zBuffer_readEmpty <= '0';
			elsif zBuffer_read = '1' then				
				address := address + 1;
				zBuffer_readData <= zBuffer_memory(address);			
			end if;
		end if;			
	end process name;

end architecture RTL;