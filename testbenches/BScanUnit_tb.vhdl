library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity BScanUnit_tb is
  
end BScanUnit_tb;

architecture BScanUnit_tb_RTB of BScanUnit_tb is
  component BScanUnit
    port (
      clk, res         : in     std_logic;
      iReady, oWaiting : in     std_logic;
      iWaiting, oReady : buffer std_logic;
      triangle         : in     TriangleType;
      output           : out    PackedColourBurst);
  end component;

  signal clk, res         : std_logic := '0';
  signal iReady, oWaiting : std_logic;
  signal iWaiting, oReady : std_logic;
  signal triangle         : TriangleType;
  signal output           : PackedColourBurst;
begin  -- BScanUnit_tb_RTB

  BScanUnit_1: BScanUnit
    port map (
      clk      => clk,
      res      => res,
      iReady   => iReady,
      oWaiting => oWaiting,
      iWaiting => iWaiting,
      oReady   => oReady,
      triangle => triangle,
      output   => output);


  process
  begin  -- process
    clk <= '0';

    wait for 10 ps;

    clk <= '1';

    res <= '1';
    wait for 10 ps;
  end process;

  process
  begin  -- process
    iReady <= '1';
    oWaiting <= '1';

    triangle.a0 <= to_signed(-100,32);
    triangle.b0 <= to_signed(-100,32);
    triangle.g0 <= to_signed(41000,32);

    triangle.a1 <= to_signed(250,32);
    triangle.b1 <= to_signed(-360,32);
    triangle.g1 <= to_signed(0,32);

    triangle.a2 <= to_signed(-150,32);
    triangle.b2 <= to_signed(460,32);
    triangle.g2 <= to_signed(31000,32);

    wait;
  end process;

end BScanUnit_tb_RTB;
