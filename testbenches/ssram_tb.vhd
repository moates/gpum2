library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.GpuTypes.all;

entity ssram_tb_tb is
end entity ssram_tb_tb;

architecture RTL of ssram_tb_tb is
component SSRAM
	port(memory_clk, fpga_clk                                        : in    std_logic;
		 ssram_data                                                  : inout std_logic_vector(31 downto 0);
		 ssram_address                                               : out   Address;
		 ssram_ce1_n, ssram_ce2, ssram_ce3_n                         : out   std_logic;
		 ssram_gwe, ssram_bwe                                        : out   std_logic;
		 ssram_adsp, ssram_adsc                                      : out   std_logic;
		 ssram_bw                                                    : out   std_logic_vector(3 downto 0);
		 ssram_adv, ssram_oe                                         : out   std_logic;
		 vga_readAddress                                             : in    Address;
		 vga_readData                                                : out   std_logic_vector(31 downto 0);
		 outputCache_writeData                                       : in    std_logic_vector(31 downto 0);
		 zBuffer_readAddress                                         : in    Address;
		 zBuffer_readData                                            : out   std_logic_vector(31 downto 0);
		 zBuffer_writeData                                           : in    std_logic_vector(31 downto 0);
		 vga_read, vga_request, outputCache_write                    : in    std_logic;
		 zBuffer_read, zBuffer_request, zBuffer_write                : in    std_logic;
		 outputCache_be                                              : in    std_logic_vector(1 downto 0);
		 outputCache_writeAddress                                    : in    Address;
		 vga_readEmpty                                               : out   std_logic;
		 zBuffer_be                                                  : in    std_logic_vector(1 downto 0);
		 zBuffer_writeAddress                                        : in    Address;
		 zBuffer_readEmpty, zBuffer_writeFull, outputCache_writeFull : out   std_logic);
end component SSRAM;
	signal clk, res : std_logic := '0';
	signal memory_clk : std_logic;
	signal fpga_clk : std_logic;
	signal ssram_data : std_logic_vector(31 downto 0);
	signal ssram_address : Address;
	signal ssram_ce1_n : std_logic;
	signal ssram_ce2 : std_logic;
	signal ssram_ce3_n : std_logic;
	signal ssram_gwe : std_logic;
	signal ssram_bwe : std_logic;
	signal ssram_adsp : std_logic;
	signal ssram_adsc : std_logic;
	signal ssram_bw : std_logic_vector(3 downto 0);
	signal ssram_adv : std_logic;
	signal ssram_oe : std_logic;
	signal vga_readAddress : Address;
	signal vga_readData : std_logic_vector(31 downto 0);
	signal outputCache_writeData : std_logic_vector(31 downto 0);
	signal zBuffer_readAddress : Address;
	signal zBuffer_readData : std_logic_vector(31 downto 0);
	signal zBuffer_writeData : std_logic_vector(31 downto 0);
	signal vga_read : std_logic;
	signal vga_request : std_logic;
	signal outputCache_write : std_logic;
	signal zBuffer_read : std_logic;
	signal zBuffer_request : std_logic;
	signal zBuffer_write : std_logic;
	signal outputCache_be : std_logic_vector(1 downto 0);
	signal outputCache_writeAddress : Address;
	signal zBuffer_be : std_logic_vector(1 downto 0);
	signal vga_readEmpty : std_logic;
	signal zBuffer_writeAddress : Address;
	signal zBuffer_readEmpty : std_logic;
	signal zBuffer_writeFull : std_logic;
	signal outputCache_writeFull : std_logic;
	signal ssram_data_output : std_logic_vector(31 downto 0);
begin
	testbench_ssram: component SSRAM
		port map(memory_clk               => memory_clk,
			     fpga_clk                 => fpga_clk,
			     ssram_data               => ssram_data,
			     ssram_address            => ssram_address,
			     ssram_ce1_n              => ssram_ce1_n,
			     ssram_ce2                => ssram_ce2,
			     ssram_ce3_n              => ssram_ce3_n,
			     ssram_gwe                => ssram_gwe,
			     ssram_bwe                => ssram_bwe,
			     ssram_adsp               => ssram_adsp,
			     ssram_adsc               => ssram_adsc,
			     ssram_bw                 => ssram_bw,
			     ssram_adv                => ssram_adv,
			     ssram_oe                 => ssram_oe,
			     vga_readAddress          => vga_readAddress,
			     vga_readData             => vga_readData,
			     outputCache_writeData    => outputCache_writeData,
			     zBuffer_readAddress      => zBuffer_readAddress,
			     zBuffer_readData         => zBuffer_readData,
			     zBuffer_writeData        => zBuffer_writeData,
			     vga_read                 => vga_read,
			     vga_request              => vga_request,
			     outputCache_write        => outputCache_write,
			     zBuffer_read             => zBuffer_read,
			     zBuffer_request          => zBuffer_request,
			     zBuffer_write            => zBuffer_write,
			     outputCache_be           => outputCache_be,
			     outputCache_writeAddress => outputCache_writeAddress,
			     vga_readEmpty            => vga_readEmpty,
			     zBuffer_be               => zBuffer_be,
			     zBuffer_writeAddress     => zBuffer_writeAddress,
			     zBuffer_readEmpty        => zBuffer_readEmpty,
			     zBuffer_writeFull        => zBuffer_writeFull,
			     outputCache_writeFull    => outputCache_writeFull);

	memory_clk  <= clk;
	fpga_clk <= clk;
	ssram_data <= ssram_data_output when ssram_bwe = '1' else (others => 'Z');

	vga_read <= not vga_readEmpty;

	clock: process
	begin
		clk <= '0';	
		wait for 50 ps;
			
		clk <= '1';	
		wait for 50 ps;
	end process clock;
	
	dataProcess: process		
	begin
		wait until ssram_adsc = '0';
		wait for 300 ps;
		
		for i in 0 to 31 loop
			ssram_data_output <= std_logic_vector(to_unsigned(i,32));
			
			wait for 100 ps;								
		end loop;		
	end process dataProcess;
	
	stimuli: process
	begin
		vga_readAddress <= (others => '0');
		vga_request <= '1';
		
		wait for 100 ps;
		vga_request <= '0';
		
		wait for 100 ps;
		zBuffer_readAddress <= (5 => '1', others => '0');
		zBuffer_request <= '1';
		
		wait for 100 ps;
		zBuffer_request <= '0';
		
		wait for 500 ps;
		zBuffer_write <= '1';
		zBuffer_writeData <= (others => '1');
		zBuffer_writeAddress <= (others => '0');
		zBuffer_be <= (others => '1');
		
		wait for 3200 ps;
		zBuffer_write <= '0';
		
		wait for 500 ps;
		outputCache_write <= '1';
		outputCache_writeData <= (15 downto 0 => '0', others => '1');
		outputCache_writeAddress <= (5 => '1', others => '0');
		outputCache_be <= (others => '1');
		
		wait for 3200 ps;
		outputCache_write <= '0';
		
					
				
		wait;
	end process stimuli;

end architecture RTL;