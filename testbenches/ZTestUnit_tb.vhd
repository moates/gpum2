library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gputypes.all;
use work.ZBufferTypes.all;

entity ZTestUnit_tb is
end entity ZTestUnit_tb;

architecture RTL of ZTestUnit_tb is
	signal clk, res : std_logic := '0';
	component ZBuffer
		generic(TileCount  : natural := 4800;
			    ZBankWidth : natural := 2;
			    ClockWidth : natural := 2;
			    SetWidth   : natural := 4);
		port(clk, res                               : in  std_logic;
			 inZRequest, outZRequest                : in  std_logic;
			 stallZPipeline, read, write, beginRead : out std_logic;
			 inZBufferFetchAddress                  : in  Address;
			 inZBufferData                          : out ZBufferDataArray(PixelChannels - 1 downto 0);
			 outZBufferSaveAddress                  : in  Address;
			 outZBufferData                         : in  ZBufferDataArray(PixelChannels - 1 downto 0);
			 memoryReadData                         : in  std_logic_vector(PixelChannels * BPD - 1 downto 0);
			 writeMemoryData                        : out std_logic_vector(PixelChannels * BPD - 1 downto 0);
			 readEmpty, writeFull                   : in  std_logic;
			 writeMemoryAddress, readMemoryAddress  : out Address);
	end component ZBuffer;
	signal inZRequest            : std_logic;
	signal stallZPipeline        : std_logic;
	signal inZBufferFetchAddress : Address;
	signal inZBufferData         : ZBufferDataArray(PixelChannels - 1 downto 0);
	signal outZBufferSaveAddress : Address;
	signal outZBufferData        : ZBufferDataArray(PixelChannels - 1 downto 0);
	signal memoryReadData        : std_logic_vector(PixelChannels * BPD - 1 downto 0);
	signal outZRequest           : std_logic;
	signal read                  : std_logic;
	signal write                 : std_logic;
	signal writeMemoryData       : std_logic_vector(PixelChannels * BPD - 1 downto 0);
	signal readEmpty             : std_logic;
	signal writeFull             : std_logic;
	signal writeMemoryAddress    : Address;
	signal readMemoryAddress     : Address;
	signal beginRead : std_logic;

begin
	ZBuffer_inst : component ZBuffer
		generic map(TileCount  => 4800,
			        ZBankWidth => 2,
			        ClockWidth => 2,
			        SetWidth   => 3)
		port map(clk                   => clk,
			     res                   => res,
			     inZRequest            => inZRequest,
			     outZRequest           => outZRequest,
			     stallZPipeline        => stallZPipeline,
			     read                  => read,
			     write                 => write,
			     inZBufferFetchAddress => inZBufferFetchAddress,
			     inZBufferData         => inZBufferData,
			     outZBufferSaveAddress => outZBufferSaveAddress,
			     outZBufferData        => outZBufferData,
			     memoryReadData        => memoryReadData,
			     writeMemoryData       => writeMemoryData,
			     readEmpty             => readEmpty,
			     writeFull             => writeFull,
			     writeMemoryAddress    => writeMemoryAddress,
			     readMemoryAddress     => readMemoryAddress,
			     beginRead             => beginRead);

	clock : process
	begin
		clk <= '1';

		wait for 50 ps;

		clk <= '0';

		wait for 50 ps;
	end process clock;

	stimuli : process
	begin
		inZRequest  <= '0';
		outZRequest <= '0';

		wait for 100 ps;

		assert stallZPipeline = '0' report "Pipeline should by default not stall until a request is made" severity error;

		inZBufferFetchAddress <= (others => '0');
		inZRequest            <= '1';
		writeFull <= '0';

		for x in 0 to 8 loop
		
		inZBufferFetchAddress <= std_logic_vector(to_unsigned(x,11)) & "0000000";

		wait for 100 ps;
		assert stallZPipeline = '1' report "Pipeline should stall" severity error;
		
		wait until beginRead = '1';
		
		wait for 100 ps;

		for i in 0 to 31 loop
			readEmpty <= '0';
			assert read = '1' report "The memory should be waiting for reads" severity error;
			for ii in 0 to 1 loop
				memoryReadData((ii + 1) * 16 - 1 downto ii * 16) <= std_logic_vector(to_unsigned(ii + i * 2 + 1, 16));

			end loop;

			wait for 100 ps;
		end loop;

		readEmpty <= '1';
		wait for 100 ps;
		
		
		end loop;

		wait until stallZPipeline = '0';

		assert read = '0' report "The memory should stop waiting for reads" severity error;
		assert stallZPipeline = '0' report "The pipeline should allow for requests to continue" severity error;

		assert inZBufferData(0) = x"00000001" report "The output data should correspond to the data from the memory";
		assert inZBufferData(1) = x"00000002" report "The output data should correspond to the data from the memory";

		inZBufferFetchAddress(2) <= '1';

		wait for 100 ps;

		inZRequest <= '0';

		wait for 100 ps;

		assert inZBufferData(0) = x"00000003" report "The output data should correspond to the data from the memory";
		assert inZBufferData(1) = x"00000004" report "The output data should correspond to the data from the memory";

		inZRequest <= '0';
		inZBufferFetchAddress <= (others => '0');
		
		wait;
	end process stimuli;
end architecture RTL;