-------------------------------------------------------------------------------
-- Title      : Testbench for design "TransferUnit"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : TransferUnit_tb.vhdl
-- Author     : Michael Oates  <michael@michael-desktop>
-- Company    : 
-- Created    : 2013-08-18
-- Last update: 2013-08-18
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2013-08-18  1.0      michael	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity TransferUnit_tb is

end TransferUnit_tb;

-------------------------------------------------------------------------------

architecture TransferUnit_tb_RTL of TransferUnit_tb is

  component TransferUnit
    generic (
      INDEX_WIDTH       : natural;
      IDENTIFER_WIDTH   : natural;
      SET_WIDTH         : natural;
      SET_COUNT_WIDTH   : natural;
      BURST_WIDTH       : natural;
      BURST_COUNT_WIDTH : natural;
      BPP               : natural;
      TILE_WIDTH        : natural);
    port (
      clk                             : in  std_logic;
      iReady, tBlockCleared, tEnabled : in  std_logic;
      oWaiting                        : in  std_logic;
      iData                           : in  std_logic_vector(BURST_WIDTH-1 downto 0);
      iEnabled                        : in  std_logic_vector(2**BURST_COUNT_WIDTH-1 downto 0);
      iAddress                        : in  std_logic_vector(IDENTIFER_WIDTH-1 downto 0);
      iIndex                          : in  std_logic_vector(SET_WIDTH-1 downto 0);
      iSetIndex                       : in  std_logic_vector(SET_COUNT_WIDTH-1 downto 0);
      oIndex                          : out std_logic_vector(SET_WIDTH+SET_COUNT_WIDTH+TILE_WIDTH-BURST_COUNT_WIDTH-1 downto 0);
      clearColour                     : in  std_logic_vector(BPP-1 downto 0);
      oData                           : out std_logic_vector(BURST_WIDTH-1 downto 0);
      oEnabled                        : out std_logic_vector(2**BURST_COUNT_WIDTH-1 downto 0);
      oAddress                        : out std_logic_vector(IDENTIFER_WIDTH+TILE_WIDTH-BURST_COUNT_WIDTH-1 downto 0);
      oReady, iWaiting     : out std_logic);
  end component;

  -- component generics
  constant INDEX_WIDTH       : natural := 3;
  constant IDENTIFER_WIDTH   : natural := 7;
  constant SET_WIDTH         : natural := 2;
  constant SET_COUNT_WIDTH   : natural := 1;
  constant BURST_WIDTH       : natural := 128;
  constant BURST_COUNT_WIDTH : natural := 3;
  constant BPP               : natural := 16;
  constant TILE_WIDTH        : natural := 6;

  -- component ports
  signal iReady, tBlockCleared, tEnabled : std_logic;
  signal oWaiting                        : std_logic;
  signal iData                           : std_logic_vector(BURST_WIDTH-1 downto 0);
  signal iEnabled                        : std_logic_vector(2**BURST_COUNT_WIDTH-1 downto 0);
  signal iAddress                        : std_logic_vector(IDENTIFER_WIDTH-1 downto 0);
  signal iIndex                          : std_logic_vector(SET_WIDTH-1 downto 0);
  signal iSetIndex                       : std_logic_vector(SET_COUNT_WIDTH-1 downto 0);
  signal oIndex                          : std_logic_vector(SET_WIDTH+SET_COUNT_WIDTH+TILE_WIDTH-BURST_COUNT_WIDTH-1 downto 0);
  signal clearColour                     : std_logic_vector(BPP-1 downto 0);
  signal oData                           : std_logic_vector(BURST_WIDTH-1 downto 0);
  signal oEnabled                        : std_logic_vector(2**BURST_COUNT_WIDTH-1 downto 0);
  signal oAddress                        : std_logic_vector(IDENTIFER_WIDTH+TILE_WIDTH-BURST_COUNT_WIDTH-1 downto 0);
  signal oReady, iWaiting     : std_logic;

  -- clock
  signal Clk : std_logic := '1';

begin  -- TransferUnit_tb_RTL

  -- component instantiation
  DUT: TransferUnit
    generic map (
      INDEX_WIDTH       => INDEX_WIDTH,
      IDENTIFER_WIDTH   => IDENTIFER_WIDTH,
      SET_WIDTH         => SET_WIDTH,
      SET_COUNT_WIDTH   => SET_COUNT_WIDTH,
      BURST_WIDTH       => BURST_WIDTH,
      BURST_COUNT_WIDTH => BURST_COUNT_WIDTH,
      BPP               => BPP,
      TILE_WIDTH        => TILE_WIDTH)
    port map (
      clk           => clk,
      iReady        => iReady,
      tBlockCleared => tBlockCleared,
      tEnabled      => tEnabled,
      oWaiting      => oWaiting,
      iData         => iData,
      iEnabled      => iEnabled,
      iAddress      => iAddress,
      iIndex        => iIndex,
      iSetIndex     => iSetIndex,
      oIndex        => oIndex,
      clearColour   => clearColour,
      oData         => oData,
      oEnabled      => oEnabled,
      oAddress      => oAddress,
      oReady        => oReady,
      iWaiting      => iWaiting);

  -- clock generation
  Clk <= not Clk after 10 ps;

  -- waveform generation
  WaveGen_Proc: process
  begin
    wait for 10 ps;
    -- insert signal assignments here  
    -- insert signal assignments here
    iReady <= '1';
    oWaiting <= '1';
    iAddress <= (others => '0');
    iIndex <= (others => '0');
    iSetIndex <= (others => '0');
    tBlockCleared <= '0';
    iData <= (others => '0');
    clearColour <= (others => '1');
    iEnabled <= (others => '0');
    wait for 40 ps;

    assert oIndex = (SET_WIDTH+SET_COUNT_WIDTH+TILE_WIDTH-BURST_COUNT_WIDTH-1 downto 0 => '0') report "Address should start at base";

    wait for 60 ps;

    iEnabled <= (others => '1');
    assert oAddress = (IDENTIFER_WIDTH+TILE_WIDTH-BURST_COUNT_WIDTH-1 downto 0 => '0') report "Address should start at the base burst address";
    assert oData = (127 downto 0 => '1') report "All empty tiles should be filled with the clear colour";
    wait for 40 ps;

    assert oIndex(0) = '1' report "Should then move to the next block";
    oWaiting <= '0';
    
    wait for 40 ps;
    oWaiting <= '1';

    wait for 20 ps;
    assert oData = (127 downto 0 => '0') report "All enabled tiles should be their normal value";

    wait for 340 ps;
    tBlockCleared <= '1';
    iEnabled <= (others => '1');

    wait for 120 ps;
    assert oData = (127 downto 0 => '0') report "All full tiles should be emitted";
    iEnabled <= (others => '0');

    wait;
  end process WaveGen_Proc;

  

end TransferUnit_tb_RTL;

-------------------------------------------------------------------------------

configuration TransferUnit_tb_TransferUnit_tb_RTL_cfg of TransferUnit_tb is
  for TransferUnit_tb_RTL
  end for;
end TransferUnit_tb_TransferUnit_tb_RTL_cfg;

-------------------------------------------------------------------------------
