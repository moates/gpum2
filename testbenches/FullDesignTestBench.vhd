library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
use work.GpuTypes.all;

entity FullDesign_tb is
end entity FullDesign_tb;

architecture RTL of FullDesign_tb is
signal clk, res : std_logic := '0';

component vga
	generic(groupSize    : integer := 2;
		    bpp          : integer := 16;
		    screenWidth  : integer := 10;
		    screenHeight : integer := 9);
	port(vga_address                        : buffer Address;
		 data                               : in  std_logic_vector(31 downto 0);
		 memory_read, memory_request        : out std_logic;
		 memory_read_empty                  : in  std_logic;
		 pixel                              : out std_logic_vector(bpp - 1 downto 0);
		 clk, res                           : in  std_logic;
		 vsync, hsync, vga_clk, blank, sync : out std_logic);
end component vga;

component SSRAM
	port(memory_clk, fpga_clk, vga_clk                               : in    std_logic;
		 ssram_data                                                  : inout std_logic_vector(31 downto 0);
		 ssram_address                                               : out   std_logic_vector(18 downto 0);
		 ssram_ce1_n, ssram_ce2, ssram_ce3_n                         : out   std_logic;
		 ssram_gwe, ssram_bwe                                        : out   std_logic;
		 ssram_adsp, ssram_adsc                                      : out   std_logic;
		 ssram_bw                                                    : out   std_logic_vector(3 downto 0);
		 ssram_adv, ssram_oe                                         : out   std_logic;
		 vga_readAddress                                             : in    Address;
		 vga_readData                                                : out   std_logic_vector(31 downto 0);
		 outputCache_writeData                                       : in    std_logic_vector(31 downto 0);
		 zBuffer_readAddress                                         : in    Address;
		 zBuffer_readData                                            : out   std_logic_vector(31 downto 0);
		 zBuffer_writeData                                           : in    std_logic_vector(31 downto 0);
		 vga_read, vga_request, outputCache_write                    : in    std_logic;
		 zBuffer_read, zBuffer_request, zBuffer_write                : in    std_logic;
		 outputCache_be                                              : in    std_logic_vector(1 downto 0);
		 outputCache_writeAddress                                    : in    Address;
		 vga_readEmpty                                               : out   std_logic;
		 zBuffer_be                                                  : in    std_logic_vector(1 downto 0);
		 zBuffer_writeAddress                                        : in    Address;
		 zBuffer_readEmpty, zBuffer_writeFull, outputCache_writeFull : out   std_logic;
		 activeBuffer                                                : in    std_logic);
end component SSRAM;

component GPUPipeline
	port(clk, res                                                : in  std_logic;
		 gpu_output, zBuffer_writeData                           : out std_logic_vector(31 downto 0);
		 zBuffer_readData                                        : in  std_logic_vector(31 downto 0);
		 gpu_address, zBuffer_writeAddress, zBuffer_readAddress  : out Address;
		 gpu_enabled, zBuffer_enabled                            : out std_logic_vector(1 downto 0);
		 gpu_write, zBuffer_write, zBuffer_read, zBuffer_request : out std_logic;
		 gpu_writeFull, zBuffer_writeFull, zBuffer_readEmpty     : in  std_logic;
		 gpu_iWaiting                                            : buffer std_logic;
		 gpu_iReady                                              : in  std_logic;
		 gpu_iData                                               : in  std_logic_vector(31 downto 0);
		 active_buffer                                           : out std_logic);
end component GPUPipeline;
signal SRAM_DQ : std_logic_vector(31 downto 0);
signal SRAM_ADDR : std_logic_vector(18 downto 0);
signal SRAM_ADSC_N : std_logic;
signal SRAM_BWE : std_logic;
signal SRAM_OE_N : std_logic;
signal vga_address : Address;
signal vga_data : std_logic_vector(31 downto 0);
signal outputCache_writeData : std_logic_vector(31 downto 0);
signal zBuffer_readAddress : Address;
signal zBuffer_readData : std_logic_vector(31 downto 0);
signal zBuffer_writeData : std_logic_vector(31 downto 0);
signal vga_read : std_logic;
signal vga_request : std_logic;
signal outputCache_write : std_logic;
signal zBuffer_read : std_logic;
signal zBuffer_request : std_logic;
signal zBuffer_write : std_logic;
signal outputCache_be : std_logic_vector(1 downto 0);
signal outputCache_writeAddress : Address;
signal vga_readEmpty : std_logic;
signal zBuffer_enabled : std_logic_vector(1 downto 0);
signal zBuffer_writeAddress : Address;
signal zBuffer_readEmpty : std_logic;
signal zBuffer_writeFull : std_logic;
signal outputCache_writeFull : std_logic;
signal active_buffer : std_logic;
signal micro_oWaiting : std_logic;
signal micro_oReady : std_logic;
signal micro_oData : std_logic_vector(31 downto 0);
signal vga_pixel : std_logic_vector(bpp - 1 downto 0);
signal VGA_VS : std_logic;
signal VGA_HS : std_logic;

type memoryType is array(2**19-1 downto 0) of std_logic_vector(31 downto 0);
signal memory : memoryType := (others => (others => '0'));

begin

SSRAMInstance: component SSRAM
		port map(memory_clk               => clk,
			     fpga_clk                 => clk,
			     vga_clk 				  => clk,
			     ssram_data               => SRAM_DQ,
			     ssram_address            => SRAM_ADDR,
			     ssram_ce1_n              => open,
			     ssram_ce2                => open,
			     ssram_ce3_n              => open,
			     ssram_gwe                => open,
			     ssram_bwe                => SRAM_BWE,
			     ssram_adsp               => open,
			     ssram_adsc               => SRAM_ADSC_N,
			     ssram_bw                 => open,
			     ssram_adv                => open,
			     ssram_oe                 => SRAM_OE_N,
			     vga_readAddress          => vga_address,
			     vga_readData             => vga_data,
			     outputCache_writeData    => outputCache_writeData,
			     zBuffer_readAddress      => zBuffer_readAddress,
			     zBuffer_readData         => zBuffer_readData,
			     zBuffer_writeData        => zBuffer_writeData,
			     vga_read                 => vga_read,
			     vga_request              => vga_request,
			     outputCache_write        => outputCache_write,
			     zBuffer_read             => zBuffer_read,
			     zBuffer_request          => zBuffer_request,
			     zBuffer_write            => zBuffer_write,
			     outputCache_be           => outputCache_be,
			     outputCache_writeAddress => outputCache_writeAddress,
			     vga_readEmpty            => vga_readEmpty,
			     zBuffer_be               => zBuffer_enabled,
			     zBuffer_writeAddress     => zBuffer_writeAddress,
			     zBuffer_readEmpty        => zBuffer_readEmpty,
			     zBuffer_writeFull        => zBuffer_writeFull,
			     outputCache_writeFull    => outputCache_writeFull,
			     activeBuffer => active_buffer);


	vga_1 : vga
		generic map(groupSize    =>  ColourBurstWidth,
					bpp          => BPP,
					screenWidth  => SWidth,
					screenHeight => SHeight)
		port map(vga_address           => vga_address,
			     data              => vga_data,
			     memory_read       => vga_read,
			     memory_request    => vga_request,
			     memory_read_empty => vga_readEmpty,
			     pixel             => vga_pixel,
			     clk               => clk,
			     res               => res,
			     vsync             => VGA_VS,
			     hsync             => VGA_HS,
			     vga_clk           => open,
			     blank             => open,
			     sync              => open);

	GPUPipeline_1 : GPUPipeline
		port map(clk                  => clk,
			     res                  => res,
			     gpu_output           => outputCache_writeData,
			     zBuffer_writeData    => zBuffer_writeData,
			     zBuffer_readData     => zBuffer_readData,
			     gpu_address          => outputCache_writeAddress,
			     zBuffer_writeAddress => zBuffer_writeAddress,
			     zBuffer_readAddress  => zBuffer_readAddress,
			     gpu_enabled          => outputCache_be,
			     zBuffer_enabled      => zBuffer_enabled,
			     gpu_write            => outputCache_write,
			     zBuffer_write        => zBuffer_write,
			     zBuffer_read         => zBuffer_read,
			     zBuffer_request      => zBuffer_request,
			     gpu_writeFull        => outputCache_writeFull,
			     zBuffer_writeFull    => zBuffer_writeFull,
			     zBuffer_readEmpty    => zBuffer_readEmpty,
			     gpu_iWaiting         => micro_oWaiting,
			     gpu_iReady           => micro_oReady,
			     gpu_iData            => micro_oData,
			     active_buffer        => active_buffer);

clock: process
begin
	clk <= '0';
	
	wait for 50 ps;
	
	clk <= '1';
	res <= '1';
	wait for 50 ps;
end process clock;

start_process : process(clk, res)
		variable inline : line;
		variable input  : std_logic_vector(31 downto 0);

		file infile : text is in "input_rs_tb.txt"; -- memory file
	begin                               -- process start_process
		if res = '0' then
			micro_oReady <= '0';
		elsif clk = '1' and clk'event then
			if micro_oReady = '1' and micro_oWaiting = '1' then
				micro_oReady <= '0';
			end if;

			if micro_oWaiting = '1' and not endfile(infile) then
				report "Loading command";

				readline(infile, inline);
				hread(inline, input);

				micro_oData <= input;

				micro_oReady <= '1';
			end if;
		end if;
	end process start_process;
	
	stimuli: process(clk)
	type requestPipeline is array(0 to 3) of std_logic_vector(18 downto 0);
	variable pipeline : requestPipeline  := (others => (others => '0'));	
	begin
	if rising_edge(clk) then		
		for i in 0 to 1 loop
			pipeline(i) := pipeline(i+1);
		end loop;
		
		if SRAM_BWE ='0' then
			SRAM_DQ <= (others => 'Z');
			memory(to_integer(unsigned(SRAM_ADDR))) <= SRAM_DQ;
		else
			pipeline(2) := SRAM_ADDR;
			SRAM_DQ <= (31 downto 19  => '0') & pipeline(0);
		end if;
		
		
	end if;
	end process stimuli;

end architecture RTL;