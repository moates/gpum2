library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;

entity InterpolationUnit is
  
  port (
    clk, res : in  std_logic;
    input    : in InterpolationCoords;
    triangle : in TriangleAttributes;
    output   : out Colour3Type;
    oEnabled  : buffer std_logic;
    clkEnabled, iEnabled : in std_logic);

end InterpolationUnit;

architecture InterpolationUnit_RTL of InterpolationUnit is
  
  
  function MulColour (
    colour : signed(BPC downto 0);
    scale  : InterpolationElement)
    return signed is
    variable tmp : signed(ColourElement'high+InterpolationElement'high+3 downto 0);
  begin  -- MulColour
    tmp := colour * signed("0"&scale);

    return tmp(tmp'high downto tmp'high - 6);
  end MulColour;
begin  -- InterpolationUnit_RTL

  interpolationProcess: process (clk, clkEnabled)
	variable temp : unsigned(6 downto 0);
  begin  -- process interpolationProcess      
    if clk'event and clk = '1' and clkEnabled='1' then  -- rising clock edge
		temp := unsigned(MulColour(triangle.r0, input.b0) + MulColour(triangle.r1, input.b1)  + MulColour(triangle.r2, input.b2));
                if temp(6)='1' then
                      output.r <= (others => not temp(5));
                else
                      output.r <= temp(4 downto 0);
                end if;

                
		temp := unsigned(MulColour(triangle.g0, input.b0) + MulColour(triangle.g1, input.b1)  + MulColour(triangle.g2, input.b2));

                if temp(6)='1' then
                      output.g <= (others => not temp(5));
                else
                      output.g <= temp(4 downto 0);
                end if;
                
		temp := unsigned(MulColour(triangle.b0, input.b0) + MulColour(triangle.b1, input.b1)  + MulColour(triangle.b2, input.b2));

                if temp(6)='1' then
                      output.b <= (others => not temp(5));
                else
                      output.b <= temp(4 downto 0);
                end if;   

      oEnabled <= iEnabled;
    end if;
  end process interpolationProcess;

end InterpolationUnit_RTL;
