library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.GpuTypes.all;
use work.ZBufferTypes.all;

entity Rasterizer is
  generic (
    scanWidth  : natural := 2;
    scanHeight : natural := 1
    );

  port (
    clk, res         : in     std_logic;
    triangle         : in     TriangleType;
    triangleAttrs    : in     TriangleAttributes;
    pixelData        : out    ColourVectorArray(scanWidth*scanHeight-1 downto 0);
    pixelAddress     : out    Address;
    pixelsEnabled    : out     std_logic_vector(scanWidth*scanHeight-1 downto 0);
    iReady, oWaiting, clear, zBuffer_readEmpty, zBuffer_writeFull : in     std_logic;
    boundingBox		 : in     BoxType;
    iWaiting, oReady : buffer std_logic;
    empty, zBuffer_read, zBuffer_write, zBuffer_beginRead, zBuffer_bufferEmpty : out std_logic;
    zBuffer_readAddress, zBuffer_writeAddress  : out Address;
    zBuffer_readData : in std_logic_vector(31 downto 0);
    zBuffer_writeData : out std_logic_vector(31 downto 0);
    clearDepth : in std_logic_vector(BPD-1 downto 0);
    comparisonType : in std_logic_vector(1 downto 0)
    );
end Rasterizer;

architecture Rasterizer_RTL of Rasterizer is

  -----------------------------------------------------------------------------
  -- Component declarations
  -----------------------------------------------------------------------------

  component InterpolationUnit
    port (
      clk, res             : in     std_logic;
      input                : in     InterpolationCoords;
      triangle             : in     TriangleAttributes;
      output               : out    Colour3Type;
      oEnabled             : buffer std_logic;
      clkEnabled, iEnabled : in     std_logic);
  end component;

  component BScanUnit
  	generic(TILE_WIDTH : natural := 3;
  		    scanWidth  : natural := 1;
  		    scanHeight : natural := 1);
  	port(clk, res                : in  std_logic;
  		 iReady, oWaiting        : in  std_logic;
  		 iWaiting, oReady        : buffer std_logic;
  		 triangle                : in  TriangleType;
  		 startCoords             : in  BarycentricCoords;
  		 barycentricAreas        : out BarycentricCoordsArray(scanWidth * scanHeight - 1 downto 0);
  		 barycentricAreasEnabled : out std_logic_vector(scanWidth * scanHeight - 1 downto 0);
  		 inAreaAddress           : in  Address;
  		 areaAddress             : out Address;
  		 empty 					 : out std_logic);
  end component BScanUnit;
  
  component BScanUnitTop
  	generic(TILE_WIDTH : natural := 3);
  	port(clk, res         : in  std_logic;
  		 iReady, oWaiting : in  std_logic;
  		 iWaiting, oReady : buffer std_logic;
  		 triangle         : in  TriangleType;
  		 triangleOut      : out TriangleType;
  		 barycentricAreas : out BarycentricCoords;
  		 boundingBox      : in  BoxType;
  		 areaAddress      : out Address;
  		 empty 					 : out std_logic);
  end component BScanUnitTop;

  component BInterpolator
    port (
      clk, res             : in  STD_LOGIC;
      coords               : in  BarycentricCoords;
      oCoords              : out InterpolationCoords;
      clkEnable, enabledIn : in  STD_LOGIC;
      enabledOut           : out STD_LOGIC);
  end component;
  
  component ZUnit
  	generic(TileCount  : natural := 4800;
  		    ZBankWidth : natural := 2;
  		    ClockWidth : natural := 2;
  		    SetWidth   : natural := 4);
  	port(clk, clear, res                       : in  std_logic;
  		 iReady, oWaiting                      : in  std_logic;
  		 iWaiting, oReady, empty, buffer_empty : out std_logic;
  		 readMemoryAddress, writeMemoryAddress : out Address;
  		 readMemoryData                        : in  std_logic_vector(31 downto 0);
  		 writeMemoryData                       : out std_logic_vector(31 downto 0);
  		 inputData                             : in  ZBufferCheckType;
  		 outputData                            : out ZBufferCheckType;
  		 read, write, beginRead                : out std_logic;
  		 readEmpty, writeFull                  : in  std_logic;
  		 clearDepth                            : in  std_logic_vector(BPD - 1 downto 0);
  		 comparisonType                        : in  std_logic_vector(1 downto 0));
  end component ZUnit;

  -------------------------------------------------------------------------------
  -- Types
  -------------------------------------------------------------------------------
  
  type PipelineType is record
    triangle : TriangleAttributes;
    address : address;
    enabled : std_logic;
    covered : std_logic_vector(scanWidth*scanHeight-1 downto 0);
  end record;
  
  type TriangleAttributesPipeline is array (10 downto 0) of PipelineType;

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------
  
  signal pipeline : TriangleAttributesPipeline := (others => (address => (others => '0'), enabled => '0', triangle => (z0|z1|z2 => (others => '0'), others => (others => '0')), covered => (others => '0')));
  signal colourOutputs : ColourVectorArray(scanWidth*scanHeight-1 downto 0) := (others => (others => (others => '0')));
  signal interpolatorsEnabled, barycentricAreasEnabled : std_logic_vector(scanWidth*scanHeight-1 downto 0);
  signal areaAddress, BScan_areaAddress : address := (others => '0');
  signal scanUnitOutput : BarycentricCoordsArray(scanWidth*scanHeight-1 downto 0);
  signal activeTriangle : TriangleAttributes;
  signal triangle_ready, BScan_iReady, BScan_iWaiting : std_logic;
  signal BScan_Input_Triangle : TriangleType;
  signal BScan_Input_Coords : BarycentricCoords;
  signal BScan_empty : std_logic;
  signal BScan_top_empty, pixelChannelEmpty : std_logic;
  signal zTest_inputData : ZBufferCheckType;
  signal zTest_outputData : ZBufferCheckType;
  signal zTest_iWaiting : std_logic;
  signal interpolators_clkEn : std_logic;
  

  -----------------------------------------------------------------------------
  -- Architecture begin
  -----------------------------------------------------------------------------
begin  -- Rasterizer_RTL

  empty <= BScan_empty and BScan_top_empty and pixelChannelEmpty;

  pixelData <= colourOutputs;
  pixelAddress <= pipeline(0).address;
  pixelsEnabled <= interpolatorsEnabled;
  oReady <= pipeline(0).enabled;
  zTest_inputData.DataEnabled <= pipeline(2).enabled;
  zTest_inputData.Attributes <= pipeline(2).triangle;     
  zTest_inputData.PixelAddress <= pipeline(2).address;
  interpolators_clkEn <= oWaiting and zTest_iWaiting;
      
           
  interpolators_x : for x in 0 to scanWidth-1 generate
    begin
    interpolators_y : for y in 0 to scanHeight-1 generate 
      signal interpolation_coordinates, interpolation_coordinates_1 : InterpolationCoords;
      signal b_interpolator_enabled, b_interpolator_enabled_1  : std_logic;
      begin
         	                
      
      
      
      zTest_inputData.Coords(x+y*scanWidth) <= interpolation_coordinates;
      zTest_inputData.Enabled(x+y*scanWidth) <= b_interpolator_enabled;
      
      
      b_interpolator_enabled_1 <= zTest_outputData.Enabled(x+y*scanWidth);
      interpolation_coordinates_1 <= zTest_outputData.Coords(x+y*scanWidth);
      
      Interpolator_Stage0 : BInterpolator
        port map (
          clk      => clk,
          res      => res,
          clkEnable=> interpolators_clkEn,
          enabledIn => barycentricAreasEnabled(x+y*scanWidth),
          enabledOut => b_interpolator_enabled,
          coords   => scanUnitOutput(x+y*scanWidth),          
          oCoords  => interpolation_coordinates);
      
      Interpolator_Stage1 : InterpolationUnit
        port map (
          clk      => clk,
          res      => res,
          clkEnabled=> interpolators_clkEn,
          input    => interpolation_coordinates_1,
          triangle => pipeline(1).triangle,
          output   => colourOutputs(x+y*scanWidth),        
          oEnabled => interpolatorsEnabled(x+y*scanWidth),
          iEnabled => b_interpolator_enabled_1);
      
    end generate interpolators_y;
  end generate interpolators_x;  
  
  ZTestInstance : component ZUnit
  	generic map(TileCount  => 4800,
  		        ZBankWidth => 2,
  		        ClockWidth => 2,
  		        SetWidth   => 3)
  	port map(clk                => clk,
  		     clear              => clear,
  		     res                => res,
  		     iReady             => oWaiting,
  		     oWaiting           => oWaiting,
  		     iWaiting           => zTest_iWaiting,
  		     oReady             => open,
  		     empty              => open,
  		     buffer_empty => 	zBuffer_bufferEmpty,
  		     readMemoryAddress  => zBuffer_readAddress,
  		     writeMemoryAddress => zBuffer_writeAddress,
  		     readMemoryData     => zBuffer_readData,
  		     writeMemoryData    => zBuffer_writeData,
  		     inputData          => zTest_inputData,
  		     outputData         => zTest_outputData,
  		     read               => zBuffer_read,
  		     write              => zBuffer_write,
  		     beginRead          => zBuffer_beginRead,
  		     readEmpty          => zBuffer_readEmpty,
  		     writeFull          => zBuffer_writeFull,
  		     clearDepth         => clearDepth,
  		     comparisonType     => comparisonType);
     
  BScanTop_1 : BScanUnitTop
  	generic map(TILE_WIDTH => TileWidth)
  	port map(clk              => clk,
  		     res              => res,
  		     iReady           => iReady,
  		     oWaiting         => BScan_iWaiting,
  		     iWaiting         => iWaiting,
  		     oReady           => BScan_iReady,
  		     triangle         => triangle,
  		     triangleOut      => BScan_Input_Triangle,
  		     barycentricAreas => BScan_Input_Coords,
  		     boundingBox      => boundingBox,
  		     areaAddress      => BScan_areaAddress,
  		     empty 			  => BScan_top_empty);

  BScanUnit_1 : BScanUnit
    generic map (
      scanWidth  => scanWidth,
      scanHeight => scanHeight,
      TILE_WIDTH => TileWidth)
    port map (
      clk                     => clk,
      res                     => res,
      iReady                  => BScan_iReady,
      oWaiting                => oWaiting,
      iWaiting                => BScan_iWaiting,
      oReady                  => triangle_ready,
      triangle                => BScan_Input_Triangle,
      startCoords 			  => BScan_Input_Coords, 
      barycentricAreas        => scanUnitOutput,
      barycentricAreasEnabled => barycentricAreasEnabled,
      areaAddress 			  => areaAddress,
      inAreaAddress 		  => BScan_areaAddress,
      empty					  => BScan_empty);

  triangleAttributesPipelineProcess: process (clk, res)
  	variable pipelineFill : integer range 30 downto 0 := 0;
  begin  -- process
    if res = '0' then                   -- asynchronous reset (active low)
      
    elsif clk'event and clk = '1' then  -- rising clock edge
      -- Place a new triangle into the pipeline, as a new segment is ready
      if oWaiting = '1'  and zTest_iWaiting = '1' and triangle_ready = '1' then
        pipeline(pipeline'high).triangle <= activeTriangle;        
        pipeline(pipeline'high).enabled <= '1';
        pipeline(pipeline'high).address <= areaAddress;
        pipeline(pipeline'high).covered <= barycentricAreasEnabled;
    	pipelineFill := pipelineFill + 1;    
      elsif oWaiting = '1' then
        pipeline(pipeline'high).enabled <= '0';
      end if;

      if iWaiting='1' and iReady = '1' then
        activeTriangle <= triangleAttrs;
      end if;           
      
      if oWaiting = '1' and zTest_iWaiting = '1' then
        for i in 0 to pipeline'high-1 loop
          if i /= 1 then
          	pipeline(i) <= pipeline(i+1);
          end if;
        end loop; 
        
        pipeline(1).enabled <= zTest_outputData.DataEnabled;
  		pipeline(1).triangle <=zTest_outputData.Attributes;
  		pipeline(1).address <= zTest_outputData.PixelAddress; 
  		pipeline(1).covered  <= zTest_outputData.Enabled;
        
    	if oReady = '1' then
	      	pipelineFill := pipelineFill - 1;
      	end if;                     -- i
      end if;
      
      if pipeline(1).enabled = '1' and not (and_reduce(pipeline(1).covered)) = '1' then
      	--output from zbuffer failed test, reject data
      	pipelineFill := pipelineFill - 1;
      	pipeline(0).enabled <= '0';
      end if;
      
      if pipelineFill = 0 then
		pixelChannelEmpty <= '1';
	else
		pixelChannelEmpty <= '0';
	end if;
    end if;
  end process;
  
end Rasterizer_RTL;
